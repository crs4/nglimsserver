Use Picard's SamToFastq program to convert BAM files to fastq. This makes it
easy to store reads in Galaxy as compressed, accessible BAM files but then
allow them to be extracted to feed into programs requiring fastq.

Requires:
  Picard (http://picard.sourceforge.net/)
    The SamToFastq.jar file needs to be linked from this directory or available
    in a standard directory like /usr/share/java/picard.
  pysam (http://code.google.com/p/pysam/)
