"""Wrapper around Toshi's refFilesOfFeaturesGalaxyAnalysis.

This re-formats the command line and retrieves organism information to pass
along fasta headers, by providing the following three parameters.

REF_HEADER_SIZES
FEATURES_FILES
FEATURES_FILE_SORTED

{'userEmail': 'chapman@molbio.mgh.harvard.edu', u'features': [], u'limits':
{'limits_select': 'limits_none', '__current_case__': 0}, u'feature1':
<galaxy.model.HistoryDatasetAssociation object at 0x2aaaace3a950>,
u'alignments': <galaxy.model.HistoryDatasetAssociation object at
0x2aaaace3a2d0>, 'userId': '2', u'dbkey': 'hg18', u'feature1_sort': True,
u'reads': <galaxy.model.HistoryDatasetAssociation object at 0x2aaaace026d0>,
u'chromInfo': '/store3/alt_home/chapman/install/web/galaxy-central/tool-data/shared/ucsc/chrom/hg18.len'}
"""
import sys
import os
import re

param_pat = re.compile("[A-Z_]+=")

run_cmd = "refFilesOfFeaturesGalaxyAnalysis"

# determine which arguments are passed through, and which we need to reformat
out_args = []
extra_args = []
for arg in sys.argv[1:]:
    if param_pat.match(arg):
        out_args.append(arg)
    else:
        extra_args.append(arg)
# get the organism and user information, organize the rest as feature files
# first item is file, second is whether it is sorted or not
dbkey, chrom_info = extra_args[:2]
ffiles = []
fsorted = []
for i, item in enumerate(extra_args[2:]):
    if i % 2 == 0:
        ffiles.append(item)
    else:
        fsorted.append(item)
assert len(ffiles) == len(fsorted)
out_args.append("FEATURES_FILES=%s" % ",".join(ffiles))
out_args.append("FEATURES_FILE_SORTED=%s" % ",".join(fsorted))

# Use organism to get the fasta header file.
# 1. get the tool data directory in galaxy
ref_file = None
base_ref = "tool-data"
base_i = chrom_info.find(base_ref)
if base_i > 0:
    tool_dir = chrom_info[:base_i+len(base_ref)]
    index_file = os.path.join(tool_dir, "sam_fa_indices.loc")
    # 2. use index file to get reference location for this organism
    index_handle = open(index_file)
    for line in index_handle:
        if line.startswith("index\t%s" % db_key):
            (_, _, ref_file) = line.strip().split()
    index_handle.close()

ready = False
if ref_file:
    path, base = os.path.split(ref_file)
    (ref_path, _) = os.path.split(path)
    header_file = os.path.join(ref_path, "arachne", "%s.headerSizes" % base)
    if os.path.exists(header_file):
        out_args.append("REF_HEADER_SIZES=%s" % header_file)
        ready = True
if ready:
    os.system(" ".join([run_cmd] + out_args))
else:
    sys.stderr.write("Did not find header file for organism: %s" % dbkey) 
