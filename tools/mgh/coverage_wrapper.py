"""Wrapper around coverage executable to get genome reference files.
"""
import sys
import os

run_cmd = "coverage"

args = sys.argv[1:]

# the last arguments are the chromosome info file and database key, 
# which we use to retrieve the reference file and Arachne Header
chrom_info = args.pop(-1)
db_key = args.pop(-1)

# Use organism to get the fasta header file.
# 1. get the tool data directory in galaxy
ref_file = None
base_ref = "tool-data"
base_i = chrom_info.find(base_ref)
if base_i > 0:
    tool_dir = chrom_info[:base_i+len(base_ref)]
    index_file = os.path.join(tool_dir, "sam_fa_indices.loc")
    # 2. use index file to get reference location for this organism
    index_handle = open(index_file)
    for line in index_handle:
        if line.startswith("index\t%s" % db_key):
            (_, _, ref_file) = line.strip().split()
    index_handle.close()
# add the locations of the reference and header files to the arguments
ready = False
if ref_file and os.path.exists(ref_file):
    args.append("REF=%s" % ref_file)
    path, base = os.path.split(ref_file)
    (ref_path, _) = os.path.split(path)
    header_file = os.path.join(ref_path, "arachne", "%s.headerSizes" % base)
    if os.path.exists(header_file):
        args.append("REF_HEADER_SIZES=%s" % header_file)
        ready = True

if ready:
    #sys.stderr.write(" ".join([run_cmd] + args))
    os.system(" ".join([run_cmd] + args))
else:
    sys.stderr.write("Did not find header and reference file for organism: %s" %
            db_key) 
