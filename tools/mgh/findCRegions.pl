#! /usr/bin/perl -w

##############################################################################
#
# Date: Fri Dec 10 15:06:54 EST 2010
# Notes: Just to produce the hox*diff-like files
#
##############################################################################

use warnings;
use strict;
use Getopt::Long;
use File::Basename;


# Below are variables for options.
my $FILE = "";
my $OUT = "";
my $CHR = "";
my $START = 0;
my $STOP = 0;


# Can add additional option below, separating with commas.
GetOptions ('FILE=s' => \$FILE,
            'OUT=s' => \$OUT,
            'CHR=s' => \$CHR,
            'START=i' => \$START,
            'STOP=i' => \$STOP) ||
die "Could not parse options.";

sub usage {
   print "Usage:\n";
   print "blank.pl --FILE=string --OUT=string --CHR=string --START=positive_integer --STOP=positive_integer\n\n";
   print "FILE is the TSV output of printClusterGenes.\n";
   print "OUT is the output file.\n";
   print "CHR is the desired chromosome.  I.e. chr7.\n";
   print "START is the start position.\n";
   print "STOP is the ending position.\n";
}

if (@ARGV > 0) {
  # All arguments should be passed via the GetOptions form.
  usage();
  exit;
}


# my $tempFile = "$OUT.temp.RemoveAfterDone.tsv";
# system("awk \'{FS=\"\\t\"} (\$1 ~ /$CHR/ && \$2 >= $START && \$2 <= $STOP) || (\$3~ /$CHR/ && \$4 >= $START && \$4 <= $STOP) {print}\' $FILE > $tempFile");
# # Now remove rows where the same HinDIII site is "joined"
# system("awk \'(\$1 != \$3) || (\$2 != \$4) {print}\' $tempFile > $OUT");
# system("rm -f $tempFile");

system("awk \'{FS=\"\\t\"} (\$1 ~ /$CHR/ && \$2 >= $START && \$2 <= $STOP) || (\$3~ /$CHR/ && \$4 >= $START && \$4 <= $STOP) {print}\' $FILE | awk \'(\$1 != \$3) || (\$2 != \$4) {print}\' > $OUT");


