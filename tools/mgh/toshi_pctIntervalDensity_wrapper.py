"""Wrapper around pctIntervalDensity to provide REF_HEADER_SIZES.
"""
import sys
import subprocess
import os

def get_sam_index(chrom_info, dbkey):
    """Use organism to get the fasta header file.
    """
    ref_file = None
    base_ref = "tool-data"
    base_i = chrom_info.find(base_ref)
    if base_i > 0:
        tool_dir = chrom_info[:base_i+len(base_ref)]
        index_file = os.path.join(tool_dir, "sam_fa_indices.loc")
        # 2. use index file to get reference location for this organism
        index_handle = open(index_file)
        for line in index_handle:
            if line.startswith("index\t%s" % dbkey):
                ref_file = line.rstrip("\r\n").split()[-1]
                ref_file += ".fai" # add on the sam index
                break
        index_handle.close()
    return ref_file

cmd = "pctIntervalDensity"
base_args = sys.argv[1:-2]
dbkey, chrom_info = sys.argv[-2:]
ref_file = get_sam_index(chrom_info, dbkey)
if ref_file:
    base_args.append("REF_HEADER_SIZES=%s" % ref_file)
    subprocess.check_call([cmd] + base_args)
else:
    sys.stderr.write("Did not find header file for organism: %s" % dbkey)
