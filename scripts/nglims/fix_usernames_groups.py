#!/usr/bin/env python
"""Fix username and group problems on a custom basis for managing users.

Usage:
    add_ng_defaults.py <ini file>
"""
import os
import sys
import time, ConfigParser
from optparse import OptionParser

new_path = [ os.path.join( os.getcwd(), "lib" ) ]
new_path.extend( sys.path[1:] ) # remove scripts/ from the path
sys.path = new_path

from galaxy import eggs
import galaxy.model.mapping
from galaxy import model
import pkg_resources
pkg_resources.require( "SQLAlchemy >= 0.4" )

def main(ini_file):
    conf_parser = ConfigParser.ConfigParser({'here':os.getcwd()})
    conf_parser.read(ini_file)
    config = dict()
    for key, value in conf_parser.items("app:main"):
        config[key] = value
    try:
        db_con = config['database_connection']
    except KeyError:
        db_con = "sqlite:///%s?isolation_level=IMMEDIATE" % config["database_file"]
    app = SimpleApp(db_con, config['file_path'])

    fix_usernames_and_labs(app)

def fix_usernames_and_labs(app):
    allow_labs = [
            "Bioinformatics",
            "Solexa Core",
            "Blower Lab",
            "Chess Lab",
            "Gusella Lab",
            "Kingston Lab",
            "Lee Lab",
            "Oettinger Lab",
            "Ruvkun Lab",
            "Seed Lab",
            "Sheen Lab",
            "Mootha Lab",
            "Zong Wang Lab",
            "Hochedlinger Lab",
            ]
    remaps = {}
    name_remaps = {}
    named_labs = {}
    #remaps = {"Testing Lab" : "Solexa Core",
    #          "mb Admin" : "Solexa Core",
    #          "Sequencing" : "Solexa Core",
    #          "BioInformatics" : "Bioinformatics",
    #          "Oettinger" : "Oettinger Lab",
    #          "oettinger lab" : "Oettinger Lab",
    #          "Kingston" : "Kingston Lab",
    #          "Chess" : "Chess Lab",
    #          "Ruvkun": "Ruvkun Lab",
    #          "Seed" : "Seed Lab",
    #          "Jeannie Lee" : "Lee Lab",
    #          "JT Lee" : "Lee Lab",
    #          "lee lab" : "Lee Lab",
    #          "ZWang" : "Zong Wang Lab",
    #          "Konrad Hochedlinger" : "Hochedlinger Lab",
    #          "Mootha": "Mootha Lab",
    #          }
    #name_remaps = {"Aaron" : "Aaron Goldman",
    #               "shangtao" : "Shangtao Liu",
    #               "sylviafischer" : "Sylvia Fischer",
    #               "attilaszanto" : "Attila Szanto",
    #               "mattsimon" : "Matt Simon",
    #               "april": "April Cook",
    #               "jingzhao": "Jing Zhao",
    #               }
    #named_labs = {"Mark Borowsky": "Bioinformatics",
    #              "Toshiro Ohsumi": "Bioinformatics",
    #              "Matthew McCormack": "Sheen Lab",
    #              "Mike Blower": "Blower Lab",
    #              "Judith Sharp": "Blower Lab",
    #              "Joonyup Kim": "Sheen Lab",
    #              }
    for user in app.sa_session.query(app.model.User):
        lab = ""
        if user.form_values_id:
            form_val = app.sa_session.query(app.model.FormValues).get(
                         user.form_values_id)
            lab = form_val.content[0]
            if lab not in allow_labs:
                try:
                    new_lab = remaps[lab]
                    form_val.content = [new_lab]
                except KeyError:
                    print "Lab", user.username, lab
        else:
            lab = named_labs[user.username]
            form_val = app.model.FormValues()
            form_val.form_definition_id = 72
            form_val.content = [lab]
            app.sa_session.add(form_val)
            app.sa_session.flush()
            user.form_values_id = form_val.id
        if len(user.username.split()) < 2:
            try:
                new_name = name_remaps[user.username]
                user.username = new_name
            except KeyError:
                print "User", user.username
        print user.username, lab
    app.sa_session.flush()

class SimpleApp:
    def __init__(self, db_conn, file_path):
        self.model = galaxy.model.mapping.init(file_path, db_conn, engine_options={}, create_tables=False)
    @property
    def sa_session( self ):
        """
        Returns a SQLAlchemy session -- currently just gets the current
        session from the threadlocal session context, but this is provided
        to allow migration toward a more SQLAlchemy 0.4 style of use.
        """
        return self.model.context.current

if __name__ == "__main__":
    parser = OptionParser()
    (options, args) = parser.parse_args()
    main(*args)
