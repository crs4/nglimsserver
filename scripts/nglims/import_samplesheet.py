#!/usr/bin/env python

import os, sys, argparse
import csv,logging
import yaml,json,requests
sys.path.insert( 0, os.path.dirname( __file__ ) )
from api import GalaxyApiAccess

LOG_FORMAT = '%(asctime)s|%(levelname)-8s|%(message)s'
LOG_DATEFMT = '%Y-%m-%d %H:%M:%S'
LOG_LEVELS = ['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL']
CSV_LABELS =  ["FCID","Lane","SampleID","SampleRef","Index","Description","Control","Recipe","Operator","SampleProject"]
SAMPLE_FORM = {
    'quantitation_method': u'Quantitation Method',
    'recipe': u'',
    'analysis_validate': u'Standard',
    'analysis': u'Standard',
    'concentration': u'50',
    'validation_results': u'Validation Results',
    'library_type': u'',
    'insert_size_(including_adapters)': u'250',
    'service_names': u'Next gen sequencing',
    'method': u'paired end',
    'multiplex_barcodes': u'{}',
    'description': u'',
    'sample_preparation_method': u'Sample Preparation Method',
    'volume': u'15',
    'genome_build': u'',
    'services': u'7,8',
    'services_check': u'7,8',
    'name': u'',
    'is_multiplex': [],
    'library_construction_method': u'Library Construction Method',
    'genome_build_validate': u'',
    'cycles': u'100',
    'adapters': u''
}

SAMPLES_TO_PROJECT_FORM = {
    'payment_(fund_number)': u'99999',
    'project_name': u'',
    'create_request_button': u'Submit',
    'description': u'dummy descrition',
    'samples': [],
    'form_id': u'samples_to_project'
}

FLOWCELL_FORM = {
    'run_folder': u'',
    'run_notes': u'Run Notes',
    'create_request_button': u'Save',
    'form_id': u'add_to_sequencing_run',
    '_next_gen_prep': 8,
    'cur_id': u'',
    'sorted_samples': u'',
    'ids': u'',
    'state': u'Sequencing',
    '_func_name': 'add_to_sequencing_run',
    '_add_together': True
}

LIGA_URL =  "http://10.0.142.60/json/liga.php"

def make_parser():
    parser = argparse.ArgumentParser(description='Import CSV  samplesheet file')
   
    parser.add_argument('--galaxy_host', type=str, help='galaxy host (with port)')
    parser.add_argument('--api_key', type=str, help='Galaxy API key')
    
    #parser.add_argument('--ini-file', type=str, required=True, help='universe_wsgi config file')
    parser.add_argument('--only-check', action='store_true', default=False, help='Check samplesheet without import it')
   
    parser.add_argument('--import-from', type=str, required=True, choices=['csv','liga'], help='Set the frouce from import: csv or liga', default='csv')
    parser.add_argument('--samplesheet', type=str, required=True, help='Samplesheet to import: filename (if import-from=csv) or experimentname (if import-from=liga)')
    
    parser.add_argument('--loglevel', type=str, choices=LOG_LEVELS, help='logging level (default: INFO)', default='INFO')
    parser.add_argument('--logfile', type=str, help='log file (default=stderr)')
    
    return parser

def read_csv_samplesheet(csv_filename):
    global logger
    try:
        logger.info('opening csv samplesheet  %s' % csv_filename)
        f = open(csv_filename,'r')
    except:
        msg = 'ERROR => File %s doesn\'t exist' %csv_filename
        logger.critical(msg)
        sys.exit(msg)
    f.seek(0)
    csv_samplesheet = csv.DictReader(f,CSV_LABELS,delimiter=',')
    f.close
    return csv_samplesheet

def read_liga_samplesheet(expname):
    headers = {'content-type': 'application/json'}
    payload = {
        "method": "getSampleSheet",
        "params": [expname],
        "jsonrpc": "2.0",
        "id": 0,
    }
    
    response = requests.post(
        LIGA_URL, data=json.dumps(payload), headers=headers).json()
    
    if response['result'] not in ['None']:
        return response['result']
    else:
        msg = 'ERROR => Samplesheet %s doesn\'t exist' %expname
        logger.critical(msg)
        sys.exit(msg)
    
def flowcell_id_exists(id_flowcell):
    exists = apiRest.api_exists_flowcell_id(id_flowcell)
    return exists

def check_samplesheet(samplesheet, nglims_config):
    try:
        all_ok = True
        header = True
        msg = []
        for line in samplesheet:
            # checking header
            if header and sorted(line.values()) != sorted(CSV_LABELS):
                header = False
            else:
                continue
            # checking rows    
            if not header and sorted(line.values()) != sorted(CSV_LABELS):
                # checking consistency
                if sorted(line.keys()) != sorted(CSV_LABELS):
                    msg.append('ERROR => Samplesheet is not well formatted')
                    all_ok = False
               
                if flowcell_id_exists(line['FCID']):
                    if 'ERROR => Flowcell ID %s exists' % line['FCID'] not in msg:
                        msg.append('ERROR => Flowcell ID %s exists' % line['FCID'])
                    all_ok = False
                
                if line['Index'].lower() <> 'none' and line['Index'].lower() <> 'na' and line['Index'].strip() <> '' and line['Index'] not in nglims_config['barcodes'].keys():
                    msg.append('ERROR => %s is not a valid barcode' %line['Index'])
                    all_ok = False
                
                #checkin recipes
                ok_recipe = False
                if line['Recipe'] not in nglims_config['recipes']:
                    msg.append('ERROR => %s is not a valid recipe' %line['Recipe'])
                    all_ok = False
                    
                #checking references
                if line['SampleRef'] not in nglims_config['genome_build'].keys():
                    msg.append('ERROR => %s is not a valid reference' %line['SampleRef'])
                    all_ok = False
                    
        # check lenght of samplesheet
        if len(samplesheet) < 8:
            msg.append('ERROR => Only %s in samplesheet' %len(samplesheet))
            all_ok = False        
        
        if not all_ok:
            for error in msg:
                logger.critical(error)
        return all_ok
    except:
        msg = 'ERROR => Samplesheet is not valid'
        logger.critical(msg)
        return False
    
def retrieve_samplesheet(csv_samplesheet,nglims_config):
    flowcell_id = None
    samples = dict()
    try:
        for line in csv_samplesheet:
            if (sorted(line.values()) != sorted(CSV_LABELS)):
                key = line["SampleID"] + "|"+ line["Index"]
                this_lane = line["Lane"]
                if (samples.has_key(key)):
                    samples[key]['lanes'].append(this_lane)
                else:
                    if (flowcell_id is None):
                        flowcell_id = line['FCID']
                    samples[key] = dict()
                    samples[key] = {
                        'description' : line["SampleID"],
                        'recipe' : line["Recipe"],
                        'lanes' : [this_lane],
                        'multiplex_barcodes' : retrieve_barcode(line["Index"],nglims_config['barcodes']),
                        'id' : 0,
                        'name': '',
                        'adapters': line['Description'],
                        'genome_build' : retrieve_genome_build(line["SampleRef"],nglims_config['genome_build']),
                        'genome_build_validate' : retrieve_genome_build(line["SampleRef"],nglims_config['genome_build']),
                        'operator' : retrieve_operator(line["Operator"]),
                        'project' : line['SampleProject']
                        }
                  
                    if samples[key]['multiplex_barcodes'] != '{}':
                        samples[key]['is_multiplex'] = '["true","true"]'
                    else:
                        samples[key]['is_multiplex'] = '["false","false"]'
                    
    except:
        msg = 'ERROR => File is not a samplesheet csv file'
        logger.critical(msg)
        sys.exit(msg)
    return samples, flowcell_id

def retrieve_operator(operator):
    api_key = apiRest.api_get_api_key(operator)
    return api_key
    
def retrieve_genome_build(sampleRef,genome_build):
    if sampleRef in genome_build.keys():
        return genome_build[sampleRef]['label']
    else:
        return ''
    
def retrieve_recipe(recipe,recipes):
    for key in recipes.keys():
        if key  in recipe.lower():
            return recipes[key]
    
def retrieve_barcode(index,barcodes):
    multiplexed_barcodes = '{}'
    if barcodes.has_key(index):
        multiplexed_barcodes = '{"barcodes":[["NEW^^barcode","%s"]],"barcode_type" : "%s", "parent_id" : "", "not_filled": [], "not_named" : false}' % (barcodes[index]['label'],barcodes[index]['type'])
    return multiplexed_barcodes
    

def retrieve_lanes(lanes):
    str_lanes = ''
    for i in range (1,9):
        klane = "lane%s" % i
        str_lanes = '%s"%s":%s,' %(str_lanes,klane,json.dumps(lanes[klane]))
    str_lanes = '{%s}' %str_lanes[0:len(str_lanes)-1]
    lanes =  unicode(str_lanes.replace(" ", ""))
    return lanes

def retrieve_nglims_config(nglims_config):
    # getting barcodes
    nglims_barcodes = {}
    
    for entry in nglims_config['barcodes']:
        barcodes = entry['data']
        for barcode in  barcodes:
            for k, v in barcode.iteritems():
                if not nglims_barcodes.has_key(v):
                    label = "%s : %s"  %(k,v)
                    type = entry['name']
                    nglims_barcodes[v] = {'label' : label, 'type' : type}
    
    # getting recipes
    nglims_recipes = []
    nglims_requests = nglims_config['requests']
    for req in nglims_requests:
        if req['name'] == 'Next gen sequencing':
            inputs = req['form']['inputs']
            for row in inputs:
                if row['label'] == 'Recipe':
                    nglims_recipes = row['type']
                    
    # getting genome build
    nglims_genome_build = {}
    for ref in nglims_config['genome_build']:
        nglims_genome_build[ref['name']] = {'code' : ref['code'], 'label' : ref['label']}
    nglims_config = {'barcodes' : nglims_barcodes, 'recipes' : nglims_recipes, 'genome_build' : nglims_genome_build} 
    return nglims_config

def get_nglims_config():
    nglims_config = apiRest.api_get_nglims_config()
    
    if nglims_config is None:
        msg = 'ERROR => No nglims config file found'
        logger.critical(msg)
        sys.exit(msg)
        
    nglims_config = retrieve_nglims_config(nglims_config)
    return nglims_config

def delete_samples(ids):
    # deleting other samples saved
    for sample_id in ids:
        logger.info('deleting sample with id %s ' %sample_id)
        apiRest.api_delete_sample(sample_id)
        
def save_samples(samples,args):
    lanes = dict()
    projects = dict()
    ids = []
    for key,sample in samples.iteritems():
        # preparing form to post data
        if sample['operator'] is not None:
            apiRest = GalaxyApiAccess(args.galaxy_host,sample['operator'])
        else:
            apiRest = GalaxyApiAccess(args.galaxy_host,args.api_key)
        
        del sample['operator']
        
        this_project = sample['project']
        del sample['project']
        
        if not projects.has_key(this_project):
            projects[this_project] = []

        form_sample = SAMPLE_FORM
        sample['name'] = apiRest.api_get_new_sample_name()
        for k,v in sample.iteritems():
            if k not in 'lanes' and k not in 'id':
                form_sample[k] = v
        # saving sample
        logger.info('saving sample %s with name %s | lanes: %s | barcode: %s | recipe: %s | adapters: %s' %(sample['description'], sample['name'], ','.join(sample['lanes']),sample['multiplex_barcodes'],sample['recipe'],sample['adapters']))
        id_sample = apiRest.api_save_sample(form_sample)
        if id_sample:
            # sample saved
            logger.info('saved with id: %s' %id_sample)
            ids.append('%s' % id_sample)
            if id_sample not in projects[this_project]:
                projects[this_project].append('%s' % id_sample)
                
            sample['id'] = id_sample
            samples[key] = sample
            for lane in sample["lanes"]:
                klane = "lane%s" % lane
                if lanes.has_key(klane):
                    lanes[klane].append("%s" % id_sample)
                else:
                    lanes[klane] = ["%s" % id_sample]
        else:
            # error while saving sample
            logger.critical('error while saving sample %s' % sample['description'])
            # deleting other samples saved
            logger.info('deleting other samples saved')
            delete_samples(ids)
            msg = 'ERROR => error while saving sample %s' % sample['description']
            sys.exit(msg)
    lanes = retrieve_lanes(lanes)
    
    return lanes,ids,projects

def samples_to_projects(ids,projects):
    
    for key, value in projects.items():
        logger.info('adding samples with ids %s on project %s' %(value,key))
        form_samples_to_project = SAMPLES_TO_PROJECT_FORM
        # preparing form to post data
        form_samples_to_project['samples'] = value
        form_samples_to_project['project_name'] = key
        # run api call
        success,messages = apiRest.api_samples_to_project(form_samples_to_project)

        if success == False:
            # error while associating samples to project 
            logger.critical('error while associating samples to project  %s' % form_samples_to_ptoject['project_name'])
            # deleting other samples saved
            logger.info('deleting other samples saved')
            delete_samples(ids)
            msg = 'ERROR => error while associating samples to project  %s' % form_samples_to_ptoject['project_name']
            sys.exit(msg)
            
    return True

def move_samples_to_sequencing_queue(ids):
    # from 'awaiting' state to 'pre-sequencing' state
    new_state_0 = apiRest.api_move_samples_to_next_queue(ids)
    
    # from 'pre-sequencing' state to 'sequencing' state
    new_state_1 = apiRest.api_move_samples_to_next_queue(ids)

    if new_state_0 not in ['Pre-sequencing quantitation'] or new_state_1 not in ['Sequencing']:
        # error while setting samples state in 'sequencing mode'
        logger.critical("error while setting samples state in 'sequencing mode'")
        # deleting other samples saved
        logger.info('deleting other samples saved')
        delete_samples(ids)
        msg = "ERROR => error while setting samples state in 'sequencing mode'"
        sys.exit(msg)

def add_samples_to_sequencing_run(lanes,ids,id_flowcell):
    form_flowcell = FLOWCELL_FORM
    # preparing form to post data
    form_flowcell['ids'] = ','.join(ids)
    form_flowcell['sorted_samples'] = lanes
    form_flowcell['run_folder'] = str(id_flowcell)
    form_flowcell['cur_id'] = str(ids[0])
    # run api call
    apiRest.api_add_samples_to_sequencing_run(form_flowcell)

    return True

def import_samplesheet(samples,id_flowcell,args):
    # saving samples in nglims
    logger.info('saving samples in nglims')
    lanes,ids,projects = save_samples(samples,args)
    
    apiRest = GalaxyApiAccess(args.galaxy_host,args.api_key)
    
    # associating samples to project
    logger.info('associating samples to projects')
    samples_to_projects(ids,projects)
    
    # setting samples state in 'sequencing mode'
    logger.info("setting samples state in 'sequencing mode'")
    move_samples_to_sequencing_queue(ids)

    # building flowcell && creating sequencing run
    logger.info("building flowcell && creating sequencing run")
    add_samples_to_sequencing_run(lanes,ids,id_flowcell)
    
    logger.info("SUCCESS => Samplesheet %s is now on Galaxy nglims with flowcell id %s" %(args.samplesheet,id_flowcell))
    

def get_samplesheet(args):
    global logger

    # get nglims configuration
    logger.info('get nglims configuration')
    nglims_config = get_nglims_config()
        
    if args.import_from in ['csv']:
        # reading csv samplesheet file
        logger.info('reading csv samplesheet from file %s' % args.samplesheet)
        samplesheet = read_csv_samplesheet(args.samplesheet)
        to_check = list(samplesheet)
    elif args.import_from in ['liga']:
        # reading csv samplesheet file
        logger.info('reading json samplesheet from ligadb %s' % args.samplesheet)
        samplesheet = read_liga_samplesheet(args.samplesheet)
        to_check = list(samplesheet)
   
    # checking samplesheet
    logger.info('checking samplesheet data')
    all_ok = check_samplesheet(to_check,nglims_config)
   
    if all_ok:
        msg = '%s is a valid samplesheet' % args.samplesheet 
        logger.info(msg)
        if args.only_check:
            sys.exit(msg)
    else:
        msg = 'WARNING: %s is NOT a valid samplesheet' % args.samplesheet 
        logger.critical(msg)
        sys.exit(msg)
    
    # formatting samplesheet to import
    logger.info('formatting samplesheet to import')
    samples,id_flowcell = retrieve_samplesheet(samplesheet,nglims_config)
    
    if len(samples) == 0:
        msg = 'ERROR => No samples found in samplesheet csv file'
        logger.critical(msg)
        sys.exit(msg)

    logger.info('found %i samples' % len(samples))
    return samples,id_flowcell

def init_logger(args,logging):
    log_level = getattr(logging, args.loglevel)
    kwargs = {
        'format'  : LOG_FORMAT,
        'datefmt' : LOG_DATEFMT,
        'level'   : log_level}

    if args.logfile:
        kwargs['filename'] = args.logfile
    logging.basicConfig(**kwargs)
   
    logger = logging.getLogger('csv_to_samplesheet')
    return logger

def init_api_galaxy(args):
    try:
        galaxy_host = args.galaxy_host or os.environ['GALAXY_HOST']
        api_key = args.api_key or os.environ['GALAXY_API_KEY']
    except KeyError, ke:
        msg = 'No argument passed and no global variable %s found' % ke
        logger.critical(msg)
        sys.exit(msg)

    
    logger.info('opening connection to %s' % galaxy_host)
    apiRest = GalaxyApiAccess(galaxy_host,api_key)

    return apiRest

def main(argv):
    global logger
    global apiRest
     
    parser = make_parser()
    args = parser.parse_args(argv)

    # Initializing logger
    logger = init_logger(args,logging)

    # Initializing connection to apiGalaxy
    apiRest = init_api_galaxy(args)

    # Reading && formatting samplesheet
    samples,id_flowcell = get_samplesheet(args)
    
    # Importing samplesheet
    import_samplesheet(samples,id_flowcell,args)

if __name__ == '__main__':
    main(sys.argv[1:])
    
