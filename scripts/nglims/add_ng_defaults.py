#!/usr/bin/env python
"""Custom script which adds next-gen sequencing defaults to a galaxy database.

This adds the basic parts we will be building off of to track and manage
sequencing results.

See tool-data/nglims.yaml for the configuration file to adjust the same request
information.

While testing, you can remove all of your current data with:

    DELETE FROM form_values;
    DELETE FROM sample_event;
    DELETE FROM sample_request_map;
    DELETE FROM sample;
    DELETE FROM request_event;
    DELETE FROM request

Then all of the definitions with:
    DELETE FROM sample_state;
    DELETE FROM request_type;
    update form_definition_current SET latest_form_id = NULL;
    DELETE FROM form_definition;
    DELETE FROM form_definition_current;

Usage:
    add_ng_defaults.py <ini file>
"""
import os
import sys
import time, ConfigParser
from optparse import OptionParser
import yaml

new_path = [ os.path.join( os.getcwd(), "lib" ) ]
new_path.extend( sys.path[1:] ) # remove scripts/ from the path
sys.path = new_path

from galaxy import eggs
import pkg_resources

import galaxy.model.mapping
from galaxy import model, config

pkg_resources.require( "SQLAlchemy >= 0.4" )

def main(ini_file):
    conf_parser = ConfigParser.ConfigParser({'here':os.getcwd()})
    conf_parser.read(ini_file)
    ini_config = dict()
    for key, value in conf_parser.items("app:main"):
        ini_config[key] = value
    ini_config = config.Configuration(**ini_config)
    nglims_config_file = ini_config.get("nglims_config_file", None)
    if not nglims_config_file:
        raise ValueError("Need to specify nglims configuration in universe_wsgi")
    in_handle = open(nglims_config_file)
    nglims_config = yaml.load(in_handle)
    in_handle.close()
    db_con = ini_config.database_connection
    if not db_con:
        db_con = "sqlite:///%s?isolation_level=IMMEDIATE" % ini_config.database
    app = SimpleApp(db_con, ini_config.file_path)
    top_level_type = create_default_requests(app, nglims_config["requests"],
            nglims_config["samples"], nglims_config["user"])
    create_sample_states(app, top_level_type,
            nglims_config["samples"]["states"])
    for control in nglims_config["controls"]:
        create_control(app, control, nglims_config["requests"], nglims_config["barcodes"])
    create_sqn_results(app, nglims_config["sqn_run_results"])

class RequestDef:
    def __init__(self, name, form_type, fields, form_only = False):
        self.name = name
        self.form_type = form_type
        self.fields = fields
        self.form_only = form_only

    def add_request_type(self, app, form, sample_form):
        rt = None
        if not self.form_only:
            rt = app.sa_session.query(app.model.RequestType).filter_by(
                name=self.name).first()
            if rt is None:
                rt = app.model.RequestType(self.name, "", form, sample_form)
                app.sa_session.add(rt)
        return rt

    def add_form_definition(self, app, update_fields=False):
        form = app.sa_session.query(app.model.FormDefinition).filter_by(
            name=self.name).first()
        fields, tooltips = self._get_fields()
        if form is None:
            form = app.model.FormDefinition(self.name, "", fields,
                    form_type=self.form_type, layout=[])
            form_curr = app.model.FormDefinitionCurrent(form)
            form.form_definition_current = form_curr
            app.sa_session.add(form_curr)
            app.sa_session.add(form)
        # if we are consistent with our fields, add the new data
        elif len(fields) == len(form.fields):
            form.fields = fields
        else:
            self._update_values(form, fields, app)
            form.fields = fields
        app.sa_session.flush()
        # add or update tooltips for each of the fields
        tt_fields = []
        for i, field in enumerate(form.fields):
            if tooltips.get(i, ""):
                field['title'] = tooltips[i]
            tt_fields.append(field)
        form.fields = tt_fields
        app.sa_session.flush()
        return form

    def _update_values(self, form, new_fields, app):
        """Update values to match the new form fields.

        This requires that you don't change the labels of the old
        fields while trying to add new items. Do one at a time.
        """
        is_delete = len(new_fields) < form.fields
        new_labels = [f["label"] for f in new_fields]
        if not is_delete:
            for old_label in (f["label"] for f in form.fields):
                assert old_label in new_labels, "Form label changed: %s" % old_label
        label_to_name = dict()
        for f in form.fields:
            label_to_name[f["label"]] = f["name"]
        label_remap = dict()
        new_info = dict()
        for f in new_fields:
            if f["label"] in label_to_name:
                label_remap[label_to_name[f["label"]]] = f["name"]
            else:
                new_info[f["name"]] = ""

        for form_val in app.sa_session.query(app.model.FormValues
                ).filter_by(form_definition_id = form.id):
            new_content = dict()
            for key, val in form_val.content.items():
                try:
                    new_key = label_remap[key]
                except KeyError:
                    if is_delete:
                        new_key = "deleted_%s" % key
                    else:
                        raise
                new_content[new_key] = val
            new_content.update(new_info)
            form_val.content = new_content

    def _get_fields(self):
        final_fields = []
        tooltips = {}
        for i, field in enumerate(self.fields):
            ftype = "TextField"
            tooltip = ""
            required = True
            freetext = False
            selectf = None
            if isinstance(field, str):
                name = field
            elif len(field) == 2:
                name, tooltip = field
            elif len(field) == 3:
                name, tooltip, ftype = field
            elif len(field) == 5:
                name, tooltip, ftype, required, freetext = field
            else:
                print field
                raise NotImplementedError
            if isinstance(ftype, list):
                selectf = ftype
                ftype = "ComboboxField"
                if freetext:
                    ftype += "_freetext"
                required = False
            cur_field = self._get_field(name, required, ftype, i, selectf)
            final_fields.append(cur_field)
            tooltips[i] = tooltip
        return final_fields, tooltips

    def _get_field(self, name, required, ftype, pos, selectlist=None):
        if required:
            required = "required"
        else:
            required = "optional"
        base = dict(
                name = "field_%s" % pos,
                label = name,
                required = required,
                type = ftype,
                layout = "%s" % pos,
                helptext = "",
                visible = True)
        if selectlist is not None:
            base['selectlist'] = selectlist
        return base

def _get_form_items(form):
    """Retrieve the input form elements from a configuration.
    """
    final = []
    for item in form:
        label = item["label"]
        tooltip = item.get("tooltip", "")
        itype = item.get("type", "TextField")
        required = item.get("required", True)
        freetext = item.get("freetext", False)
        final.append((label, tooltip, itype, required, freetext))
    return final

def create_default_requests(app, request_conf, sample_conf, user_conf):
    """Create a set of possible sequencing and preparation requests.
    """
    top_level_type = None
    requests = []
    # start with specific forms for samples and users
    sample_form = sample_conf["form"]
    user_form = user_conf["form"]
    requests.append(RequestDef("Sample Information",
        model.FormDefinition.types.get("SAMPLE"),
        _get_form_items(sample_form), True))
    requests.append(RequestDef("User Registration Form",
        model.FormDefinition.types.get("USER_INFO"),
        _get_form_items(user_form), True))
    # now general requests for under sample data
    for request in request_conf:
        if request.get('top_level'):
            requests.append(RequestDef(request["name"],
                model.FormDefinition.types.get("REQUEST"),
                _get_form_items(request["form"])))
            top_level_type = request["name"]
        else:
            form_type = model.FormDefinition.types.get("SUBREQUEST")
            for sub_form_type in ["inputs", "details"]:
                sub_form = request["form"][sub_form_type]
                if sub_form:
                    items = _get_form_items(sub_form)
                    requests.append(RequestDef(
                        "%s %s" % (request["name"], sub_form_type),
                        form_type, items))
    assert top_level_type is not None, "No top level project type in config"

    print "Adding standard forms and requests for sequencing"
    #requests = [
    #  RequestDef("Sample information", "Sequencing Sample Form",
    #        ["Name", "Description", "Genome Build"], True),
    #  RequestDef("User Registration Form", "User Information",
    #        ["Lab assocation"], True),
    #  RequestDef("Project", "Sequencing Request Form",
    #        ["Project name", ("Description", "TextArea", False),
    #         "Payment (Fund number)"]),
    #  RequestDef("Library construction inputs", "Sequencing Request Form",
    #        [("Sample preparation method", "TextArea"),
    #         "Adapters",
    #         ("Library construction method", "TextArea")]),
    #  RequestDef("Library construction details", "Sequencing Request Form",
    #        ["Quantitation method", "Concentration", "Insert size"]),
    #  RequestDef("Library validation details", "Sequencing Request Form",
    #        [("Validation results", "TextArea")]),
    #  RequestDef("Quantitation details", "Sequencing Request Form",
    #        [("Quantitation results", "TextArea")]),
    #  RequestDef("Next gen sequencing inputs", "Sequencing Request Form",
    #        [("Cycles", ["36", "76"]),
    #         ("Method", ["single", "paired end"])]),
    #  RequestDef("Next gen sequencing details", "Sequencing Request Form",
    #        ["Run folder", ("Run notes", "TextArea")]),
    #  RequestDef("Sequencing confirmation details", "Sequencing Request Form",
    #        ["Run folder", "Sequencing result",
    #         ("Results notes", "TextArea", False)]),
    #  ]

    # Specify tooltips for various form labels. These are specified here
    # to allow them to be added and modified separately from fields.
    #tooltips = {
    #  "Description": "Your descriptive name for the sample.",
    #  "Genome Build": "Organism and genome build associated with the sample.",
    #  "Sample preparation method":
    #    "Description of the method used to prepare your DNA or RNA samples.",
    #  "Adapters" : 
    #    "Relevant adaptor sequences used in library preparation. " \
    #    "This is especially important for short RNA sequencing where "\
    #    "these adaptors will need to be trimmed from your final reads.",
    #  "Library construction method":
    #    "Description of the method used to generate the sample library. "\
    #    "If you are "\
    #    "submitting DNA or RNA to be constructed here, please specify the "\
    #    "specific protocols to use.",
    #  "Quantitation method": 
    #    "Method used to quantitate your library sample preparation.",
    #  "Concentration": 
    #    "Calculated sample concentration and units.",
    #  "Insert size": 
    #    "Expected insert size for size selected libraries.",
    #  "Validation results": 
    #    "Description of method and results in validating the library. For " \
    #    "spot sequencing validation, please provide a summary of counts " \
    #    "for good and problematic clones.",
    #  }

    sample_form = None
    for i, request in enumerate(requests):
        form = request.add_form_definition(app, i == 0)
        app.sa_session.flush()
        if sample_form is None:
            sample_form = form
        rt = request.add_request_type(app, form, sample_form)
        app.sa_session.flush()
    return top_level_type

def create_sample_states(app, request_type, state_info):
    """Create the possible states a sequencing sample can be present in.
    """
    print "Creating potential sample states"
    states = [item["name"] for item in state_info]

    request_type = app.sa_session.query(app.model.RequestType).filter_by(
            name=request_type).first()
    assert request_type is not None
    for state in states:
        ss = app.sa_session.query(app.model.SampleState).filter_by(
            name=state).first()
        if ss is None:
            ss = app.model.SampleState(name=state, request_type=request_type)
            app.sa_session.add(ss)
            app.sa_session.flush()

def create_control(app, control_info, request_info, barcode_info):
    """Generate a control sample for next-gen runs.
    """
    print "Adding standard sequencing controls"
    details = dict(
            form_name = "Sample Information",
            request_type = "Control",
            request = "Controls")
            #name = "PhiX",
            #description = "control",
            #genome_build = "phix")

    form = app.sa_session.query(app.model.FormDefinition).filter_by(
            name=details["form_name"]).order_by("id DESC").first()
    assert form is not None, "Did not find a sample form to work from"

    request_type = app.sa_session.query(app.model.RequestType).filter_by(
            name=details["request_type"]).first()
    if request_type is None:
        request_type = app.model.RequestType(details["request_type"], "", form, form)
        app.sa_session.add(request_type)
        app.sa_session.flush()
    request = app.sa_session.query(app.model.Request).filter_by(
            name=details["request"]).filter_by(request_type_id=
                    request_type.id).first()
    if request is None:
        request = app.model.Request(details["request"],
                request_type=request_type)
        app.sa_session.add(request)
        app.sa_session.flush()

    control = app.sa_session.query(app.model.Sample).filter_by(
            name=control_info["name"]).filter_by(request_id=
                    request.id).first()
    if control is None:
        vals = {}
        for field in form.fields:
            info = control_info.get(field['label'].lower().replace(" ", "_"), None)
            if info:
                vals[field['name']] = info
        form_vals = app.model.FormValues(form, vals)
        control = app.model.Sample(control_info["name"],
            control_info["description"], request, form_vals)
        request_map = app.model.SampleRequestMap(control,
                request, 0, False)
        app.sa_session.add(request_map)
        app.sa_session.add(control)
        app.sa_session.flush()
    if control_info.get("barcode_type", None):
        add_control_barcode(app, control, control_info, request_info,
                            barcode_info)
    else:
        remove_control_barcode(app, control, request_info)

def remove_control_barcode(app, control, request_info):
    request_type = _get_barcode_request_type(app, request_info)
    to_remove = None
    for request in control.requests_by_map():
        if request.type == request_type:
            to_remove = request
    if to_remove:
        rs_map = app.sa_session.query(app.model.SampleRequestMap).filter_by(
            request_id=to_remove.id).filter_by(sample_id=control.id).first()
        app.sa_session.delete(rs_map)
    app.sa_session.flush()

def add_control_barcode(app, control, control_info, request_info,
                        barcode_info):
    """Potentially add barcode information to a control sample.
    """
    request_type = _get_barcode_request_type(app, request_info)
    seq = _get_barcode_seq(control_info, barcode_info)
    control_info["sequence"] = seq
    cur_request = None
    cur_request_count = 0
    for request in control.requests_by_map():
        if request.type == request_type:
            cur_request = request
            cur_request_count += 1
    if cur_request is None:
        cur_request = app.model.Request("", "", request_type=request_type)
        sample_map = app.model.SampleRequestMap(control, cur_request,
                                                cur_request_count + 1, True)
        app.sa_session.add(cur_request)
        app.sa_session.add(sample_map)
    cur_request.values = _get_form_values(app, request_type.request_form,
                                          control_info)
    app.sa_session.flush()

def _get_form_values(app, form, params):
    values = {}
    for field in form.fields:
        k = field["label"].replace(" ", "_").lower()
        values[field["name"]] = params.get(k, "")
    return app.model.FormValues(form, values)

def _get_barcode_seq(control_info, barcode_info):
    seq = None
    for bc in barcode_info:
        if bc["name"] == control_info["barcode_type"]:
            for sub_bc in bc["data"]:
                if sub_bc.has_key(control_info["barcode_id"]):
                    seq = sub_bc[control_info["barcode_id"]]
    assert seq is not None
    return seq

def _get_barcode_request_type(app, request_info):
    bc_request_name = None
    for request in request_info:
        if request.get("state", "") == "Multiplex":
            ext = "details" if len(request["form"].get("details", {})) > 0 else "inputs"
            bc_request_name = "%s %s" % (request["name"], ext)
    request_type = app.sa_session.query(app.model.RequestType).filter_by(
            name=bc_request_name).first()
    assert request_type is not None
    return request_type

def create_sqn_results(app, config):
    """Create form definition for storing high level sequencing run results.
    """
    print "Creating form for sequencing results"
    request = RequestDef(config["name"],
            model.FormDefinition.types.get("SQNRESULTS"),
            _get_form_items(config["form"]), True)
    form = request.add_form_definition(app, True)
    app.sa_session.flush()

class SimpleApp:
    def __init__(self, db_conn, file_path):
        self.model = galaxy.model.mapping.init(file_path, db_conn, engine_options={},
                                               create_tables=False)
    @property
    def sa_session( self ):
        """
        Returns a SQLAlchemy session -- currently just gets the current
        session from the threadlocal session context, but this is provided
        to allow migration toward a more SQLAlchemy 0.4 style of use.
        """
        return self.model.context.current

if __name__ == "__main__":
    parser = OptionParser()
    (options, args) = parser.parse_args()
    main(*args)
