#!/usr/bin/env python
"""Replace misentered organism form data with the correct information.
"""
import os
import sys
import ConfigParser
from optparse import OptionParser

new_path = [ os.path.join( os.getcwd(), "lib" ) ]
new_path.extend( sys.path[1:] ) # remove scripts/ from the path
sys.path = new_path

from galaxy import eggs
import galaxy.model.mapping
from galaxy import model
import pkg_resources
pkg_resources.require( "SQLAlchemy >= 0.4" )

def main(ini_file, swap_names, old_organism, new_organism):
    sa_session = get_sa_session(ini_file)
    swap_names = swap_names.split(",")
    [replace_org(n, old_organism, new_organism, sa_session) for n in swap_names]

def replace_org(name, old_org, org, sa_session):
    vals = sa_session.query(model.FormValues).filter(
            "content like '%" + name + "%'")
    for val in vals:
        print name, val.content
        for i, item in enumerate(val.content):
            if item == old_org:
                val.content[i] = org
        sa_session.flush()

def get_sa_session(ini_file):
    conf_parser = ConfigParser.ConfigParser({'here':os.getcwd()})
    conf_parser.read(ini_file)
    config = dict()
    for key, value in conf_parser.items("app:main"):
        config[key] = value
    try:
        db_con = config['database_connection']
    except KeyError:
        db_con = "sqlite:///%s?isolation_level=IMMEDIATE" % config["database_file"]
    model = galaxy.model.mapping.init(config['file_path'], db_con,
            engine_options={}, create_tables=False)
    return model.context.current

if __name__ == "__main__":
    parser = OptionParser()
    (options, args) = parser.parse_args()
    main(*args)
