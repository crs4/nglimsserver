"""Provide an interface for managing next generation sequencing samples.

The functionality of the LIMS is managed via a configuration file
defining available services, options, and sample states. See
tool-data/nglims.yaml for more details.

To enable this LIMS in the main menus, set 'use_nglims=True' in the main
universe_wsgi.ini configuration file.
"""
import operator
import collections
import itertools
import string
import re
import csv
import StringIO
from datetime import datetime, timedelta
import logging, os, string, re
from random import choice
import urlparse
import urllib
#from functools import wraps

import yaml,json
from mako.template import Template

from galaxy.web.framework.helpers import grids
from galaxy.web.base.controller import *
from galaxy.model.orm import *
from galaxy.web.form_builder import *
from galaxy import model, util

from galaxy.webapps.galaxy.controllers import requests

from email.MIMEText import MIMEText
import smtplib

log = logging.getLogger( __name__ )

WEB_NAME = "nglims"
LIMS_ORDER = ["services_to_samples", "samples_to_project", "list_projects"]

def require_sequencing(func):
    """Decorator requiring a user to be logged in with sequencing permissions.
    """
    def decorator( self, trans, *args, **kwargs ):
        if trans.user:
            roles = trans.user.all_roles()
        else:
            roles = []
        if "sequencing" in [r.name for r in roles]:
            return func( self, trans, *args, **kwargs )
        else:
            return trans.show_error_message(
                "You must be <a target='galaxy_main' href='%s'>logged in</a> "
                "with sequencing permissions</div>"
                % (url_for( controller='user', action='login')))
    return decorator

def require_admin(func):
    """Decorator requiring a user to be logged in with admin permissions.
    """
    def decorator( self, trans, *args, **kwargs ):
        if trans.user:
            user_mail = trans.user.email
        else:
            user_mail = None
        if user_mail in trans.app.config.admin_users:
            return func( self, trans, *args, **kwargs )
        else:
            return trans.show_error_message(
                "You must be <a target='galaxy_main' href='%s'>logged in</a> "
                "with admin permissions</div>"
                % (url_for( controller='user', action='login')))
    return decorator
def pass_func_name(func):
    """Pass name of function as keyword argument _func_name.

    From: http://stackoverflow.com/questions/245304/how-do-i-get-the-name-of-
    a-function-or-method-from-within-a-python-function-or-me/245333#245333
    """
    # XXX would be nice to re-include when dropping python2.4 support
    #@wraps(func)
    def _pass_name(*args, **kwds):
        kwds['_func_name'] = func.func_name
        return func(*args, **kwds)
    return _pass_name

class _FormMixin:
    """Provide functionality related to dealing with forms and form data.
    """
    def _form_label(self, display_label):
        """Retrieve the form label based on this diplay label.
        """
        return "_".join(x.lower() for x in display_label.split())

    def _add_form_label(self, widget, params=None):
        """Given a ready to apply widget, adjust the label to be non-generic.

        Also applied any passed arguments as the default value.
        """
        form_label = self._form_label(widget['label'])
        widget['widget'].name = form_label
        if params is not None:
            pvalue = params.get(form_label, "")
            # add the selected value for pull down options like select fields
            if hasattr(widget['widget'], 'multiple'):
                for i, (option, val, _) in enumerate(widget['widget'].options):
                    if option == pvalue:
                        widget['widget'].options[i] = (option, val, True)
                        break
            elif pvalue:
                widget['widget'].value = pvalue

        widget['helptext'] = widget['helptext'].replace('(Optional)', '')
        return widget

    def _get_organism_choice_widget(self, widget):
        """Retrieve a widget with a select list of organism choices.
        """
        old_widget = widget['widget']
        cur_select = old_widget.value or util.dbnames.default_value
        select_widget = ComboboxField(old_widget.name)
        if hasattr(util, "nextgen_dbkeys"):
            nextgen_dbkeys = util.nextgen_dbkeys
        else:
            nextgen_dbkeys = []
        for build_val, build_name in util.dbnames:
            if len(nextgen_dbkeys) == 0 or build_val in nextgen_dbkeys:
                select_widget.options.append(build_name)
                if build_val == cur_select:
                    select_widget.value = build_name
        select_widget.options.sort()
        widget['widget'] = select_widget
        return widget

    def _get_name_widget(self, trans, widget, project_org):
        """Provide a short default name for a sample, based on current names.

        Implements a simple naming scheme that provides short names for samples.
        Samples are named as the first and last letter of the users name, plus
        letter for uniqueness, plus a sample number. So my (Brad Chapman)
        samples would be:

        BCa1, BCa2...

        Additional BC initial would be BCb1, BCc1 and so on.
        This isn't infinately scaleable, but works with up to 26 different users
        with the same initials, so should be fine for our purposes.
        """
        # if we don't have a current value, generate and add one
        name_pat = re.compile(r'[A-Z][A-Z][a-z]\d+')
        name_size = 3
        if not widget['widget'].value:
            name_prefix = self._get_name_prefix(trans, name_size, name_pat, project_org)
            sampleq = trans.app.model.Sample
            query = trans.sa_session.query(sampleq).filter(
                  func.substr(sampleq.table.c.name, 1, name_size) == name_prefix
                  ).order_by('id DESC')
            last_sample = query.first()
            if last_sample:
                last_num = int(last_sample.name.replace(name_prefix, ""))
            else:
                last_num = 0
            new_name = "%s%s" % (name_prefix, last_num + 1)
            widget['widget'].value = new_name
        widget['helptext'] = "Assigned sample name. " + \
                "Please label submissions using this name."
        widget['widget'].readonly = True
        return widget

    def _get_name_prefix(self, trans, name_size, name_pat, project_org):
        """Retrieve the naming prefix to use for sample names.
        """
        # check for previous samples in the requests table -- either as
        # projects (submitted) or next gen input data (not yet submitted)
        check_requests = [self._project_org.name, "%s %s" %
            (self._service_org.next_gen_name(), self._service_org.input_name())]
        for request in [r for r in trans.user.requests if
                r.type.name in check_requests]:
            for sample in itertools.chain(request.samples_by_map(),
                                          request.parent_samples_by_map()):
                example_name = sample.name
                if (example_name and len(example_name) > name_size and
                        name_pat.match(example_name)):
                    return example_name[:name_size]
        # did not find anything -- generate a new name
        return self._generate_new_name_prefix(trans, name_size)

    def _generate_new_name_prefix(self, trans, name_size):
        """Generate a new prefix to use for a users sample names.
        """
        username = trans.user.username
        if username is None or len(username.strip()) == 0:
            username = trans.user.email
        username = username.replace("-", " ")
        parts = [p.strip() for p in username.split() if p.strip()]
        if len(parts) > 1:
            first_part = parts[0][0] + parts[-1][0]
        else:
            first_part = parts[0][:2]
        first_part = first_part.upper()
        assert len(first_part) == name_size - 1
        sampleq = trans.app.model.Sample
        for add_letter in list(string.lowercase):
            possible_name = first_part + add_letter
            query = trans.sa_session.query(sampleq).filter(
               func.substr(sampleq.table.c.name, 1, name_size) == possible_name)
            if query.count() == 0:
                return possible_name
        raise ValueError("Did not find an available name for username: %s" %
                username)

    def form_value_dict_from_id(self, trans, form_value_id):
        form_val = trans.sa_session.query(
                trans.app.model.FormValues).get(form_value_id)
        return self.form_value_dict(form_val)

    def form_value_dict(self, form_values):
        """Return a dictionary of values keyed by ids from a form values object.
        """
        details = {}
        for i, field in enumerate(form_values.form_definition.fields):
            field_label = self._form_label(field['label'])
            try:
                value = form_values.content[field['name']]
            except KeyError:
                value = form_values.content.get(field['label'], "")
            details[field_label] = value
        return details

    def _get_form_values(self, trans, form, params):
        """Retrieve values from our submitted form in a Galaxy-ready object.
        """
        values = {}
        for field in form.fields:
            form_label = self._form_label(field['label'])
            cur_val = params.get(form_label, '')
            if isinstance(cur_val, str):
                cur_val = util.restore_text(cur_val)
            values[field['name']] = cur_val
        return trans.app.model.FormValues(form, values)

    def _get_payment(self, d):
        """Retrieve payment, which can change names, from a dictionary.
        """
        payment_keys = [k for k in d.keys() if k.startswith("payment")]
        assert len(payment_keys) == 1, d.keys()
        pkey = payment_keys[0]
        return d[pkey]

class _RequestMixin:
    """Provide reuseable functionality for adding and querying requests.
    """
    def _new_request(self, trans, request_type, form_values, params, state):
        for check_name in ["name", "project_name", "run_folder"]:
            name = params.get(check_name, "")
            if name:
                break
        cur_request = trans.app.model.Request(name, "",
                request_type, trans.user, form_values)
        trans.sa_session.add(cur_request)
        request_event = trans.app.model.RequestEvent(cur_request, state)
        trans.sa_session.add(request_event)
        return cur_request

    def _get_request_type(self, trans, name):
        for rt in trans.sa_session.query(trans.app.model.RequestType
                ).filter_by(name=name):
            if not rt.deleted:
                return rt
        raise ValueError("Did not find a request type: %s" % name)

    def _filter_request_by_state(self, trans, query, state):
        """Build a SQL alchemy query that filters requests to a specific state.

        This handles multiple events for a request, but requires that
        timestamps are stable -- the most recent ID should have the most recent
        timestamp which is the most recent event and state.
        """
        event_q = trans.app.model.RequestEvent
        return query.join(event_q).filter(event_q.state == state).filter(
                event_q.id.in_(select(columns=[func.max(event_q.table.c.id)],
                                      from_obj=event_q.table,
                                      group_by=event_q.table.c.request_id)))

    def _text_barcode_summary(self, barcodes):
        """Provide high level summary text of barcodes present in a sample.
        """
        bc_type = ""
        bc_ids = []
        all_numbers = True
        for bc in barcodes:
            bc_type = bc["barcode_type"]
            try:
                bc_id = int(bc["barcode_id"])
            except ValueError:
                bc_id = bc["barcode_id"]
                all_numbers = False
            bc_ids.append(bc_id)
        bc_ids.sort()
        # if these are numbers, collapse them to ranges
        if all_numbers:
            # group items together that are next to each other
            # http://stackoverflow.com/questions/3429510/pythonic-way-to-convert-
            # a-list-of-integers-into-a-string-of-comma-separated-range
            groups = (list(x) for (_, x) in itertools.groupby(enumerate(bc_ids),
                lambda (i,x):i-x))
            # create ranges from each group
            bc_ids = ("-".join(map(str, (g[0][1], g[-1][1])[:len(g)]))
                      for g in groups)
        id_str = ",".join(str(i) for i in bc_ids)
        return "%s %s" % (bc_type, id_str)

class _NGLimsResultsAnalysis:
    """Provide summary information related to the results of next-gen runs.
    """
    @web.expose
    @web.require_login("manage sequencing samples")
    def list_sqn_results(self, trans, **kwd):
        """Provide a list of statistics from sequencing runs.
        """
        # Save the current grid (referer URL) as CSV for download
        if kwd.get("operation", None) == "download":
            refer_kwd = self._normalized_kwds(trans.request.referer)
            trans.response.send_redirect(web.url_for(controller=WEB_NAME,
                    action="results_to_csv", **refer_kwd))
        else:
            grid = _ResultsGrid(self._project_org)
            kwd = self._handle_grid_kwds(trans, **kwd)
            return grid(trans, **kwd)

    def _normalized_kwds(self, url):
        kwds = {}
        for key, val in urlparse.parse_qs(urlparse.urlsplit(url).query
                ).iteritems():
            if len(val) == 1:
                val = val[0]
            kwds[key] = val
        return kwds

    @web.expose
    @web.require_login("manage sequencing samples")
    def results_to_csv(self, trans, **kwd):
        """Prepare sequencing results to a downloadable CSV format.
        """
        grid = _ResultsGrid(self._project_org, web_ready=False)
        kwd = self._handle_grid_kwds(trans, **kwd)
        results = grid.apply_query_filter(trans, None, **kwd)
        out_info = StringIO.StringIO()
        writer = csv.writer(out_info)
        writer.writerow(["Run", "Lane", "Researcher", "Name", "Description",
            "Genome", "Length", "Clusters", "Reads"])
        for result in results:
            writer.writerow([result.run, result.lane, result.researcher,
                result.name, result.description, result.genome_build,
                result.read_length, result.clusters, result.reads])
        researchers = list(set([r.researcher for r in results]))
        if kwd.has_key('search'):
            ext = kwd['search'].split("_")[0]
        elif len(researchers) == 1 and researchers[0]:
            ext = researchers[0].split(" ")[-1]
        else:
            ext = datetime.now().strftime("%m%d%Y")
        out_name = "seqresults_%s" % ext
        trans.response.set_content_type("text/csv")
        trans.response.headers['Content-disposition'] = \
                "attachment; filename=%s.csv" % out_name
        return out_info.getvalue()

class _NGLimsSequencingRuns:
    """Prepare galaxy request for next-generation sequencing runs.

    Web controller functionality to prepare a next-gen sequencing run as
    a request with linked samples, and display these in a grid based view.
    """
    @web.expose
    @require_sequencing
    def list_sqn_runs(self, trans, **kwd):
        """List all submitted projects that need to be processed.
        """
        kwd = self._handle_grid_kwds(trans, **kwd)
        request_grid = _SqnRunGrid(self._service_org)
        return request_grid(trans, **kwd)

    @web.expose
    def show_sqnrun_samples(self, trans, **kwd):
        kwd = self._handle_grid_kwds(trans, **kwd)
        kwd['_include_lane'] = True
        kwd['_include_state'] = True
        kwd['sort'] = 'lane'
        rid = kwd.get('ids', '')
        if rid:
            request = trans.sa_session.query(trans.app.model.Request).get(rid)
            if request:
                kwd['request_details'] = self.form_value_dict(request.values)
        grid = _SampleDetailsGrid(self._project_org, self._service_org)
        return grid(trans, **kwd)

    @web.expose
    @require_sequencing
    @pass_func_name
    def edit_sqnrun(self, trans, **kwd):
        buttons = dict(
            save = "Submit")
        kwd = self._handle_grid_kwds(trans, **kwd)
        params = util.Params(kwd)
        rid = params.get('ids', '')
        contents = []
        if rid:
            request = trans.sa_session.query(trans.app.model.Request).get(rid)
            contents = request.values.content
        is_submit = (params.get("create_request_button", None) == buttons['save'])
        cur_form = request.values.form_definition
        if is_submit:
            messages = self._check_submission([cur_form], params)
        else:
            messages = []
        widgets = []
        for widget in cur_form.get_widgets(trans.user, contents=contents):
            widget = self._add_form_label(widget, params)
            widgets.append(widget)
        # form values to pass through
        widgets.append(dict(label="", helptext="",
                    widget=HiddenField("ids", rid)))
        options = []
        if (is_submit and not messages):
            request.values = self._get_form_values(trans, cur_form, params)
            if not self._testing:
                trans.sa_session.flush()
            trans.response.send_redirect(web.url_for(controller=WEB_NAME,
                action="list_sqn_runs"))
        else:
            if messages:
                msg = messages[0]
                msg_type = 'error'
            else:
                msg = ""
                msg_type = None
            return trans.fill_template('/nglims/form_entry.mako',
                title="Edit run information",
                form_id=kwd['_func_name'],
                form_url=web.url_for(controller=WEB_NAME,
                    action=kwd['_func_name']),
                msg=msg,
                messagetype=msg_type,
                button_name=buttons['save'],
                widgets=widgets)
        return ""

    @web.expose
    @require_sequencing
    def download_sqnrun(self, trans, **kwd):
        """Download the details of a sequencing run as a CSV file.

        Calls our REST API to get the result as JSON, and then reformats the
        results into HTML.
        """
        rid = kwd.get('ids', '')
        if rid:
            request = trans.sa_session.query(trans.app.model.Request).get(rid)
        details = self._details_from_request(trans, request)
        out_info = StringIO.StringIO()
        writer = csv.writer(out_info)
        writer.writerow(["Run", "Lane", "Name", "Description", "Researcher",
            "Concentration", "Adapter", "InsertSize", "Quantitation", "Payment"])
        for run_lane in details:
            writer.writerow([request.name, run_lane["lane"], run_lane["name"],
                run_lane["description"], run_lane.get("researcher", ""),
                run_lane.get("concentration", ""),
                run_lane.get("adapter", ""),
                run_lane.get("insertsize", ""),
                run_lane.get("quantitation", ""),
                run_lane.get("payment", "")])
        trans.response.set_content_type("text/csv")
        trans.response.headers['Content-disposition'] = \
                "attachment; filename=%s.csv" % request.name
        return out_info.getvalue()

    @web.expose
    @require_sequencing
    def download_samplesheet(self, trans, **kwd):
        """Download the details of a sequencing run as a CSV file.

        Calls our REST API to get the result as JSON, and then reformats the
        results into HTML.
        """
        rid = kwd.get('ids', '')
        if rid:
            request = trans.sa_session.query(trans.app.model.Request).get(rid)
        details = self._details_from_request(trans, request)
        out_info = StringIO.StringIO()
        writer = csv.writer(out_info)
        writer.writerow(["FCID", "Lane","SampleID", "SampleRef", "Index", "Description",
            "Control", "Recipe", "Operator", "SampleProject", "CustomerSampleID"])
        for run_lane in details:
            barcode = run_lane["multiplex"][0]
            control = 'N'
            if run_lane['sample_id'] == 1: control = 'Y'
            writer.writerow([request.name, run_lane["lane"], run_lane["name"],
                run_lane["genome_build"], barcode['sequence'],
                barcode['barcode_type'],control,
                run_lane["recipe"],
                request.user.username,
                run_lane["project_name"],
                run_lane["description"],])
        trans.response.set_content_type("text/csv")
        trans.response.headers['Content-disposition'] = \
                "attachment; filename=samplesheet_%s.csv" % request.name
        return out_info.getvalue()
    @web.expose
    @require_admin
    @pass_func_name
    def delete_sqnrun(self, trans, **kwd):
        """Delete a sequencing run
        """

        kwd = self._handle_grid_kwds(trans, **kwd)
        params = util.Params(kwd)
        rid = params.get('ids', '')
        if rid:
            request = trans.sa_session.query(trans.app.model.Request).get(rid)
            buttons = dict(delete = "Delete %s" %request.name)
        widgets = []

        # form values to pass through
        widgets.append(dict(label="", helptext="",
                    widget=HiddenField("ids", rid)))
        options = []
        is_submit = (params.get("create_request_button", None) == buttons['delete'])
        if (is_submit):
             if request:
                all_details = self._details_from_request(trans, request)
                for details in all_details:
                    sample_id = details['sample_id']
                    sample = trans.sa_session.query(trans.app.model.Sample).get(sample_id)
                    if sample is not None:
                        sample_requests = trans.sa_session.query(trans.app.model.Request).filter_by(name = sample.name).all()
                        trans.sa_session.query(trans.app.model.SampleRequestMap).filter_by(sample_id = sample_id).delete()
                        trans.sa_session.query(trans.app.model.SampleEvent).filter_by(sample_id = sample_id).delete()
                        trans.sa_session.query(trans.app.model.Sample).filter_by(name = sample.name).delete()
                        for req in sample_requests:
                            trans.sa_session.query(trans.app.model.RequestEvent).filter_by( request_id = req.id ).delete()
                        trans.sa_session.query(trans.app.model.Request).filter_by(name = sample.name).delete()
                trans.sa_session.query(trans.app.model.RequestEvent).filter_by(request_id = request.id).delete()
                trans.sa_session.query(trans.app.model.Request).filter_by(id = request.id).delete()
                trans.response.send_redirect(web.url_for(controller=WEB_NAME,action="list_sqn_runs"))
        else:
            return trans.fill_template('/nglims/form_entry.mako',
                title="Are you sure that you want to permanently delete the selected run (%s) from NGLims? </br> Deletion will remove all samples correlated" %request.name,
                form_id=kwd['_func_name'],
                form_url=web.url_for(controller=WEB_NAME,
                    action=kwd['_func_name']),
                msg='',
                messagetype=None,
                button_name=buttons['delete'],
                widgets=widgets)
        return ""
        #rid = kwd.get('ids', '')
        #if rid:
         #   request = trans.sa_session.query(trans.app.model.Request).get(rid)


            #print details

class _NGLimsStatusReport:
    """ Manage the flowcell status report
    """
    @web.expose
    @require_sequencing
    def list_flowcell_status_report(self, trans, **kwd):
        """List all flowcells status report
        """
        kwd = self._handle_grid_kwds(trans, **kwd)
        request_grid = _StatusReportGrid(self._service_org)
        return request_grid(trans, **kwd)

    @web.expose
    @require_sequencing
    def show_flowcell_status_report(self, trans, **kwd):
        """ samples status report (from single flowcell)
        """
        kwd = self._handle_grid_kwds(trans, **kwd)
        rid = kwd.get('ids', '')
        if rid:
            request = trans.sa_session.query(trans.app.model.Request).get(rid)
            if request:
                kwd['fcid'] = request.name
        grid = _FlowcellStatusReportGrid(self._service_org)
        return grid(trans, **kwd)

    @web.expose
    @require_sequencing
    def show_fastqc_report(self, trans, **kwd):
        """ samples fastqc report (from single flowcell)
        """
        kwd = self._handle_grid_kwds(trans, **kwd)
        fcid = kwd.get('fcid', '')
        rid  = kwd.get('ids', '')
        if not fcid and rid:
            request = trans.sa_session.query(trans.app.model.Request).get(rid)
            if request:
                kwd['fcid'] = request.name
        else:
            request = trans.sa_session.query(trans.app.model.Request).filter_by(name = fcid).first()
            if request:
                kwd['fcid'] = request.name
        # Save the current grid (referer URL) as CSV for download
        if kwd.get("operation", None) == "download":
            refer_kwd = self._normalized_kwds(trans.request.referer)
            trans.response.send_redirect(web.url_for(controller=WEB_NAME,
                    action="fastqc_to_csv", **refer_kwd))
        else:
            grid = _FastqcReportGrid(self._service_org)
            return grid(trans, **kwd)

    @web.expose
    @web.require_login("manage sequencing samples")
    def fastqc_to_csv(self, trans, **kwd):
        """Prepare sequencing results to a downloadable CSV format.
        """
        grid = _FastqcReportGrid(self._service_org)
        kwd = self._handle_grid_kwds(trans, **kwd)
        rid = kwd.get('ids', '')
        fcid = kwd.get('fcid', '')
        if not fcid and rid:
            request = trans.sa_session.query(trans.app.model.Request).get(rid)
            if request:
                kwd['fcid'] = request.name
        else:
            request = trans.sa_session.query(trans.app.model.Request).filter_by(name = fcid).first()
            if request:
                kwd['fcid'] = request.name

        fastqc_report = grid.apply_query_filter(trans, None, **kwd)

        out_info = StringIO.StringIO()
        writer = csv.writer(out_info)
        writer.writerow(["FCID", "Sample ID", "Sample Name", "Total Sequences", "Filtered Sequences",
            "Sequence length", "%GC"])
        for fastqc in fastqc_report:
            writer.writerow([request.name,fastqc.sample_id, fastqc.sample_name,
                fastqc.total_sequences, fastqc.filtered_sequences, fastqc.sequence_length,
                fastqc.perc_gc])
        #researchers = list(set([r.researcher for r in results]))
        #ext = datetime.now().strftime("%m%d%Y")
        out_name = "fastqc_%s" %request.name
        trans.response.set_content_type("text/csv")
        trans.response.headers['Content-disposition'] = \
                "attachment; filename=%s.csv" % out_name
        return out_info.getvalue()

    @web.expose
    @require_sequencing
    def show_pdf_status_report(self, trans, **kwd):
        """ view pdf status report (from sample)
        """
        kwd = self._handle_grid_kwds(trans, **kwd)
        rid = kwd.get('ids', '')
        if rid:
            request = trans.sa_session.query(trans.app.model.Request).get(rid)
            if request:
                 request_vals = self._project_org.form_value_dict(request.values)
                 if 'pdf' in request_vals['qc']:
                     print dir(trans.response)
                     url = '%s/%s' %(self._config["paths"][0]["pdf_status_report"],request_vals['qc'])
                     trans.response.send_redirect(web.url_for(url))

        return trans.show_warn_message(
                "<div>Sorry, quality check not available. "
                "Back to <a target='galaxy_main' href='%s'>Flowcell Status Report</a></div>"
                % (url_for( controller=WEB_NAME, action='list_flowcell_status_report')))
class _NGLimsExporting:
    """ Manage the exporting request
    """

    @web.expose
    @require_sequencing
    def list_export_request(self, trans, **kwd):
        """List all export request.
        """
        kwd = self._handle_grid_kwds(trans, **kwd)
        request_grid = _ExportRequestGrid(self._service_org)
        return request_grid(trans, **kwd)

    @web.expose
    @require_sequencing
    def show_objects_to_export(self, trans, **kwd):
        kwd = self._handle_grid_kwds(trans, **kwd)

        rid = kwd.get('ids', '')
        if rid:
            request = trans.sa_session.query(trans.app.model.Request).get(rid)
            if request:
                kwd['request_details'] = self.form_value_dict(request.values)
        grid = _ObjectsToExportGrid(self._service_org)
        return grid(trans, **kwd)

    @web.expose
    @require_sequencing
    def show_export_details(self, trans, **kwd):
        kwd = self._handle_grid_kwds(trans, **kwd)

        rid = kwd.get('ids', '')
        if rid:
            request = trans.sa_session.query(trans.app.model.Request).get(rid)
            if request:
                kwd['request_details'] = self.form_value_dict(request.values)
        grid = _ExportDetailsGrid(self._service_org)
        return grid(trans, **kwd)

    @web.expose
    @require_admin
    @pass_func_name
    def edit_export_request_status(self, trans, **kwd):
        buttons = dict(
            save = "Submit")
        kwd = self._handle_grid_kwds(trans, **kwd)
        params = util.Params(kwd)
        rid = params.get('ids', '')
        contents = []
        if rid:
            request = trans.sa_session.query(trans.app.model.Request).get(rid)
            contents = request.values.content
        is_submit = (params.get("create_request_button", None) == buttons['save'])
        cur_form = request.values.form_definition
        if is_submit:
            messages = self._check_submission([cur_form], params)
        else:
            messages = []
        widgets = []

        for widget in cur_form.get_widgets(trans.user, contents=contents):
            widget = self._add_form_label(widget, params)
            #if widget['label'] in ['Export status']:
            widgets.append(widget)
        # form values to pass through
        widgets.append(dict(label="", helptext="",
                    widget=HiddenField("ids", rid)))
        options = []
        if (is_submit and not messages):
            #request.values = self._get_form_values(trans, cur_form, params)
            status = params.get('export_status', '')
            params.object_ids = self._retrieve_value(params.get('object_ids',''))
            params.export_details = self._retrieve_value(params.get('export_details',''))
            params.export_contact = self._retrieve_value(params.get('export_contact',''))
            request.values = self._get_form_values(trans, cur_form, params)
            if not self._testing:
                 trans.sa_session.flush()
                 self.update_export_request_state(trans,rid,status)
            trans.response.send_redirect(web.url_for(controller=WEB_NAME,
                action="list_export_request"))
        else:
            if messages:
                msg = messages[0]
                msg_type = 'error'
            else:
                msg = ""
                msg_type = None
            return trans.fill_template('/nglims/form_entry.mako',
                title="Edit export request",
                form_id=kwd['_func_name'],
                form_url=web.url_for(controller=WEB_NAME,
                    action=kwd['_func_name']),
                msg=msg,
                messagetype=msg_type,
                button_name=buttons['save'],
                widgets=widgets)
        return ""

    def _retrieve_value(self,value):
        #value = value.replace("_","")
        value = value.replace("_dq_","\"")
        value = value.replace("_ob_","[")
        value = value.replace("_cb_","]")
        value = value.replace("_at_","@")
        value = value.replace("_","")
        return value
    @web.expose
    @require_admin
    @pass_func_name
    def delete_export_request(self, trans, **kwd):
        """Delete a export request
        """
        kwd = self._handle_grid_kwds(trans, **kwd)
        params = util.Params(kwd)
        rid = params.get('ids', '')
        if rid:
            request = trans.sa_session.query(trans.app.model.Request).get(rid)
            contents = request.values.content
            buttons = dict(delete_export = "Delete %s export request" %contents['field_3'])
        widgets = []

        # form values to pass through
        widgets.append(dict(label="", helptext="",
                    widget=HiddenField("ids", rid)))
        options = []
        is_submit = (params.get("create_request_button", None) == buttons['delete_export'])
        if (is_submit):
             if request:
                trans.sa_session.query(trans.app.model.RequestEvent).filter_by(request_id = request.id).delete()
                trans.sa_session.query(trans.app.model.Request).filter_by(id = request.id).delete()
                trans.sa_session.query(trans.app.model.FormValues).filter_by(id = request.form_values_id).delete()
                trans.response.send_redirect(web.url_for(controller=WEB_NAME,action="list_export_request"))
        else:
            return trans.fill_template('/nglims/form_entry.mako',
                title="Are you sure that you want to permanently delete the selected export request (%s) from NGLims?" %contents['field_3'],
                form_id=kwd['_func_name'],
                form_url=web.url_for(controller=WEB_NAME,
                    action=kwd['_func_name']),
                msg='',
                messagetype=None,
                button_name=buttons['delete_export'],
                widgets=widgets)
        return ""

    @web.expose
    @require_sequencing
    def update_export_request_state(self, trans,rid,status):
            if rid:
                request = trans.sa_session.query(trans.app.model.Request).get(rid)
                if request:
                    values = request.values
                    content = values.content

                    export_description = 'field_3'
                    if content['field_5'] in [status]: return True
                    content['field_5'] = status
                    values.content = content

                    trans.sa_session.update(values)
                    trans.sa_session.flush()

                    if trans.app.config.smtp_server is not None:
                        nglims_conf = self._config["mails"][0]
                        receivers = nglims_conf['export_request']
                        receivers.append(request.user.email)
                        #frm = trans.app.config.admin_users
                        frm = 'nglims.dontreply@crs4.it'
                        if status in ['SUCCESSFUL']:
                            body = 'Message from galaxy-nglims server. \nExport request %s from %s user (%s) has been completed successfully.' %(content[export_description],request.user.username,request.user.email)
                            subject = 'From galaxy-nglims: export request completed successfully - %s' %content[export_description]

                        elif status in ['RUNNING']:
                            body = 'Message from galaxy-nglims server. \nExport request %s from %s user (%s) is running.' %(content[export_description],request.user.username,request.user.email)
                            subject = 'From galaxy-nglims: export request running - %s' %content[export_description]

                        elif status in ['FAILED']:
                            body = 'Message from galaxy-nglims server. \nExport request %s from %s user (%s) is failed.' %(content[export_description],request.user.username,request.user.email)
                            subject = 'From galaxy-nglims: export request failed - %s' %content[export_description]

                        else:
                            return True

                        for to  in receivers:
                            try:
                                util.send_mail( frm, to, subject, body, trans.app.config )
                            except:
                                error = 'mail not sent'

                    return True
            return False


class _NGLimsPlotting:
    """Provide plots of sequencing statistics for process monitoring.
    """
    @web.expose
    def sqn_plots(self, trans, **kwd):
        return trans.fill_template('/nglims/plots.mako')

    @web.expose
    @web.json
    def sqn_plot_reads(self, trans, **kwd):
        """Get data on reads over time for plotting purposes.
        """
        #return util.json.from_json_string(urllib.urlopen(
        #    "http://galaga/galaxy/nglims/sqn_plot_reads").read())
        query = trans.sa_session.query(model.SqnRunResults)
        if trans.user:
            user_roles = [r.name for r in trans.user.all_roles()]
            if 'sequencing' not in user_roles:
                query = query.filter_by(user=trans.user)
        query = query.order_by(
              ["sqn_run_results.request_id ASC", "lane ASC", "sample_id DESC"])
        out = dict(reads=[], passing=[], lanes=dict())
        results_by_date = dict()
        for result in query:
            vals = self._project_org.form_value_dict(result.form_values)
            request_vals = self._project_org.form_value_dict(result.request.values)
            request_date_str = request_vals.get("run_folder", "").split("_")[0]
            try:
                request_date = datetime.strptime(request_date_str, "%y%m%d")
            except ValueError:
                request_date = result.request.create_time
            date = request_date.strftime("%Y/%m/%d")
            if not results_by_date.has_key(date):
                results_by_date[date] = []
            if result.user:
                if not out["lanes"].has_key(str(result.lane)):
                    out["lanes"][str(result.lane)] = []
                # reads are scaled to millions for ease of reading
                scale = float(1e6)
                if vals.get("reads", ""):
                    reads = vals["reads"]
                else:
                    reads = vals["clusters_passed"]
                reads = float(reads) / scale
                results_by_date[date].append(reads)
                out["lanes"][str(result.lane)].append([date, reads])
            clusters = None
            if vals["clusters"]:
                clusters = float(vals["clusters"]) / scale
            if clusters and clusters > reads:
                out["passing"].append([clusters, reads / clusters * 100.0,
                    date])
        for date, values in results_by_date.iteritems():
            out_values = [max(values), min(values), sum(values) / len(values)]
            out["reads"].append([date] + out_values)
        return out

    def __getPlotPath(self, run_folder, only_check):
        import subprocess
        try:
            irods_path = self._config["paths"][0]["irods"]
            cmd = ['ils', '-r', irods_path ]
            output = subprocess.check_output(cmd, stderr=subprocess.STDOUT)

            runs = output.split('\n')

            path = None

            if any(run_folder in run for run in runs):
                matching = [run for run in runs if run_folder in run]
                idir = matching[1].replace(':','')
                pathset = matching[2].strip()
                cmd = ['iget','%s/%s' %(idir,pathset),'-']
                output = subprocess.check_output(cmd, stderr=subprocess.STDOUT)
                paths = output.split('\n')
                path = paths[1].replace('file://','')
            else:
                dir_path = self._config["paths"][0]["dir"]
                cmd = ['ls', '-t', dir_path ]
                output = subprocess.check_output(cmd, stderr=subprocess.STDOUT)
                matching = [run for run in runs if run_folder in run]
                if len(matching) > 0 :
                    path = matching[0]
            if path is not None:
                first_base_report = '%s/%s' %(path.strip(),self._config["paths"][0]["first_base_report"])
                status_report = '%s/%s' %(path.strip(),self._config["paths"][0]["status_report"])

                if os.path.exists(status_report):
                    status_report = status_report.replace(self._config["paths"][0]["sequencing_data_file"],self._config["paths"][0]["sequencing_data_http"])
                else:
                    status_report = 0
                if os.path.exists(first_base_report):
                    first_base_report = first_base_report.replace(self._config["paths"][0]["sequencing_data_file"],self._config["paths"][0]["sequencing_data_http"])
                else:
                    first_base_report = 0
            else:
                status_report = 0
                first_base_report = 0

            if not only_check:
                return first_base_report,status_report
            else:
                if not first_base_report and not status_report:
                    return False
                else:
                    return True
        except:
            if not only_check:
                return False,False
            else: return False

    def _getPlotPath(self, run_folder, only_check):
            import subprocess

            path = None
            running_path = self._config["paths"][0]["dir"]
            cmd = 'ls %s | grep %s' %(running_path,run_folder)
            try:
                output = subprocess.check_output(cmd, shell=True)
            except:
                output = None

            if output is not None:
                path = output.split('\n')[0]
                path = '%s%s' %(running_path,path)
            else:
                completed_path = self._config["paths"][0]["completed"]
                cmd = 'ls %s | grep %s' %(completed_path,run_folder)
                try:
                    output = subprocess.check_output(cmd, shell=True)
                except:
                    output = None
                if output is not None:
                    path = output.split('\n')[0]
                    path = '%s%s/raw' %(completed_path,path)

            if path is not None:
                first_base_report = '%s/%s' %(path.strip(),self._config["paths"][0]["first_base_report"])
                status_report = '%s/%s' %(path.strip(),self._config["paths"][0]["status_report"])

                if os.path.exists(status_report):
                    status_report = status_report.replace(self._config["paths"][0]["sequencing_data_file"],self._config["paths"][0]["sequencing_data_http"])
                else:
                    status_report = 0
                if os.path.exists(first_base_report):
                    first_base_report = first_base_report.replace(self._config["paths"][0]["sequencing_data_file"],self._config["paths"][0]["sequencing_data_http"])
                else:
                    first_base_report = 0
            else:
                status_report = 0
                first_base_report = 0

            if not only_check:
                return first_base_report,status_report
            else:
                if not first_base_report and not status_report:
                    return False
                else:
                    return True


    @web.expose
    @web.require_login("manage sequencing samples")
    def list_sqn_plots(self, trans, **kwd):
        """Provide a list of plots from sequencing runs.
        """

        run_folder = kwd['search']
        first_base_report,status_report = self._getPlotPath(run_folder,False)
        if not first_base_report and not status_report:
            trans.response.send_redirect(web.url_for(controller=WEB_NAME,
                  action='list_sqn_runs'))
        return trans.fill_template('/nglims/sqn_plots.mako',first_base_report = first_base_report, status_report = status_report)

class _NGLimsMultiplex:
    """Handle barcoded multiplex input samples.
    """
    @web.expose
    @pass_func_name
    def add_barcode(self, trans, **kwd):
        """Provide template to associate barcodes with samples.
        """
        parent_id = None
        ids = []
        if kwd.get("parent_id", ""):
            parent_id = trans.security.decode_id(kwd["parent_id"])
        elif kwd.get("ids", ""):
            (ids, _, _) = self._normalize_ids(**kwd)
        default_type = None
        str_samples = []
        editable_samples = []
        send_parent = ""
        if parent_id:
            # get parent details for display and processing
            parent = trans.sa_session.query(trans.app.model.Sample).get(
                    parent_id)
            (sid, sname, sdescr, _) = self._make_displayready(
                    self._get_sample_details([parent]))[0]
            sample_title = "Multiplex details for %s: %s" % (sname, sdescr)
            send_parent = trans.security.encode_id(parent_id)
            # retrieve existing barcode information
            (_, request_type) = self._service_org.get_detail_widgets(trans,
                    "Barcode")
            barcodes = []
            for request in parent.requests_by_map():
                if request.type == request_type:
                    request_info = self.form_value_dict(request.values)
                    barcodes = request_info.get("barcodes", [])
            for barcode in barcodes:
                default_type = barcode['barcode_type']
                editable_samples.append(barcode["name"])
        else:
            sample_title = "Multiplex details"
            # retrieve our samples, filtering any multiplexed samples
            all_samples = [trans.sa_session.query(trans.app.model.Sample
                    ).get(sid) for sid in ids]
            all_samples = [s for s in all_samples if not
                    self._service_org.get_barcode(s).get("barcodes", None)]
            for (sid, sname, sdescr, _) in self._make_displayready(
                    self._get_sample_details(all_samples)):
                display_name = "%s %s" % (sname, sdescr)
                str_samples.append((sid, display_name))
        barcodes = []
        for i, barcode in enumerate(self._config['barcodes']):
            if default_type is None and i == 0:
                default_type = barcode['name']
            barcodes.append(barcode['name'])
        if len(editable_samples) == 0 and len(str_samples) == 0:
            editable_samples.append("")
        return trans.fill_template('/nglims/barcodes.mako',
                barcode_details='/nglims/barcodes_by_type',
                assoc_barcodes='/nglims/assoc_barcodes',
                callback_url=trans.request.referrer,
                data_store_id=kwd.get("data_store_id", ""),
                default_barcode_type=default_type,
                sample_title=sample_title,
                parent_id=send_parent,
                editable_samples=editable_samples,
                samples=str_samples, barcodes=barcodes)

    @web.expose
    @web.json
    def barcodes_by_type(self, trans, **kwd):
        """Retrieve JSON string of barcodes available for a particular type.
        """
        cur_type = kwd.get("barcode_type", "")
        barcodes = {}
        for choice in self._config["barcodes"]:
            if choice["name"] == cur_type:
                barcodes["names"] = []
                for item in choice["data"]:
                    barcodes["names"].append("%s : %s" % (item.items()[0]))
        return barcodes

    @web.expose
    @web.json
    def assoc_barcodes(self, trans, **kwd):
        """Associate barcodes with samples, redirecting to the starting state.
        """
        data = util.json.from_json_string(kwd.get('data', '{}'))
        (_, request_type) = self._service_org.get_detail_widgets(trans,
                "Barcode")
        parent_data = []
        for sid, barcode_info in data['barcodes']:
            bid, bseq = barcode_info.split(" : ")
            params = dict(barcode_type=data["barcode_type"], barcode_id=bid,
                    sequence=bseq)
            new_prefix = "NEW^^"
            if sid.startswith(new_prefix):
                name = sid.replace(new_prefix, "")
                params["name"] = name
                parent_data.append(params)
            else:
                m = self._check_and_add_sample_data(trans, sid, [], request_type,
                        False, params)
                assert len(m) == 0, m
        parent_id = kwd.get("parent_id", data.get("parent_id", ""))
        if parent_id and parent_data:
            parent_id = trans.security.decode_id(parent_id)
            m = self._check_and_add_sample_data(trans, parent_id, [], request_type,
                    False, dict(barcodes=parent_data))
            assert len(m) == 0, m
        out = dict()
        return out

class _NGLimsSequencingQueue(_NGLimsSequencingRuns):
    """Functionality for displaying queues related to sequencing states.

    This compartmentalizes the web controller functionality for displaying the
    sequencing queue, which formalizes various states samples can be in while
    being processed. This provides a grid display of the samples, along with
    buttons to add information and move from state to state.
    """
    @web.expose
    @require_sequencing
    def list_queues(self, trans, **kwd):
        """List samples present in various active sequencing queues.
        """
        kwd = self._handle_grid_kwds(trans, **kwd)
        grid = _SampleDetailsGrid(self._project_org, self._service_org,
                default_sort='update_time')
        std_filters = []
        for state in self._service_org.get_states():
            std_filters.append(
                grids.GridColumnFilter(state, args=dict(
                    controller=WEB_NAME, action='list_queues',
                    state=state)))
        grid.standard_filters = std_filters
        if not kwd.has_key('state'):
            kwd['state'] = 'Sequencing'
        if kwd['state'] in ['Sequencing confirmation', 'Complete']:
            kwd['_include_sqn_run'] = True
        elif kwd['state'] not in self._service_org.pre_received_states():
            kwd["_include_date_received"] = True
        grid.operations = self._service_org.operations_by_state(kwd['state'])
        grid.title = "Sequencing queues"
        grid.show_filter = kwd['state']
        return grid(trans, **kwd)

    def _normalize_ids(self, **kwd):
        """Manage ID information submitted in queues, keeping form state.
        """
        # comma separate list
        if isinstance(kwd['ids'], basestring) and kwd['ids'].find(','):
            kwd['ids'] = kwd['ids'].split(",")
        if not isinstance(kwd['ids'], list):
            ids = [kwd['ids']]
        else:
            ids = kwd['ids']
        cur_id = kwd.get('cur_id', None)
        if cur_id is not None:
            cur_index = ids.index(cur_id)
            try:
                next_id = ids[cur_index + 1]
            except IndexError:
                next_id = None
        elif len(ids) > 0:
            next_id = ids[0]
        else:
            next_id = None
        return ids, cur_id, next_id

    def _add_info_and_transition(self, trans, **kwd):
        """Add details to samples and move them to the next processing state.

        This requires two stages:
        1. Entering information for each of the samples.
        2. Adding this information and moving to the next queue.
        """
        ids, cur_id, next_id = self._normalize_ids(**kwd)
        widgets, request_type = self._service_org.get_detail_widgets(trans,
                kwd["state"])
        add_together = kwd.get('_add_together', False)
        # if we have a current ID, add details from our form
        messages = []
        if cur_id:
            messages = self._check_and_add_sample_data(trans, cur_id,
                    ids, request_type, add_together, kwd)
            if add_together:
                next_id = None
        # if we are done submitting forms, move our samples and get to
        # the next queue
        if cur_id and next_id is None and len(messages) == 0:
            self._move_to_next_queue(trans, ids, kwd.get('_move_next', ''))
        # otherwise, generate our form details for submission
        else:
            return self._sample_submission_form(trans, ids, cur_id, next_id,
                    widgets, messages, request_type, add_together, kwd)

    def _move_to_next_queue(self, trans, ids, next_state=None):
        """Move a list of sample IDs to the next service queue.
        """
        all_states = collections.defaultdict(int)
        for sid in ids:
            sample = trans.sa_session.query(trans.app.model.Sample).get(sid)
            if next_state:
                new_state = self._service_org.move_sample_to_state(trans, sample,
                        next_state)
            else:
                new_state = self._service_org.move_to_next_state(trans, sample)
            all_states[new_state] += 1
        states = [(count, s) for (s, count) in all_states.items()]
        states.sort(reverse=True)
        trans.response.send_redirect(web.url_for(controller=WEB_NAME,
            action="list_queues", state=states[0][1]))

    def _check_and_add_sample_data(self, trans, cur_id, all_ids, request_type,
            add_together, params):
        """Read data from form submission and add to an associated sample.
        """
        if add_together:
            all_sample_ids = all_ids
        else:
            all_sample_ids = [cur_id]
        samples = []
        for sid in all_sample_ids:
            samples.append(trans.sa_session.query(trans.app.model.Sample
                ).get(sid))
        messages = self._check_submission([request_type.request_form], params)

        if len(messages) == 0:
            if add_together:
                messages = self._add_high_level_request_data(trans, samples,
                        request_type, all_ids, params)
            else:
                messages = []
                self._add_sub_request_data(trans, samples, request_type, params)
            # submit the information
            # Comment out to test
            if not self._testing and len(messages) == 0:
                trans.sa_session.flush()
        return messages

    def _add_high_level_request_data(self, trans, samples, request_type,
            all_ids, params):
        """Add request data where we generate a new high level request.

        This handles the case where our request is a top level request
        that has samples associated with it. This will be the case for
        sequencing runs, which are new requests that re-organize a group
        of existing samples in the system.
        """
        messages = []
        form_values = self._get_form_values(trans, request_type.request_form,
                params)
        cur_request = self._new_request(trans, request_type, form_values,
                params, trans.app.model.Request.states.NEW)
        order_info = util.json.from_json_string(params.get("sorted_samples"))
        all_ids = all_ids[:]
        for lane_info, sids in order_info.iteritems():
            # convert back into 0-based indexing for storage
            lane = int(lane_info.replace("lane", "")) - 1
            lane_bcs = {}
            for sid in sids:
                sample = trans.sa_session.query(trans.app.model.Sample
                        ).get(sid)
                assert sample is not None
                lane_bcs = self._update_lane_barcodes(sample, lane_bcs)
                sample_map = trans.app.model.SampleRequestMap(sample,
                        cur_request, lane, False)
                trans.sa_session.add(sample_map)
                # sanity check to be sure we add all IDs requested
                try:
                    del all_ids[all_ids.index(str(sid))]
                except ValueError:
                    pass
            messages = self._check_lane_barcodes(lane_bcs, lane, messages)
        if len(all_ids) > 0:
            messages.append("Need to add all samples to sequencing lanes")
        return messages

    def _check_lane_barcodes(self, lane_bcs, lane, messages):
        """Add messages about issues with barcodes in a lane.
        """
        for bc, val in lane_bcs.iteritems():
            if val > 1:
                if bc is None:
                    msg = "non-barcoded samples"
                else:
                    msg = "'%s' barcodes" % bc
                messages.append("Multiple %s present in lane %s." %
                                (msg, lane + 1))
        if len(lane_bcs) > 1 and lane_bcs.has_key(None):
            messages.append("Barcoded and non-barcoded samples in lane %s"
                            % (lane + 1))
        return messages

    def _update_lane_barcodes(self, sample, lane_bcs):
        """Update collected barcodes with details from this sample.
        """
        bc_list = self._service_org.get_barcode(sample)
        if len(bc_list.get("barcodes", [])) > 0:
            bc_list = bc_list["barcodes"]
        elif bc_list.get("barcode_id", ""):
            bc_list = [bc_list]
        if len(bc_list) == 0:
            bc_strs = [None]
        else:
            bc_strs = ["%s %s" % (b["barcode_type"], b["barcode_id"])
                       for b in bc_list]
        for bc in bc_strs:
            try:
                lane_bcs[bc] += 1
            except KeyError:
                lane_bcs[bc] = 1
        return lane_bcs

    def _add_sub_request_data(self, trans, samples, request_type, params):
        """Add the regular form data for most sequencing request items.

        This deals with the standard case where a sample has several
        sub-requests, and we are adding details about one of them.
        """
        for sample in samples:
            added_values = False
            cur_request_count = 0
            # if we can find the request associated with a sample, add it
            for request in sample.requests_by_map():
                cur_request_count += 1
                if not params.get("_make_new_request", False):
                    if request.type == request_type:
                        request.values = self._get_form_values(trans,
                                request_type.request_form, params)
                        added_values = True
                        break
            if (params.get("_make_new_request", None) is False and
                    not added_values):
                raise ValueError("Did not find request type: %s" % request_type)
            # otherwise, add a new request with the details to the sample.
            if not added_values:
                form_values = self._get_form_values(trans,
                        request_type.request_form, params)
                cur_request = self._new_request(trans, request_type, form_values,
                        params, trans.app.model.Request.states.NEW)
                sample_map = trans.app.model.SampleRequestMap(sample,
                        cur_request, cur_request_count + 1, True)

    def _sample_submission_form(self, trans, ids, cur_id, next_id,
            widgets, messages, request_type, add_together, kwd):
        """Provide a form for submitting sample details, maintaining state.
        """
        # if we have problems, need to re-submit the current form
        if messages:
            work_id = cur_id
        else:
            work_id = next_id
            if kwd.get("_default_sqn_info", None):
                kwd = self._prefilled_sqn_info(trans, kwd, work_id,
                        kwd["_default_sqn_info"])
        send_widgets = []
        if not add_together:
            sample = trans.sa_session.query(trans.app.model.Sample).get(work_id)
            sample_info = self.form_value_dict(sample.values)
            send_widgets.append(dict(label="", helptext="",
                widget=_SectionLabel("%s: %s" % (sample_info["name"],
                    sample_info["description"]))))
        for w in widgets:
            send_widgets.append(self._add_form_label(w, kwd))
        # details to pass on through with the form
        for name, value in [("ids", ",".join([str(i) for i in ids])),
                            ("state", kwd["state"]),
                            ("cur_id", work_id),
                            ("rids", kwd.get("rids", None))]:
            if value:
                send_widgets.append(dict(label="", helptext="",
                    widget=HiddenField(name, value)))
        # if we are adding everything together, show all of the items we
        # will be adding to.
        if add_together and (cur_id or next_id):
            cur_samples, sort_samples, sort_order = \
                    self._sample_list(trans, ids, kwd)
        else:
            cur_samples, sort_samples, sort_order = ([], [], {})
        if messages:
            msg = messages[0]
            msg_type = 'error'
        else:
            msg = ""
            msg_type = None
        return trans.fill_template('/nglims/samples.mako',
            title=request_type.name,
            form_id=kwd['_func_name'],
            form_url=web.url_for(controller=WEB_NAME,
                action=kwd['_func_name']),
            button_name='Save',
            add_finished=False,
            current_samples=cur_samples,
            sort_sample_order=util.json.to_json_string(sort_order),
            sort_samples=sort_samples,
            widgets=send_widgets,
            msg=msg,
            messagetype=msg_type)

    def _sample_list(self, trans, ids, kwd):
        """Retrieve a list of samples, either as a sortable list or a table.
        """
        prep_items = kwd.get('_next_gen_prep', 0)
        cur_samples = []
        sort_samples = []
        all_samples = [trans.sa_session.query(trans.app.model.Sample
                ).get(sid) for sid in ids]
        if not prep_items:
            cur_samples = self._make_displayready(self._get_sample_details(
                all_samples))
            sort_order = {}
        else:
            str_samples = self._make_displayready(self._get_sample_details(
                all_samples, include_barcode=True))
            sort_samples, sort_order = self._prepare_sortable_samples(trans,
                    str_samples, prep_items, kwd)
        return cur_samples, sort_samples, sort_order

    def _prepare_sortable_samples(self, trans, str_samples, sort_items, kwd):
        """Prepare samples ready for sorting to organize on flow cells.

        This hard codes sorting logic and preparation for an illumina flow cell
        and should eventually be extracted into a configurable class.
        """
        seq_config = self._config["sequencing"]
        num_lanes = int(seq_config["lanes"])
        order_info = util.json.from_json_string(kwd.get("sorted_samples", "{}"))
        if len(order_info) == 0:
            for l in range(num_lanes):
                order_info["lane%s" % (l + 1)] = []
        else:
            assert len(order_info) == num_lanes
        sort_samples = []
        control_samples = self._make_displayready(self._get_sample_details(
            self._get_standard_controls(trans), include_barcode=True))
        str_samples.extend(control_samples)
        final_order_info = {}
        samples_to_lanes = collections.defaultdict(list)
        for l, sids in order_info.iteritems():
            final_order_info[l] = []
            for sid in sids:
                samples_to_lanes[int(sid)].append(l)
        for sid, sname, sdescr, _ in str_samples:
            lanes = samples_to_lanes[sid]
            display_name = "%s %s" % (sname, sdescr)
            if len(lanes) == 0:
                sort_samples.append((sid, display_name))
            else:
                for l in lanes:
                    final_order_info[l].append((sid, display_name))
        return sort_samples, final_order_info

    def _get_standard_controls(self, trans):
        """Retrieve the standard controls for a next gen sequence run.
        """
        request_type = trans.sa_session.query(trans.app.model.RequestType
                ).filter_by(name="Control").first()
        request = trans.sa_session.query(trans.app.model.Request).filter_by(
                request_type_id = request_type.id).first()
        return request.samples_by_map()

    @web.expose
    def sample_arrived(self, trans, **kwd):
        """Manage the arrival of samples, moving them to the proper queue.
        """
        ids, cur_id, next_id = self._normalize_ids(**kwd)
        self._move_to_next_queue(trans, ids)

    @web.expose
    @pass_func_name
    def add_construction_details(self, trans, **kwd):
        """Manage the transition from construction to validation.
        """
        kwd['state'] = kwd.get("state", "Construction")
        return self._add_info_and_transition(trans, **kwd)

    @web.expose
    @pass_func_name
    def add_validation(self, trans, **kwd):
        kwd['state'] = kwd.get("state", "Validation")
        return self._add_info_and_transition(trans, **kwd)

    @web.expose
    @pass_func_name
    def add_quantitation(self, trans, **kwd):
        kwd['state'] = kwd.get("state", "Pre-sequencing quantitation")
        return self._add_info_and_transition(trans, **kwd)

    @web.expose
    @pass_func_name
    def add_to_sequencing_run(self, trans, **kwd):
        kwd['state'] = kwd.get("state", "Sequencing")
        kwd['_add_together'] = True
        kwd['_next_gen_prep'] = 8
        return self._add_info_and_transition(trans, **kwd)

    @web.expose
    def prepare_flow_cell(self, trans, **kwd):
        # Back compatibility
        return self.add_to_sequencing_run(trans, **kwd)

    @web.expose
    @pass_func_name
    def redo_sequencing(self, trans, **kwd):
        kwd['state'] = kwd.get("state", "Sequencing confirmation")
        kwd['_move_next'] = "Sequencing"
        kwd['_make_new_request'] = True
        kwd['_default_sqn_info'] = 'Failed'
        return self._add_info_and_transition(trans, **kwd)

    def _prefilled_sqn_info(self, trans, kwd, work_id, sqn_status):
        """Add information about a sequencing run to the results form.

        This pre-fills several items with the correct values that
        can be adjusted and updated as needed in the forms.
        """
        kwd['sequencing_result'] = sqn_status
        if work_id:
            sample = trans.sa_session.query(trans.app.model.Sample
                    ).get(work_id)
            latest_sqn = self._service_org.get_latest_sqn_run(sample)
            if latest_sqn:
                kwd['run_folder'] = latest_sqn["run_folder"]
        return kwd

    @web.expose
    @pass_func_name
    def completed(self, trans, **kwd):
        kwd['state'] = kwd.get("state", "Sequencing confirmation")
        kwd['_make_new_request'] = True
        kwd['_default_sqn_info'] = 'Passed'
        return self._add_info_and_transition(trans, **kwd)

    @web.expose
    @pass_func_name
    def reactivate(self, trans, **kwd):
        """Revert the given samples back to the most recent prior state.
        """
        (ids, _, _) = self._normalize_ids(**kwd)
        all_states = collections.defaultdict(int)
        for sid in ids:
            sample = trans.sa_session.query(trans.app.model.Sample).get(sid)
            # pick the most recent state before this one, and revert back to it
            next_state = sample.events[1].state.name
            new_state = self._service_org.move_sample_to_state(trans, sample,
                    next_state)
            all_states[new_state] += 1
        states = [(count, s) for (s, count) in all_states.items()]
        states.sort(reverse=True)
        trans.response.send_redirect(web.url_for(controller=WEB_NAME,
            action="list_queues", state=states[0][1]))

class _NGLimsREST:
    """Provide a programmatic REST interface to retrieve sequencing results.

    This exposes sequencing runs and sample information at a higher level
    than the database tables for automated processing downstream.
    """
    @web.expose_api
    @require_sequencing
    def api_sequencing_runs(self, trans, **kwd):
        """Retrieve a list of available sequencing runs.
        """
        request_type_name = "%s %s" % (self._service_org.next_gen_name(),
                self._service_org.output_name())
        request_type = self._get_request_type(trans, request_type_name)
        requests = trans.sa_session.query(trans.app.model.Request
                ).filter_by(type=request_type)
        runs = []
        for request in requests:
            request_vals = self.form_value_dict(request.values)
            runs.append(request_vals['run_folder'])
        return dict(runs = runs)

    @web.expose_api
    @require_sequencing
    def api_get_nglims_config(self,trans, **kwd):
        config_file = self.app.config.get("nglims_config_file", "")
        if config_file and os.path.exists(config_file):
            in_handle = open(config_file)
            nglims_config = yaml.load(in_handle)
            in_handle.close()
            return nglims_config
        else:
            return None

    @web.expose_api
    @require_sequencing
    def api_get_new_sample_name(self, trans, **kwd):
        name_pat = re.compile(r'[A-Z][A-Z][a-z]\d+')
        name_size = 3
        name_prefix = self._get_name_prefix(trans, name_size, name_pat, self._project_org)
        sampleq = trans.app.model.Sample
        query = trans.sa_session.query(sampleq).filter(
            func.substr(sampleq.table.c.name, 1, name_size) == name_prefix
            ).order_by('id DESC')
        last_sample = query.first()
        if last_sample:
            last_num = int(last_sample.name.replace(name_prefix, ""))
        else:
            last_num = 0
        new_sample_name = "%s%s" % (name_prefix, last_num + 1)
        return new_sample_name

    @web.expose_api
    @require_sequencing
    def api_save_sample(self, trans, **kwd):

        """Save sample
        """
        kwd = kwd.get('payload')
        params = self._normalize_sample_kwds(kwd)
        cur_sample, error_msgs = self._save_service_request(trans, params)
        assert len(error_msgs) == 0, error_msgs
        barcodes = kwd.get('multiplex_barcodes', '')
        if barcodes and barcodes not in ["{}"] and cur_sample.id:
            self.assoc_barcodes(trans, data=barcodes,
                    parent_id=trans.security.encode_id(cur_sample.id))
        callback = params.get("callback", "")
        return cur_sample.id

    @web.expose_api
    @require_sequencing
    def api_samples_to_project(self, trans, **kwd):

        """Sample to project
        """
        kwd = kwd.get('payload')
        project_form = self._project_org.get_project_form(trans)
        params = util.Params(kwd)
        messages = self._check_submission([project_form], params)
        if len(messages) > 0:
                return False,messages
        else:
            self._project_org.save_project(trans, params)
            return True,messages

    @web.expose_api
    @require_sequencing
    def api_move_samples_to_next_queue(self, trans, **kwd):
        kwd = kwd.get('payload')
        ids = kwd
        all_states = collections.defaultdict(int)
        for sid in ids:
            sample = trans.sa_session.query(trans.app.model.Sample).get(sid)
            new_state = self._service_org.move_to_next_state(trans, sample)
            all_states[new_state] += 1
        states = [(count, s) for (s, count) in all_states.items()]
        states.sort(reverse=True)
        return new_state

    @web.expose_api
    @require_sequencing
    def api_add_samples_to_sequencing_run(self, trans, **kwd):
        kwd = kwd.get('payload')
        kwd['state'] = kwd.get("state", "Sequencing")
        kwd['_add_together'] = True
        kwd['_next_gen_prep'] = 8
        ids, cur_id, next_id = self._normalize_ids(**kwd)
        widgets, request_type = self._service_org.get_detail_widgets(trans,
                kwd["state"])
        add_together = kwd.get('_add_together', False)
        # if we have a current ID, add details from our form
        messages = []
        if cur_id:
            messages = self._check_and_add_sample_data(trans, cur_id,
                    ids, request_type, add_together, kwd)
            if add_together:
                next_id = None
        # if we are done submitting forms, move our samples and get to
        # the next queue
        if cur_id and next_id is None and len(messages) == 0:
            all_states = collections.defaultdict(int)
            for sid in ids:
                sample = trans.sa_session.query(trans.app.model.Sample).get(sid)
                new_state = self._service_org.move_to_next_state(trans, sample)
                all_states[new_state] += 1

        return cur_id
        #self.prepare_flow_cell(trans, **kwd)

        #return {}

    @web.expose_api
    @require_sequencing
    def api_get_api_key(self, trans, **kwd):
        if kwd.has_key('payload'): kwd = kwd.get('payload')
        email = kwd.get('user_email')
        user = trans.sa_session.query(trans.app.model.User).filter_by(email = email).first()
        if user is not None and len(user.api_keys) > 0:
            return  user.api_keys[0].key
        return None

    @web.expose_api
    @require_sequencing
    def api_get_list_flowcell(self, trans, **kwd):

        request_type_name = 'Next gen sequencing details'
        request_type = self._get_request_type(trans,request_type_name)
        request = trans.sa_session.query(trans.app.model.Request).filter_by(request_type_id = request_type.id)
        if request is None:
             return request
        else:
            fcids = [r.name for r in request]
            return fcids

    @web.expose_api
    @require_sequencing
    def api_exists_flowcell_id(self, trans, **kwd):
        if kwd.has_key('payload'): kwd = kwd.get('payload')
        kwd = kwd.get('fcid')
        flowcell_id = kwd
        request_type_name = 'Next gen sequencing details'
        request_type = self._get_request_type(trans,request_type_name)
        request = trans.sa_session.query(trans.app.model.Request).filter_by(request_type_id = request_type.id).filter_by(name = flowcell_id).first()
        if request is None:
             return False
        else:
            return True

    @web.expose_api
    @require_sequencing
    def api_exists_sample_id(self, trans, **kwd):
        if kwd.has_key('payload'): kwd = kwd.get('payload')
        sample_id = kwd.get('sample_id')
        request = trans.sa_session.query(trans.app.model.Sample).filter_by(name = sample_id).first()
        if request is None:
             return False
        else:
            return True

    @web.expose_api
    @require_sequencing
    def api_exists_sample_in_run(self, trans, **kwd):
        if kwd.has_key('payload'): kwd = kwd.get('payload')
        sample_id = kwd.get('sample_id')
        flowcell_id = kwd.get('flowcell_id')
        request = trans.sa_session.query(trans.app.model.Sample).filter_by(name = sample_id).first()
        request_type_name = 'Next gen sequencing details'
        request_type = self._get_request_type(trans,request_type_name)
        request = trans.sa_session.query(trans.app.model.Request).filter_by(request_type_id = request_type.id).filter_by(name = flowcell_id).first()
        if request is not None:
            for sample in request.samples_by_map():
                if sample.name in [sample_id]: return True

        return False

    @web.expose_api
    @require_sequencing
    def api_delete_sample(self,trans, **kwd):
        if kwd.has_key('payload'): kwd = kwd.get('payload')
        sample_id = kwd.get('sample_id')
        sample = trans.sa_session.query(trans.app.model.Sample).get(sample_id)
        requests = trans.sa_session.query(trans.app.model.Request).filter_by(name = sample.name).all()
        trans.sa_session.query(trans.app.model.SampleRequestMap).filter_by(sample_id = sample_id).delete()
        trans.sa_session.query(trans.app.model.Sample).filter_by(name = sample.name).delete()
        for request in requests:
            trans.sa_session.query(trans.app.model.RequestEvent).filter_by( request_id = request.id ).delete()
        trans.sa_session.query(trans.app.model.Request).filter_by(name = sample.name).delete()

    @web.expose_api
    @require_sequencing
    def api_flowcell_complete_details(self, trans, **kwd):
        """Given the name of a sequencing run, retrieve sample details
        """
        kwd = kwd.get('payload')
        run = kwd.get('run', None)
        details = None
        if run:
            request_type_name = 'Next gen sequencing details'
            request_type = self._get_request_type(trans,request_type_name)
            request = trans.sa_session.query(trans.app.model.Request).filter_by(request_type_id = request_type.id).filter_by(name = run).order_by("id DESC").first()
            if request:
                details = self._details_from_request(trans, request)
        if details is not None:
            out = dict(details=details,
                       run_name=request.name,
                       run_id=request.id)
        else:
            out = {"error": "Did not find sequencing run: %s" % run}
        return out

    @web.expose_api
    @require_sequencing
    def api_run_details(self, trans, **kwd):
        """Given the name of a sequencing run, retrieve sample details
        """
        if kwd.has_key('payload'): kwd = kwd.get('payload')
        run = kwd.get('run', '')
        details = None
        if run:
            request = self._service_org.sequencing_run_by_search(trans, run)
            if request:
                details = self._details_from_request(trans, request)
        if details is not None:
            out = dict(details=details,
                       run_name=request.name,
                       run_id=request.id)
        else:
            out = {"error": "Did not find sequencing run: %s" % run}
        return out



    def _details_from_request(self, trans, request):
        """Retrieve sequencing details for a run request object.
        """
        details = []
        for order, sample in request.samples_by_map(True):
            prequest = self._project_org.get_sample_project(sample)
            sample_vals = self.form_value_dict(sample.values)
            sample_vals["lane"] = order + 1
            sample_vals["sample_id"] = sample.id
            if prequest is not None:
                project_vals = self.form_value_dict(prequest.values)
                sample_vals['project_name'] = project_vals['project_name']
                sample_vals['payment'] = self._get_payment(project_vals)
                sample_vals["researcher"] = prequest.user.username
                sample_vals["researcher_id"] = prequest.user.id
                sample_vals["private_libs"] = self._get_user_libraries(trans, prequest.user)
                lab_association = ""
                if prequest.user.form_values_id is not None:
                    user_vals = self.form_value_dict_from_id(trans,
                            prequest.user.form_values_id)
                    lab_association = user_vals.get("lab_association",
                            "")
                sample_vals["lab_association"] = lab_association
            details.append(sample_vals)
            for sub_request in sample.requests_by_map():
                subr_vals = self.form_value_dict(sub_request.values)
                subr_name = sub_request.type.name
                if subr_name == "Quantitation details":
                    sample_vals["quantitation"] = \
                            subr_vals["quantitation_results"]
                elif subr_name == "Library construction inputs":
                    sample_vals["adapter"] = subr_vals["adapters"]
                elif subr_name == "Library construction details":
                    sample_vals["concentration"] = \
                            subr_vals["concentration"]
                    sample_vals["insertsize"] = \
                          subr_vals.get("insert_size",
                            subr_vals["insert_size_(including_adapters)"])
                elif subr_name == "Next gen sequencing inputs":
                    sample_vals["cycles"] = subr_vals["cycles"]
                    sample_vals["method"] = subr_vals["method"]
                    sample_vals["recipe"] = subr_vals["recipe"]
                elif subr_name == "Barcode details":
                    if subr_vals.get("barcodes", None):
                        sample_vals["multiplex"] = subr_vals["barcodes"]
                    else:
                        del subr_vals["barcodes"]
                        sample_vals["barcode"] = subr_vals
        return details

    def _get_user_libraries(self, trans, user):
        """Retrieve libraries accessible to a user, ordered by privacy.
        """
        libraries = trans.app.security_agent.get_accessible_libraries(trans, user)
        available = []
        for lib in libraries:
            roles = [r for r in lib.get_access_roles(trans) if not r.deleted]
            if len(roles) > 0:
                insert_first = user.email in [r.name for r in roles]
                info = (lib.name, roles[0].id)
                if insert_first:
                    available.insert(0, info)
                else:
                    available.append(info)
        return available

    @web.expose_api
    @require_sequencing
    def api_projects(self, trans, **kwd):
        """Retrieve sequencing projects along with high level details.
        """
        projects = []
        rows = []
        for project in self._project_org.get_projects(trans, restrict_to_user=False):
            project_info = self._project_org.form_value_dict(project.values)
            project_info['researcher'] = project.user.username
            project_info['last_updated'] = project.update_time.strftime(
                    "%b %d, %Y")
            projects.append((project.update_time, project_info))
        projects.sort(reverse=True)
        out = dict(projects=[p[1] for p in projects])
        return out

    @web.expose_api
    @require_sequencing
    def api_save_export_request(self, trans, **kwd):
        """ Export sequencing result
        """
        params = kwd.get('payload')
        request_type_name = "Export sequencing result"
        request_type = self._get_request_type(trans,request_type_name)
        #params['object_ids'] = str(params['object_ids'])
        messages = self._check_submission([request_type.request_form], params)

        if len(messages) > 0:
                return False,messages
        else:
            form_values = self._get_form_values(trans, request_type.request_form,params)
            trans.sa_session.add(form_values)
            trans.sa_session.flush()
            cur_request = self._new_request(trans, request_type, form_values,
                params, None)
            trans.sa_session.flush()

            if trans.app.config.smtp_server is not None:
                    nglims_conf = self._config["mails"][0]
                    receivers = nglims_conf['export_request']
                    body = 'Message from galaxy-nglims server. \n%s user (%s) has request a new export. Please provide for it.' %(trans.user.username,trans.user.email)
                    #frm = trans.app.config.admin_users
                    frm = 'nglims.dontreply@crs4.it'
                    subject = 'From galaxy-nglims: new export request - %s' %params['export_description']
                    for to  in receivers:
                        try:
                            util.send_mail( frm, to, subject, body, trans.app.config )
                        except:
                            error = 'mail not sent'
            return True, cur_request.id

    @web.expose_api
    @require_sequencing
    def api_update_export_request_state(self, trans, **kwd):
        """ Updatde export request state
            NEW RUNNING PAUSE FAILED SUCCESSFUL
        """
        kwd = kwd.get('payload')
        rid = kwd['rid']
        status = kwd['status']

        return self.update_export_request_state(trans,rid,status)

    @web.expose_api
    @require_sequencing
    def api_confirm_import_samplesheet(self, trans, **kwd):
        """ Send mail to confirm import samplesheet
        """
        kwd = kwd.get('payload')
        id_flowcell = kwd.get('id_flowcell')
        status      = kwd['status']

        if trans.app.config.smtp_server is not None:
                    nglims_conf = self._config["mails"][0]
                    receivers = nglims_conf['import_samplesheet']
                    body = 'Message from galaxy-nglims server. \n%s user (%s) has imported a samplesheet. Flowcell %s' %(trans.user.username,trans.user.email,id_flowcell)
                    #frm = trans.app.config.admin_users
                    frm = 'nglims.dontreply@crs4.it'
                    subject = 'From galaxy-nglims: new samplesheet. Flowcell %s' %id_flowcell
                    for to  in receivers:
                        try:
                            util.send_mail( frm, to, subject, body, trans.app.config )
                        except:
                            error = 'mail not sent'

    @web.expose_api
    @require_sequencing
    def api_save_fc_status_report(self, trans, **kwd):
        """ Save flowcell statur report
        """
        params = kwd.get('payload')
        request_type_name = "Flowcell status report"
        request_type = self._get_request_type(trans,request_type_name)
        #params['object_ids'] = str(params['object_ids'])
        messages = self._check_submission([request_type.request_form], params)
        if len(messages) > 0:
                return False,messages
        else:
            form_values = self._get_form_values(trans, request_type.request_form,params)
            request = trans.sa_session.query(trans.app.model.Request).filter_by(request_type_id = request_type.id).filter_by(name = params['name']).first()
            if request is None:
                trans.sa_session.add(form_values)
                trans.sa_session.flush()
                cur_request = self._new_request(trans, request_type, form_values,
                    params, None)
                trans.sa_session.flush()

                if  params['name'].split(' ')[1] not in ['-']:
                    name = '%s -' % params['name'].split(' ')[0]
                    old_request = trans.sa_session.query(trans.app.model.Request).filter_by(request_type_id = request_type.id).filter_by(name = name).first()
                    if old_request is not None:
                        trans.sa_session.delete(old_request)
                        trans.sa_session.flush()
                return True, cur_request.id
            else:
                request.values = form_values
                trans.sa_session.update(request)
                trans.sa_session.flush()
                return True, request.id


    @web.expose_api
    @require_sequencing
    def api_save_fastqc_report(self, trans, **kwd):
        """ Save fastqc report
        """
        params = kwd.get('payload')
        request_type_name = "FastQC Report"
        request_type = self._get_request_type(trans,request_type_name)
        #params['object_ids'] = str(params['object_ids'])
        messages = self._check_submission([request_type.request_form], params)
        if len(messages) > 0:
                return False,messages
        else:
            form_values = self._get_form_values(trans, request_type.request_form,params)
            request = trans.sa_session.query(trans.app.model.Request).filter_by(request_type_id = request_type.id).filter_by(name = params['name']).first()
            if request is None:
                trans.sa_session.add(form_values)
                trans.sa_session.flush()
                cur_request = self._new_request(trans, request_type, form_values,
                    params, None)
                trans.sa_session.flush()

                if  params['name'].split(' ')[1] not in ['-']:
                    name = '%s -' % params['name'].split(' ')[0]
                    old_request = trans.sa_session.query(trans.app.model.Request).filter_by(request_type_id = request_type.id).filter_by(name = name).first()
                    if old_request is not None:
                        trans.sa_session.delete(old_request)
                        trans.sa_session.flush()
                return True, cur_request.id
            else:
                request.values = form_values
                trans.sa_session.update(request)
                trans.sa_session.flush()
                return True, request.id

    @web.expose_api
    @require_sequencing
    def api_update_sample_form_field(self, trans, **kwd):
        """
        Update sample form field
        """
        params = kwd.get('payload')
        sample_id = params.get('sample_id',None)
        request_type_name = params.get('request_type_name',None)
        update_fields = params.get('update_fields',None)
        if sample_id and request_type_name and update_fields and type(update_fields) is dict:
            sample = trans.sa_session.query(trans.app.model.Sample).filter_by(name = sample_id).first()
            if sample:
                values = None
                form = None
                object = None
                if request_type_name == 'Sample values':
                    values = sample.values
                    form = trans.sa_session.query(trans.app.model.FormDefinition).get(values.form_definition_id)
                    object = sample
                else:
                     for request in sample.requests_by_map():
                         request_type = trans.sa_session.query(trans.app.model.RequestType).get(request.request_type_id)
                         if request_type.name == request_type_name:
                             values = request.values
                             form = request_type.request_form
                             object = request
                             break

                if form and values and object:

                    form_values_dict = self.form_value_dict(values)
                    for key,value in form_values_dict.iteritems():
                        if type(value) is list:
                           for k,v in value[0].iteritems():
                               for field,name in update_fields.iteritems():
                                   if k == field: form_values_dict[key][0][k] = name
                        else:
                            for field,name in update_fields.iteritems():
                                if form_values_dict.has_key(field):
                                    form_values_dict[field] = name
                    messages = self._check_submission([form], form_values_dict)

                    if len(messages) > 0:
                        return False,messages
                    else:
                        form_values = self._get_form_values(trans, form,form_values_dict)
                        object.values = form_values
                        trans.sa_session.update(object)
                        trans.sa_session.flush()

                        return True, object.id

        return False, "Wrong params"

    @web.expose_api
    @require_sequencing
    def api_sqn_report(self, trans, **kwd):
        """Provide a report of sequencing accomplished within a time period.
        """
        finished_state = 'Complete'
        start = datetime.strptime(kwd['start'], "%Y-%m-%dT%H:%M:%S")
        end = datetime.strptime(kwd['end'], "%Y-%m-%dT%H:%M:%S")

        samples_seen = []
        samples = []
        for sample, project in self._get_samples_by_state(trans, finished_state,
                start, end):
            if sample.id not in samples_seen:
                samples_seen.append(sample.id)
                for run in self._get_sample_runs(sample):
                    project_info = self._project_org.form_value_dict(project.values)
                    sample_info = self._project_org.form_value_dict(sample.values)
                    user_vals = self.form_value_dict_from_id(trans,
                            project.user.form_values_id)
                    project_info['researcher'] = project.user.username
                    project_info['email'] = project.user.email
                    project_info['lab_association'] = user_vals.get(
                        "lab_association", "")
                    sample_info["project"] = project_info
                    sample_info["sqn_type"] = self._project_org.sample_summary(sample)
                    sample_info["sqn_run"] = run
                    sample_info["events"] = self._get_sample_events(sample)
                    samples.append(sample_info)
        return samples

    def _get_sample_runs(self, sample):
        """Retrieve all sequencing runs and lanes for a sample in the time period.
        """
        for run in self._service_org.get_sample_sqn_runs(sample):
            notes = self._service_org.get_sqn_run_results(sample, run["run_folder"])
            if notes is not None:
                run["results_notes"] = notes
                yield run

    def _get_sample_events(self, sample):
        """Retrieve events and timestamps for this sample.
        """
        def event_time(x):
            return x.update_time.strftime("%b %d, %Y")
        out = [{"event": "arrived",
                "time": event_time(self._service_org.get_sample_arrived_event(sample))}]
        for event in sample.events:
            out.append({"event": event.state.name,
                        "time": event_time(event)})
        return out

    def _get_samples_by_state(self, trans, want_state, start, end):
        """Retrieve samples and projects
        """
        state = trans.sa_session.query(trans.app.model.SampleState).filter_by(
                name=want_state).first()
        events = trans.sa_session.query(trans.app.model.SampleEvent).filter_by(
                 sample_state_id = state.id).filter(
                 "update_time > '%s'" % start.strftime("%Y-%m-%d %H:%M:%S")
                 ).filter(
                 "update_time < '%s'" % end.strftime("%Y-%m-%d %H:%M:%S"))
        for e in events:
            sample = trans.sa_session.query(trans.app.model.Sample).get(
                    e.sample_id)
            if sample.state.name == want_state:
                request = self._project_org.get_sample_project(sample)
                yield sample, request

    @web.expose_api
    @require_sequencing
    def api_upload_sqn_run_summary(self, trans, **kwd):
        """Upload summary of a sequencing run, saving in SqnRunResults table.
        """
        form_def = trans.sa_session.query(trans.app.model.FormDefinition
                ).filter_by(type=trans.app.model.FormDefinition.types.SQNRESULTS).first()
        if form_def is None:
            return dict(error="No sequencing results form. Run setup script.")
        for lane_info in kwd["payload"]["lanes"]:
            form_content = {}
            for field in form_def.fields:
                form_content[field['name']] = lane_info["metrics"].get(
                        field["label"], "")
            cur_results = trans.sa_session.query(trans.app.model.SqnRunResults
                    ).filter_by(lane=lane_info['lane']).filter_by(
                            request_id=lane_info['request']).filter_by(
                            sample_id=lane_info['sample']).first()
            if cur_results is None:
                cur_results = trans.app.model.SqnRunResults()
                cur_results.lane = lane_info['lane']
                cur_results.request_id = lane_info['request']
                cur_results.sample_id = lane_info['sample']
                cur_results.form_values = trans.app.model.FormValues(form_def,
                        form_content)
                trans.sa_session.add(cur_results)
            else:
                cur_results.form_values.content = form_content
            if lane_info.get("researcher", ""):
                cur_results.user_id = lane_info["researcher"]
            trans.sa_session.flush()


    @web.expose_api
    @require_sequencing
    def api_get_sample_info(self, trans, **kwd):
        """ Get Sample info
        """
        out = None
        sample_details = []
        if kwd.has_key('payload'): kwd = kwd.get('payload')
        sampleCustomName =  kwd["sample_name"].strip()
        if sampleCustomName:
            search_phrase = "lower(encode(content, 'escape')) LIKE '%%%s%%'" %(sampleCustomName.lower())

            for row in result:
                sample =  trans.sa_session.query(trans.app.model.Sample).filter_by(name = row.content['field_0']).first()
                runs = []
                for run in self._service_org.get_sample_sqn_runs(sample):
                    run_folder = run['run_folder']
                    if run['id'] in runs: continue
                    runs.append(run['id'])
                    request_type_name = 'Next gen sequencing details'
                    request_type = self._get_request_type(trans,request_type_name)
                    request = trans.sa_session.query(trans.app.model.Request).filter_by(request_type_id = request_type.id).filter_by(id = run['id']).first()
                    if request is None: continue
                    all_details = self._details_from_request(trans, request)
                    for details in all_details:
                        if details['description'] in [sampleCustomName] and len(request.name) == 9  and run['run_folder'] == request.name:
                            multiplex = details['multiplex']
                            for barcode in  multiplex:
                                sample_info =  {'sample_name' : details['description'], 'sample_id' : details['name'], 'flowcell_id' : request.name, 'lane' : details['lane'], 'index' : barcode['sequence'], 'kit' : details['adapter']}
                                if sample_info not in sample_details: sample_details.append(sample_info)

        if len(sample_details) > 0:
            out = dict(details=sample_details,
                       sample_name=sampleCustomName)
        else:
            out = {"error": "Did not find sample: %s" % sampleCustomName}
        return out

    @web.expose_api
    @require_sequencing
    def api_fix_flowcell_id(self, trans, **kwd):
        """ Fix flowcell id
        """
        search_phrase = "char_length(request.name) = 10"
        request_type_name = 'Next gen sequencing details'
        request_type = self._get_request_type(trans,request_type_name)
        result = trans.sa_session.query(trans.app.model.Request).filter_by(request_type_id = request_type.id).filter(search_phrase).all()
        for row in result:
            if len(row.name.strip()) == 10:
                request = trans.sa_session.query(trans.app.model.Request).get(row.id)
                old_name = request.name
                new_name = request.name[1:]
                request.name = new_name
                trans.sa_session.update(request)
                trans.sa_session.flush()
                search_phrase = "encode(form_values.content, 'escape') LIKE '%%%s%%'" %(old_name)
                res = trans.sa_session.query(trans.app.model.FormValues).filter(search_phrase).first()
                res.content['field_0'] = new_name
                trans.sa_session.update(res)
                trans.sa_session.flush()




class NGLims(BaseUIController, _FormMixin, _RequestMixin, _NGLimsSequencingQueue,
        _NGLimsREST, _NGLimsResultsAnalysis, _NGLimsMultiplex, _NGLimsPlotting,_NGLimsExporting, _NGLimsStatusReport):
    def __init__(self, app):
        BaseUIController.__init__(self, app)
        self._testing = False
        self._load_config()
        self._err_msg = "nglims not yet configured. "\
                "Please see the documentation to enable."

    def _load_config(self):
        config_file = self.app.config.get("nglims_config_file", "")
        if config_file and os.path.exists(config_file):
            in_handle = open(config_file)
            self._config = yaml.load(in_handle)
            in_handle.close()
            self._service_org = _ServiceOrganizer(self._config, self._testing)
            self._project_org = _ProjectOrganizer(self._config, self._service_org,
                    self._testing)
        else:
            self._service_org = None

    @web.expose
    def index( self, trans, **kwd ):
        self._load_config()
        if self._service_org is None:
            return self._err_msg
        else:
            return trans.fill_template("nglims/index.mako",
               start_url=web.url_for(controller=WEB_NAME,
                   action="services_to_samples"),
               section_info=self._get_section_details(trans))

    def _get_section_details(self, trans):
        """Return the sections and items that organize our customer management.
        """
        if trans.user:
            user_roles = [r.name for r in trans.user.all_roles()]
        else:
            user_roles = []
        base_info = [
            ('Samples', [
              ('Define samples and services', web.url_for(controller=WEB_NAME,
                action="services_to_samples")),
              ('Submit samples as a project', web.url_for(controller=WEB_NAME,
                action="samples_to_project")),
              ('View projects', web.url_for(controller=WEB_NAME,
                action="list_projects")),
              ('Sequencing results', web.url_for(controller=WEB_NAME,
                action="list_sqn_results")),
              ])
            ]
        if 'sequencing' in user_roles:
            base_info.append(
            ('Sequencing', [
              ('Queues', web.url_for(controller=WEB_NAME,
                action="list_queues")),
              ('Runs', web.url_for(controller=WEB_NAME,
                action="list_sqn_runs")),
              ('Export request list', web.url_for(controller=WEB_NAME,
                action="list_export_request")),
              ('Flowcell status report', web.url_for(controller=WEB_NAME,
                action="list_flowcell_status_report")),
             ]))
        return base_info

    def _move_to_next(self, trans, **kwd):
        try:
            cur_position = LIMS_ORDER.index(kwd['form_id'])
            next_id = LIMS_ORDER[cur_position + 1]
        except (IndexError, ValueError):
            next_id = None
        if next_id:
            trans.response.send_redirect(web.url_for(controller=WEB_NAME,
                action=next_id))
        else:
            return "Finished"

    def _make_displayready(self, retrieval_fn):
        final_info = []
        for sid, name, descr, details in retrieval_fn:
            descr_parts = []
            for key, val in details.items():
                key = " ".join(key.split("_"))
                key = key[0].upper() + key[1:]
                if type(val) in [list, tuple]:
                    val = ", ".join(val)
                descr_parts.append("%s: %s" % (key, val))
            final_info.append((sid, name, descr, "; ".join(descr_parts)))
        return final_info

    @web.expose
    @web.require_login("manage sequencing samples")
    def services_to_samples(self, trans, **kwd):
        """Entry point for adding details about a sample to be sequenced.
        """
        params = self._normalize_sample_kwds(kwd)
        params = self._update_sample_entry_params(trans, params)
        widgets = self._get_sample_widgets(trans, params)
        widgets = self._widgets_by_step(trans, params, widgets,
                                        "initial")
        return trans.fill_template('/nglims/sample_entry.mako',
            form_id="services_to_samples",
            form_url=web.url_for(controller=WEB_NAME,
                action="save_sample"),
            current_samples =
                self._add_display_url(trans,
                self._add_multiplex_url(trans,
                self._add_edit_url(trans,
                self._make_displayready(
                self._get_sample_details(
                self._saved_samples(trans, **kwd)))))),
            widgets=widgets)

    @web.expose
    @web.require_login("manage sequencing samples")
    def get_sample_form_detailed(self, trans, **kwd):
        """Retrieve detailed forms for entering sample information.
        """
        params = self._normalize_sample_kwds(kwd)
        params = self._update_sample_entry_params(trans, params, False)
        widgets = self._widgets_by_step(trans, params, [], "detailed")
        return trans.fill_template("/nglims/sample_entry_detailed.mako",
                widgets=widgets)

    @web.expose
    @web.require_login("manage sequencing samples")
    def save_sample(self, trans, **kwd):
        """Save a sample with all provided form details.
        """
        params = self._normalize_sample_kwds(kwd)
        cur_sample, error_msgs = self._save_service_request(trans, params)
        assert len(error_msgs) == 0, error_msgs
        barcodes = kwd.get('multiplex_barcodes', '')
        if barcodes and barcodes not in ["{}"] and cur_sample.id:
            self.assoc_barcodes(trans, data=barcodes,
                    parent_id=trans.security.encode_id(cur_sample.id))
        callback = params.get("callback", "")
        if callback:
            callback = callback.replace("__", "&")
            trans.response.send_redirect(callback)
        else:
            trans.response.send_redirect(web.url_for(controller=WEB_NAME,
                                                     action="services_to_samples"))

    @web.expose
    @web.require_login("manage sequencing samples")
    def edit_sample(self, trans, **kwd):
        """Edit all of the details of an existing sample.
        """
        only_sample = kwd.get("onlysample", "").upper() == "TRUE"
        params = self._normalize_sample_kwds(kwd)
        params = self._update_sample_entry_params(trans, params)
        widgets = self._get_sample_widgets(trans, params)
        widgets = self._widgets_by_step(trans, params, widgets,
                                        "detailed", not only_sample)
        return trans.fill_template('/nglims/samples.mako',
            form_id="services_to_samples",
            form_url=web.url_for(controller=WEB_NAME,
                                 action="save_sample",
                                 callback=trans.request.referer.replace("&", "__")),
            current_samples = [],
            widgets=widgets)

    def _normalize_sample_kwds(self, kwd):
        # Strip out the actual name of a build from the description
        dbkey_label = self._form_label(self._service_org.dbkey_label)
        dbkey_str = kwd.get(dbkey_label, "")
        if dbkey_str:
            parts = dbkey_str.split(" (")
            dbkey_str = parts[-1][:-1]
        kwd[dbkey_label] = dbkey_str
        params = util.Params(kwd)
        return params

    def _entry_step(self, params, buttons):
        """Retrieve the current step that we are in during sample entry.
        """
        if params.get("create_request_button", None) == buttons['cont']:
            return "high-level"
        elif (params.get("create_request_button", None)  == buttons['save'] or
              params.get("create_request_button_done", "") == buttons['finished']):
            return "final"
        elif params.get("create_request_button", None) is None:
            return "initial"
        else:
            raise ValueError("Unexpected step: %s" % params)

    def _widgets_by_step(self, trans, params, widgets, cur_step,
                         include_request_widgets=True):
        """Add additional display widgets based on our current processing step.
        """
        if cur_step == "detailed":
            request_ids = []
            display_requests = []
            services = params.get("services",
                                  params.get("services[]", []))
            if not isinstance(services, list):
                services = [services]
            for cur_request_ids in [s.split(",") for s in services]:
                cur_request_ids = [int(i) for i in cur_request_ids]
                for dname in self._service_org.type_ids_to_display_name(
                        trans, cur_request_ids):
                    if dname not in display_requests:
                        display_requests.append(dname)
                request_ids.extend(cur_request_ids)
            if include_request_widgets:
                for select_widget in self._service_org.selected_widgets(trans,
                        request_ids):
                    widgets.append(self._add_form_label(select_widget, params))
            else:
                widgets.append(dict(label="", helptext="", widget=HiddenField(
                    "sampleonly", "true")))
            widgets.append(dict(label="", helptext="", widget=HiddenField(
                "services", ",".join([str(r) for r in request_ids]))))
            widgets.append(dict(label="", helptext="", widget=HiddenField(
                "service_names", ",".join(display_requests))))
        elif cur_step == "initial":
            widgets.append(dict(label="Multiplexed",
                widget=CheckboxField("is_multiplex"), helptext="",
                title="Indicate if the sample uses barcoding. "
                      "Barcode details will be entered after the sample information."))
            widgets.append(self._service_org.multiselect_request_type(trans,
                self._service_org.default_services))
        else:
            raise ValueError("Unexpected step: %s" % cur_step)
        return widgets

    def _update_sample_entry_params(self, trans, params, update_sample_vals=True):
        """Update entry parameters based on our current entry step.
        """
        # if we are editing an existing sample, load up the paramters
        if params.get("editid", ""):
            edit_id = trans.security.decode_id(params.get("editid", ""))
            sample = trans.sa_session.query(trans.app.model.Sample).get(edit_id)
            new_params = self._get_sample_params(sample)
            new_params["services"] = self._service_org.display_name_to_type_ids(
                    trans, sample.desc.split(","))
            new_params["service_names"] = sample.desc
            #new_params["create_request_button"] = buttons["cont"]
            params.update(new_params)
        else:
            params = self._update_last_params(trans, params,
                                               update_sample_vals)
        return params

    def _get_sample_widgets(self, trans, params):
        """Retrieve high level widgets associated with a sample.
        """
        widgets = []
        # load sample information from our requests sample form
        sample_form = self._service_org.get_sample_form(trans)
        if sample_form is None:
            raise ValueError(self._err_msg)
        for widget in sample_form.get_widgets(trans.user):
            widget = self._add_form_label(widget, params)
            if widget['label'] == self._service_org.dbkey_label:
                widget = self._get_organism_choice_widget(widget)
            elif widget['label'] == 'Name':
                widget = self._get_name_widget(trans, widget, self._project_org)
            widgets.append(widget)
        return widgets

    def _add_display_url(self, trans, items):
        """Add a URL for displaying additional sample information.
        """
        fitems = []
        for group in items:
            sid = group[0]
            display_url = web.url_for(controller=WEB_NAME,
                    action="show_project_samples",
                    display="sample_details",
                    id=trans.security.encode_id(sid))
            fitems.append(group + (display_url,))
        return fitems

    def _add_multiplex_url(self, trans, items):
        """Add a URL reference to associated barcodes for multiplexing.
        """
        fitems = []
        for parts in items:
            multi_url = web.url_for(controller=WEB_NAME,
                    action="add_barcode",
                    parent_id=trans.security.encode_id(parts[0]))
            fitems.append(parts + (multi_url,))
        return fitems

    def _add_edit_url(self, trans, items):
        """Add a URL callback to allow editing a sample.
        """
        fitems = []
        for sid, sname, sdescr, sdetails in items:
            edit_url = web.url_for(controller=WEB_NAME,
                    action="edit_sample",
                    editid=trans.security.encode_id(sid))
            fitems.append((sid, sname, sdescr, sdetails, edit_url))
        return fitems

    def _update_last_params(self, trans, params, update_sample_vals):
        """Retrieve the last loaded parameters to fill in form boxes.

        This provides nice defaults from the last entry to get things started.
        """
        ignore_params = ["name", "description"]
        last_sample = None
        for sample in trans.sa_session.query(trans.app.model.Sample).order_by(
                "create_time DESC"):
            if sample.request.user == trans.user:
                last_sample = sample
                break
        if last_sample:
            new_params = self._get_sample_params(last_sample, update_sample_vals,
                    ignore_params)
            params.update(new_params)
        return params

    def _get_sample_params(self, sample, update_sample_vals=True,
            ignore_params=None):
        """Retrieve a dictionary of form parameters from the given sample.
        """
        if ignore_params is None:
            ignore_params = []
        new_params = dict()
        if update_sample_vals:
            sample_info = self.form_value_dict(sample.values)
            new_params.update(sample_info)
        for request in sample.requests_by_map():
            request_info = self.form_value_dict(request.values)
            new_params.update(request_info)
        for ignore in ignore_params:
            if new_params.has_key(ignore):
                del new_params[ignore]
        return new_params

    def _save_service_request(self, trans, params):
        """Save the details about a sample and service request.

        This checks for any missing required fields, returning appropriate
        error messages. If everything is here, the request will be saved and
        an empty list is returned.
        """
        request_ids = [int(r) for r in params.get("services", "").split(",")]
        # check if we are updating an existing sample
        # if so, update the data for that sample instead of adding new data
        cur_sample = trans.sa_session.query(trans.app.model.Sample).filter_by(
                name=params.get("name", "")).first()
        if cur_sample is not None:
            request_types = self._service_org.get_request_types(trans,
                    request_ids)
            sample_vals = self._get_form_values(trans, request_types[0].sample_form,
                    params)
            cur_sample.values = sample_vals
            if params.get("sampleonly", "").lower() != "true":
                all_forms = [t.request_form for t in request_types]
                all_forms.insert(0, request_types[0].sample_form)
                for request_type in request_types:
                    self._add_sub_request_data(trans, [cur_sample], request_type, params)
            else:
                all_forms = []
            all_requests = []
        # add the sample from scratch
        else:
            all_forms, all_requests, cur_sample = self._add_new_sample(trans,
                    params, request_ids)
        messages = self._check_submission(all_forms, params)
        # if we have messages, return those because we need to fix the form.
        if messages:
            return None, messages
        # otherwise save the request
        else:
            for i, request in enumerate(all_requests):
                sample_map = trans.app.model.SampleRequestMap(cur_sample,
                        request, i, True)
            if not self._testing:
                trans.sa_session.flush()
            return cur_sample, []

    def _add_new_sample(self, trans, params, request_ids):
        """Prepare a brand new sample using supplied form parameters.
        """
        all_requests = []
        all_forms = []
        for request_type in self._service_org.get_request_types(trans,
                request_ids):
            all_forms.append(request_type.request_form)
            form_values = self._get_form_values(trans,
                    request_type.request_form, params)
            trans.sa_session.add(form_values)
            cur_request = self._new_request(trans, request_type, form_values,
                    params, trans.app.model.Request.states.NEW)
            all_requests.append(cur_request)
            # get the sample form -- all requests have the same so keep the last
            sample_form = request_type.sample_form
        all_forms.insert(0, sample_form)
        sample_vals = self._get_form_values(trans, sample_form, params)
        trans.sa_session.add(sample_vals)
        # create a new sample
        cur_sample = trans.app.model.Sample(params.get("name", ""),
                params.get("service_names", ""), all_requests[0], sample_vals)
        trans.sa_session.add(cur_sample)
        return all_forms, all_requests, cur_sample

    def _check_submission(self, forms, params):
        """Determine if the submission paramters have required form information.

        This is the error checking step of form submission, making sure all
        required items have been passed.
        """
        messages = []
        for form in forms:
            for field in form.fields:
                form_label = self._form_label(field['label'])
                cur_val = params.get(form_label, None)
                if ((not cur_val or cur_val in ["?"]) and
                        field['required'] == 'required'):
                    messages.append("Please enter information for: %s" %
                            field["label"])
        return messages

    def _saved_samples(self, trans, **kwd):
        """Retrieve information on saved samples available for submission.
        """
        event_q = trans.app.model.RequestEvent
        request_q = trans.app.model.Request
        query = trans.sa_session.query(trans.app.model.Sample)
        query = query.join(request_q).filter(request_q.user == trans.user)
        query = self._filter_request_by_state(trans, query,
                trans.app.model.Request.states.NEW)
        return query
        # XXX This needs to be a filtered query. It'll slow down once
        # we have a lot of samples.
        # samples = []
        #for sample in trans.sa_session.query(trans.app.model.Sample):
        #    if (sample.request.user == trans.user and
        #            sample.request.state ==
        #        samples.append(sample)
        #return samples

    def _get_sample_details(self, samples, include_barcode=False):
        """Retrieve high level summary for a group of samples.
        """
        summary = []
        for sample in samples:
            services = []
            for request in sample.requests_by_map():
                services.append(" ".join(request.type.name.split(" ")[:-1]))
            sample_details = self.form_value_dict(sample.values)
            name = sample_details['name']
            descr = sample_details['description']
            if include_barcode:
                barcode = self._service_org.get_barcode(sample)
                if barcode.get("barcode_id", ""):
                    descr += " [%s %s]" % (barcode["barcode_type"],
                                           barcode["barcode_id"])
                elif barcode.get("barcodes", ""):
                    descr += " [%s]" % self._text_barcode_summary(barcode["barcodes"])
                sort_item = (barcode.get("date", datetime.now()), sample.id)
            else:
                sort_item = sample.id
            out_info = dict()
            out_info['services'] = sample.desc.split(",")
            try:
                sqn_info = self._project_org.sample_summary(sample)
            except ValueError:
                sqn_info = ""
            out_info['sequencing'] = sqn_info
            summary.append((sort_item, (sample.id, name, descr, out_info)))
        if include_barcode:
            summary.sort()
        return [s for (_, s) in summary]

    @web.expose
    @web.require_login("manage sequencing samples")
    def samples_to_project(self, trans, **kwd):
        """Define project details and assign samples to your project.
        """
        buttons = dict(
            save = "Submit")
        params = util.Params(kwd)
        is_submit = (params.get("create_request_button", None) == buttons['save'])
        try:
            project_form = self._project_org.get_project_form(trans)
        except ValueError:
            return self._err_msg
        message_type = None
        if is_submit:
            messages = self._check_submission([project_form], params)
            if len(messages) > 0:
                message_type = 'error'
        else:
            messages = []
        widgets = []
        for widget in project_form.get_widgets(trans.user):
            widget = self._add_form_label(widget, params)
            widgets.append(widget)
        options = []
        for sid, sname, sdescr, sdetails in self._make_displayready(
                self._get_sample_details(self._saved_samples(trans, **kwd))):
            options.append(dict(name="%s: %s -- %s" % (sname, sdescr, sdetails),
                value=str(sid), options=[]))
        picked_vals = params.get('samples', [])
        if not picked_vals and is_submit:
            message_type = 'error'
            messages.append('Please enter information for: Samples')
        if len(options) == 0:
            message_type = 'error'
            messages.append('Please describe some samples before submitting '
                            'a project')
        widgets.append(dict(label="Samples", helptext="",
            widget=DrillDownField("samples", multiple=True,
                display="checkbox", options=options, value=picked_vals)))
        if len(messages) == 0:
            message_type = "info"
            messages.append(self._config["messages"]["dropoff"])
        if (is_submit and message_type not in ["error"]):
            self._project_org.save_project(trans, params)
            return self._move_to_next(trans, **kwd)
        else:
            if messages:
                msg = messages[0]
            else:
                msg = ""
            return trans.fill_template('/nglims/form_entry.mako',
                title="Specify project details",
                form_id="samples_to_project",
                form_url=web.url_for(controller=WEB_NAME,
                    action="samples_to_project"),
                msg=msg,
                messagetype=message_type,
                button_name=buttons['save'],
                widgets=widgets)

    def _handle_grid_kwds(self, trans, **kwd):
        """Provide redirects for various requests associated with grids.
        """
        if ('operation' in kwd or 'display' in kwd) and 'id' in kwd:
            op = kwd.get('operation', None)
            if op is None:
                op = kwd['display']
            operation = op.lower().replace(" ", "_")
            if not isinstance(kwd['id'], list):
                ids = kwd['id'].split(",")
            else:
                ids = kwd['id']
            op_ids = [trans.security.decode_id(i) for i in ids if i]
            del kwd['id']
            if kwd.has_key('operation'):
                del kwd['operation']
            else:
                del kwd['display']
            if len(op_ids) > 0:
                trans.response.send_redirect(web.url_for(controller=WEB_NAME,
                    action=operation, ids=op_ids, **kwd))
            else:
                trans.response.send_redirect(trans.request.referrer)
        elif 'f-free-text-search' in kwd:
            trans.response.send_redirect(web.url_for(controller=WEB_NAME,
                action="show_project_samples",
                search=kwd['f-free-text-search']))
        elif 'f-results-search' in kwd:
            kwd['search'] = kwd['f-results-search']
            del kwd['f-results-search']
            del kwd["sort"]
        return kwd

    @web.expose
    @web.require_login("manage sequencing samples")
    def list_projects(self, trans, **kwd):
        """List all submitted projects that need to be processed.
        """
        kwd = self._handle_grid_kwds(trans, **kwd)
        if 'sequencing' in [r.name for r in trans.user.all_roles()]:
            kwd['_allow_edit'] = True
        request_grid = _ProjectGrid(self._project_org)
        return request_grid(trans, **kwd)

    @web.expose
    def show_project_samples(self, trans, **kwd):
        kwd = self._handle_grid_kwds(trans, **kwd)
        kwd['_include_state'] = True
        if 'sequencing' in [r.name for r in trans.user.all_roles()]:
            kwd['_allow_edit'] = True
        grid = _SampleDetailsGrid(self._project_org, self._service_org)
        return grid(trans, **kwd)

    @web.expose
    def sample_details(self, trans, **kwd):
        """Display a sample and associated requests. For HTML output.
        """
        allow_edit = kwd.get("edit", "").upper() == "TRUE"
        sample = trans.sa_session.query(trans.app.model.Sample).get(
                kwd['ids'])
        sample_info = self.form_value_dict(sample.values)
        services, service_details, service_ids = \
                self._saved_sample_details(sample)
        edit_urls = dict()
        base_edit = None
        if allow_edit:
            base_edit = web.url_for(controller=WEB_NAME, action="edit_sample",
                                    editid=trans.security.encode_id(sample.id),
                                    onlysample=True)
            for sname, sid_group in service_ids.iteritems():
                edit_urls[sname] = []
                # special case of barcode editing
                if sname == "Barcode":
                    assert len(sid_group) == 1
                    edit_urls[sname].append(web.url_for(
                        controller=WEB_NAME, action="add_barcode",
                        parent_id=trans.security.encode_id(sample.id)))
                # standard case
                else:
                    for sids in sid_group:
                        edit_urls[sname].append(web.url_for(controller=WEB_NAME,
                            action="edit_request",
                            sid=trans.security.encode_id(sample.id),
                            rids=util.json.to_json_string(
                                [trans.security.encode_id(i) for i in sids])))
        sqn_runs = self._service_org.get_sample_sqn_runs(sample)
        ignore = ["name"]
        ready_sample_info = []
        for key, value in sorted(sample_info.iteritems()):
            if key not in ignore and value:
                new_key = key[0].upper() + " ".join(key[1:].split("_"))
                ready_sample_info.append([new_key, value])
        return trans.fill_template('/nglims/sample_details.mako',
                name=sample_info["name"],
                sample_info=ready_sample_info,
                services=services,
                sqn_runs=sqn_runs,
                service_details=service_details,
                base_edit=base_edit,
                edit_urls=edit_urls)

    @web.expose
    @pass_func_name
    def edit_request(self, trans, **kwd):
        """Edit an existing request associated with a sample.
        """
        messages = []
        new_request = True
        # handle editing a top level project request
        if kwd.get("edit", "").upper() == "TRUE":
            request_ids = [kwd['ids']]
            kwd["rids"] = util.json.to_json_string(
                [trans.security.encode_id(i) for i in request_ids])
            kwd["sid"] = ""
            add_together = True
        else:
            request_ids = [trans.security.decode_id(i) for i in
                           util.json.from_json_string(kwd['rids'])]
            add_together = False
        # check if we are saving existing information
        if kwd.get('create_request_button', None) == 'Save':
            new_request = False
            # standard addition of requests to samples
            if kwd.get("sid", ""):
                cur_id = kwd.get('cur_id', None)
                kwd["_make_new_request"] = False
                for request_id in request_ids:
                    request = trans.sa_session.query(trans.app.model.Request).get(request_id)
                    messages.extend(self._check_and_add_sample_data(trans, cur_id, [cur_id],
                            request.type, False, kwd))
            # top level addition of a request only
            else:
                add_together = True
                messages = self._edit_top_level_request(trans, kwd, request_ids[0])
        # if a new request or there are errors, present the edit form
        if new_request or len(messages) > 0:
            kwd["state"] = "Edit"
            sample_id = trans.security.decode_id(kwd['sid']) if kwd.get("sid", "") else ""
            widgets = []
            for request_id in request_ids:
                request = trans.sa_session.query(trans.app.model.Request).get(request_id)
                # add existing values for display
                request_info = self.form_value_dict(request.values)
                kwd.update(request_info)
                widgets.extend(request.type.request_form.get_widgets(trans.user))
            if new_request:
                messages = []
            return self._sample_submission_form(trans, [sample_id],
                    sample_id, sample_id, widgets, messages, request.type,
                    add_together, kwd)
        else:
            if kwd.get("cur_id", None):
                sample = trans.sa_session.query(trans.app.model.Sample).get(kwd["cur_id"])
            else:
                sample = None
            self._redirect_to_edit(trans, sample)

    def _edit_top_level_request(self, trans, kwd, rid):
        """Edit a high level requests like project information.
        """
        params = util.Params(kwd)
        request = trans.sa_session.query(trans.app.model.Request).get(rid)
        cur_form = request.values.form_definition
        messages = self._check_submission([cur_form], params)
        if len(messages) == 0:
            request.values = self._get_form_values(trans, cur_form, params)
            if not self._testing:
                trans.sa_session.flush()
        return messages

    def _redirect_to_edit(self, trans, sample):
        project_id = None
        if sample:
            for parent in sample.parent_requests_by_map():
                if parent.type.name == 'Project':
                    project_id = parent.id
                    break
        if project_id:
            trans.response.send_redirect(web.url_for(controller=WEB_NAME,
                action="show_project_samples", ids=project_id,
                _allow_edit="true"))
        else:
            trans.response.send_redirect(web.url_for(controller=WEB_NAME,
                action="list_projects"))

    @web.expose
    @pass_func_name
    def modify_state(self, trans, **kwd):
        """Modify the state of passed constructs; an admin level function.
        """
        (ids, _, _) = self._normalize_ids(**kwd)
        new_state = kwd.get('new_state', None)
        samples = []
        for sid in ids:
            samples.append(trans.sa_session.query(trans.app.model.Sample).get(sid))
        # if we have a state, move the samples over to it
        if new_state:
            sample = None
            for sample in samples:
                new_state = self._service_org.move_sample_to_state(trans, sample,
                        new_state)
            self._redirect_to_edit(trans, sample)
        # otherwise display a form which allows us to select a state
        else:
            messages = []
            widgets = []
            select = SelectField("new_state")
            for state in self._service_org.get_states():
                select.add_option(state, state)
            widgets.append(dict(label="New state", helptext="",
                    widget=select))
            widgets.append(dict(label="", helptext="",
                    widget=HiddenField("ids", ",".join([str(i) for i in ids]))))
            if messages:
                msg = messages[0]
                msg_type = 'error'
            else:
                msg = ""
                msg_type = None
            return trans.fill_template('/nglims/samples.mako',
                title="Modify state",
                form_id=kwd['_func_name'],
                form_url=web.url_for(controller=WEB_NAME,
                    action=kwd['_func_name']),
                button_name='Save',
                add_finished=False,
                current_samples=self._add_display_url(trans,
                                self._add_empty(
                                self._add_empty(
                                self._make_displayready(
                                self._get_sample_details(
                                samples))))),
                sort_sample_order=None,
                sort_samples=None,
                widgets=widgets,
                msg=msg,
                messagetype=msg_type)

    def _add_empty(self, items):
        fitems = []
        for item in items:
            item = item + ("",)
            fitems.append(item)
        return fitems

    def _saved_sample_details(self, sample):
        """Retrieve details on all information available for a sample.
        """
        services = []
        full_services_seen = []
        service_info = dict()
        service_ids = dict()
        for request in sample.requests_by_map():
            request_info = self.form_value_dict(request.values)
            service_name = " ".join(request.type.name.split(" ")[:-1])
            if service_name not in services:
                services.append(service_name)
                service_info[service_name] = []
                service_ids[service_name] = []
            # if we've already seen this service, add a new version
            # to avoid overwriting
            if (request.type.name in full_services_seen or
                    len(service_info[service_name]) == 0):
                service_info[service_name].append(request_info)
                service_ids[service_name].append([request.id])
            # otherwise update the combined version
            else:
                service_info[service_name][-1].update(request_info)
                service_ids[service_name][-1].append(request.id)
            full_services_seen.append(request.type.name)
        services = self._service_org.order_services(services)
        return services, service_info, service_ids

class _SampleDetailsGrid(grids.Grid, _RequestMixin):
    """Display details for a sample request within a project.
    """
    template = "/nglims/sample_details_grid.mako"
    model_class = model.Sample
    def __init__(self, project_org, service_org, default_sort=None):
        self.columns = [
            grids.GridColumn("ID", key="id", visible=False),
            grids.GridColumn("Name", key="name",
                 link=(lambda item: dict(display="sample_details", id=item.id))),
            grids.GridColumn("Description", key="description"),
            grids.GridColumn("Researcher", key="researcher"),
            grids.GridColumn("Sequencing", key="seq_type"),
            grids.GridColumn("Services", key="services"),
            grids.GridColumn("Barcode", key="barcode"),
            grids.GridColumn("Last update", key="update_time"),
            ]
        self.operations = []
        grids.Grid.__init__(self)
        self._project_org = project_org
        self._service_org = service_org
        self._default_sort = default_sort

    def __call__(self, trans, **kwargs):
        # save sort before the call, and handle it later
        self._cur_sort = kwargs.get('sort', None)
        # override our table class to handle not having the column
        # add it in, and then take it back out so sort won't complain
        # XXX This is a bad hack to work around non-db derived tables
        next_sort = None
        if self._cur_sort:
            if self._cur_sort.startswith("-"):
                name = self._cur_sort[1:]
            else:
                name = self._cur_sort
            self.model_class.table.c.add(Column(name))
        out = grids.Grid.__call__(self, trans, **kwargs)
        if self._cur_sort:
            self.model_class.table.c.remove(Column(name))
        return out

    def apply_query_filter(self, trans, query, **kwd):
        if kwd.has_key('_include_lane'):
            self.columns.insert(0,
                    grids.GridColumn("Lane", key="lane"))
        if kwd.has_key('_include_sqn_run'):
            self.columns.insert(-1,
                    grids.GridColumn("Run", key="sqn_run"))
        if kwd.has_key('_include_state'):
            self.columns.insert(-1,
                    grids.GridColumn("State", key='state'))
        if kwd.has_key("_include_date_received"):
            self.columns.insert(-1,
                    grids.GridColumn("Received", key="received"))
        if kwd.has_key("_allow_edit"):
            self.columns.append(grids.GridColumn("", key="edit_barcodes",
                    link = (lambda item: dict(display="add_barcode",
                        id=item.id, parent_id=trans.security.encode_id(item.id)))))
            self.columns.append(grids.GridColumn("", key="edit",
                    link = (lambda item: dict(display="sample_details",
                        id=item.id, edit="true"))))
            self.operations.append(grids.GridOperation("Modify state"))
        if self._default_sort is None:
            for column in self.columns:
                column.sortable=False
        if kwd.has_key('ids'):
            items = self._samples_by_project(trans, kwd['ids'])
        elif kwd.has_key('search'):
            items = self._samples_by_search(trans, kwd['search'])
        else:
            items =  self._samples_by_state(trans, kwd['state'])
        return _QueryLikeList(items)

    def _samples_by_search(self, trans, search_term):
        """Retrieve samples matching a provided search term.

        This implements free-text-searching of samples for quickly finding items
        of interest.

        Finds the term using a case-insensitive search in either:
        sample name
        user name or user e-mail
        within the specifications of the sample (description text)

        The description text search is hairy because the underlying column is a
        blob not text, so it needs to be converted to text before comparing.
        """
        model = trans.app.model
        if search_term == 'search':
            search_term = ''
        query = trans.sa_session.query(model.Sample).join(
                model.Request)
        query = self._filter_request_by_state(trans, query,
                model.Request.states.SUBMITTED)
        user_roles = [r.name for r in trans.user.all_roles()]
        if 'sequencing' not in user_roles:
            query = query.filter(model.Request.user == trans.user)
        if search_term:
            query = query.join(model.User).join(model.Sample.values)
            term = '%' + search_term.lower() + '%'
            # SQLite has good handling of Blobs
            if trans.app.model.engine.name in ["sqlite"]:
                search_phrase = func.lower(model.FormValues.content).like(term)
            elif trans.app.model.engine.name in ["postgres"]:
                # Use escape since not all postgres have UTF8. Won't internationalize
                # but should work for our needs
                search_phrase = "lower(encode(form_values.content, 'escape')) LIKE '%s'" % (term)
            # others (MySQL tested) require conversion
            else:
                search_phrase = "LOWER(CONVERT(form_values.content USING utf8)) LIKE '%s'" % (term)
            query = query.filter(or_(
                func.lower(model.Sample.name).like(term),
                search_phrase,
                func.lower(model.User.username).like(term),
                func.lower(model.User.email).like(term),
                ))
        self.title = "Search: %s" % search_term
        return self._rows_from_samples([(0, s) for s in query])

    def _samples_by_state(self, trans, state):
        """Retrieve all samples beloning to a particular state.
        """
        pass_samples = []
        for sample in trans.sa_session.query(trans.app.model.Sample):
            if sample.state and sample.state.name == state:
                pass_samples.append((0, sample))
        return self._rows_from_samples(pass_samples)

    def _samples_by_project(self, trans, project_id):
        """Retrieve all samples belonging to a specified project.
        """
        request = trans.sa_session.query(trans.app.model.Request).get(
                project_id)
        rows = self._rows_from_samples(request.samples_by_map(
            include_order=True))
        # side effect. Ugh.
        request_dict = self._project_org.form_value_dict(request.values)
        name = ""
        for toget in ["project_name", "run_folder"]:
            name = request_dict.get(toget, "")
            if name:
                break
        self.title = "%s -- %s" % (name, request.user.username)
        return rows

    def _rows_from_samples(self, samples):
        rows = []
        # set up our sorting parameter
        if self._cur_sort and self._cur_sort.startswith("-"):
            reverse = True
            cur_sort = self._cur_sort[1:]
        elif self._cur_sort:
            reverse = False
            cur_sort = self._cur_sort
        else:
            reverse = False
            cur_sort = self._default_sort

        for lane, sample in samples:
            sample_info = self._project_org.form_value_dict(sample.values)
            try:
                seq_type = self._project_org.sample_summary(sample)
            except ValueError:
                seq_type = ""
            services = sample.desc.split(",")
            latest_sqn = self._service_org.get_latest_sqn_run(sample)
            barcode = self._service_org.get_barcode(sample)
            arrived_event = self._service_org.get_sample_arrived_event(sample)
            user = ""
            state = ""
            utime = ""
            atime = ""
            cur_sqn = ""
            if sample.request.user:
                user = sample.request.user.username
            if sample.state:
                state = sample.state.name
            if len(sample.events) > 0:
                utime = sample.events[0].update_time.strftime("%b %d, %Y")
            if arrived_event:
                atime = arrived_event.update_time.strftime("%b %d, %Y")
            if latest_sqn:
                cur_sqn = latest_sqn["run_folder"]
            if barcode.get("barcodes", ""):
                barcode_str = self._text_barcode_summary(barcode["barcodes"])
            else:
                barcode_str = "%s %s" % (barcode.get("barcode_type", ""),
                                         barcode.get("barcode_id", ""))
            rows.append(_GridRowSample(sample.id, sample_info['name'],
                sample_info['description'], user, state,
                seq_type, ", ".join(services), utime, atime, cur_sqn,
                lane + 1, # 1-based display
                barcode=barcode_str,
                num_lanes=1,
                sort_attr=cur_sort
                ))
        # finally, actually sort our items based on the passed in sort key
        if cur_sort:
            rows.sort(reverse=reverse)
        return rows

class _GridRowSample:
    def __init__(self, id, name, description, researcher, state, seq_type,
            services, update_time, received, sqn_run, lane, barcode,
            num_lanes=1, sort_attr=None):
        self.id = id
        self.name = name
        self.description = description
        self.researcher = researcher
        self.state = state
        self.seq_type = seq_type
        self.services = services
        self.update_time = update_time
        self.received = received
        self.sqn_run = sqn_run
        self.lane = lane
        self.barcode = barcode
        self.num_lanes = num_lanes
        self.edit_barcodes = "Edit barcodes"
        self.edit = "Edit"
        if sort_attr == 'None':
            sort_attr = None
        self._sort_attr = sort_attr or 'update_time'

    def __cmp__(self, other):
        if isinstance(other, _GridRowSample):
            # handle time based sorting
            if self._sort_attr == "update_time":
                try:
                    cmp_one = datetime.strptime(self.update_time, "%b %d, %Y")
                except ValueError:
                    cmp_one = datetime(1900, 1, 1)
                try:
                    cmp_two = datetime.strptime(other.update_time, "%b %d, %Y")
                except ValueError:
                    cmp_two = datetime(1900, 1, 1)
            else:
                cmp_one = getattr(self, self._sort_attr)
                cmp_two = getattr(other, self._sort_attr)
        else:
            cmp_one = self.id
            cmp_two = other
        return cmp(cmp_one, cmp_two)

class _PagingMixin:
    def _paging(self, query, kwd):
        page_num = kwd.get('page', '1')
        if page_num != 'all':
            total_count = query.count()
            query = query.limit(self.num_rows_per_page).offset((int(page_num) - 1) *
                    self.num_rows_per_page)
        else:
            total_count = None
        return query, total_count

    def _paging_collection(self,array,kwd):
        result = {}
        page_num = kwd.get('page', '1')
        if page_num != 'all':
            i = 0
            for k,v in array.iteritems():
                min = (int(page_num)-1)*self.num_rows_per_page
                max = min+self.num_rows_per_page
                if i in range(min,max):
                    result[k] = v
                i+=1
            import collections
            result = collections.OrderedDict(sorted(result.items(),reverse=True))
            return result, i
        else:
            total_count = None
        return array, total_count

class _ObjectsToExportGrid(grids.Grid, _RequestMixin, _FormMixin, _PagingMixin):
    """Display a grid with object list to export
    """

    title = ""
    model_class = model.Request
    num_rows_per_page = 25
    use_paging = True
    columns = [
        grids.GridColumn("SampleID", key="object"),
        grids.GridColumn("SampleName", key="sample_name"),
        #grids.GridColumn("Type", key="type"),
        #grids.GridColumn("User", key='user'),
        #grids.GridColumn("Date", key="date"),
        #grids.GridColumn("Status", key="status"),
        grids.GridColumn("ID", key="id", visible=False),
    ]

    def __init__(self, service_org):
        grids.Grid.__init__(self)
        for column in self.columns:
            column.sortable=False
        self._service_org = service_org

    def apply_query_filter(self, trans, query, **kwd):
        rows = []
        total_count = None
        user_roles = [r.name for r in trans.user.all_roles()]

        details = kwd['request_details']
        self.title = "%s -- Samples to Export" %details['export_description']
        if 'sequencing' in user_roles:
            objects = eval(details['object_ids'])
            for object in objects:
                type = 'Sample'
                request_type_name = 'Next gen sequencing details'
                request_type = self._get_request_type(trans,request_type_name)
                if trans.sa_session.query(trans.app.model.Request).filter_by(request_type_id = request_type.id).filter_by(name = object).first() is not None:
                    type = 'Flowcell'

                if type in ['Sample']:
                    sample = trans.sa_session.query(trans.app.model.Sample).filter_by(name = object).first()
                    values = sample.values
                    sample_name = values.content['field_1']
                rows.append(_GridRowObjectToExport(
                    object,
                    sample_name))
        return _QueryLikeList(rows, total=total_count)

class _GridRowObjectToExport:
    def __init__(self, object_id, sample_name):
        self.id =  object_id
        self.object = object_id
        self.sample_name = sample_name
        #self.export_mode = export_mode
        #self.export_details = export_details
        #self.user = user
        #self.date = date
        #self.status = status
        #self.description = description

class _ExportDetailsGrid(grids.Grid, _RequestMixin, _FormMixin, _PagingMixin):
    """Display a grid with object list to export
    """

    title = ""
    model_class = model.Request
    num_rows_per_page = 25
    use_paging = True
    columns = [
        grids.GridColumn("Export Mode", key="export_mode"),
        grids.GridColumn("Details", key="export_details"),
        grids.GridColumn("Contact", key="export_contact"),
        grids.GridColumn("ID", key="id", visible=False),
    ]

    def __init__(self, service_org):
        grids.Grid.__init__(self)
        for column in self.columns:
            column.sortable=False
        self._service_org = service_org

    def apply_query_filter(self, trans, query, **kwd):
        rows = []
        total_count = None
        user_roles = [r.name for r in trans.user.all_roles()]

        details = kwd['request_details']
        self.title = "%s -- Export Details" %details['export_description']
        if 'sequencing' in user_roles:
                rows.append(_GridRowExportDetails(
                    details['export_mode'],
                    details['export_details'],
                    details['export_contact']))
        return _QueryLikeList(rows, total=total_count)

class _GridRowExportDetails:
    def __init__(self, export_mode, export_details, export_contact):
        self.id =  export_mode
        self.export_mode = export_mode
        self.export_details = export_details
        self.export_contact = export_contact
        #self.export_mode = export_mode
        #self.export_details = export_details
        #self.user = user
        #self.date = date
        #self.status = status
        #self.description = description

class _ExportRequestGrid(grids.Grid, _RequestMixin, _FormMixin, _PagingMixin):
    """Display a high level grid with various export request submitted.
    """
    title = "Export Request Queue"
    model_class = model.Request
    num_rows_per_page = 25
    use_paging = True
    columns = [
        grids.GridColumn("Description", key="description"),
        grids.GridColumn("Samples", key="object_list",link = (lambda item: dict(operation="show_objects_to_export",
                    id=item.id))),
        grids.GridColumn("User", key='user'),
        grids.GridColumn("Date", key="date"),
        grids.GridColumn("Status", key="status"),
        grids.GridColumn("", key='details',link = (lambda item: dict(operation="show_export_details",
                    id=item.id))),
        grids.GridColumn("", key='edit',link = (lambda item: dict(operation="edit_export_request_status",
                    id=item.id))),
        grids.GridColumn("", key='delete',link = (lambda item: dict(operation="delete_export_request",
                    id=item.id))),
        grids.GridColumn("ID", key="id", visible=False),
    ]

    def __init__(self, service_org):
        grids.Grid.__init__(self)
        for column in self.columns:
            column.sortable=False
        self._service_org = service_org

    def apply_query_filter(self, trans, query, **kwd):
        rows = []
        total_count = None
        user_roles = [r.name for r in trans.user.all_roles()]
        if 'sequencing' in user_roles:
            request_type_name = "Export sequencing result"
            try:
                request_type = self._get_request_type(trans, request_type_name)
            except ValueError:
                request_type = None
            if request_type:
                requests = trans.sa_session.query(trans.app.model.Request
                        ).filter_by(type=request_type
                        ).order_by("create_time DESC")
                requests, total_count = self._paging(requests, kwd)
            else:
                requests = []
            for request in requests:
                request_vals = self.form_value_dict(request.values)
                user = request.user
                rows.append(_GridRowExportRequest(request.id,
                    len(eval(request_vals['object_ids'])),
                    user.username,
                    request_vals['export_status'],
                    request_vals['export_description'],
                    request.update_time.strftime("%b %d, %Y")))
        return _QueryLikeList(rows, total=total_count)

class _GridRowExportRequest:
    def __init__(self, id, object_list, user, status, description, date):
        self.id = id
        self.object_list = object_list
        self.details = "Details"
        self.edit = "Edit"
        self.delete = 'Delete'
        #self.export_mode = export_mode
        #self.export_details = export_details
        self.user = user
        self.date = date
        self.status = status
        self.description = description

class _FastqcReportGrid(grids.Grid, _RequestMixin, _FormMixin, _PagingMixin):
    title = ""
    #model_class = model.Request
    model_class = model.SqnRunResults
    num_rows_per_page = 100
    use_paging = True
    columns = [
        grids.GridColumn("Sample ID", key="sample_id"),
        grids.GridColumn("Sample Name", key="sample_name"),
        grids.GridColumn("Total Sequences", key="total_sequences"),
        grids.GridColumn("Filtered Sequences", key="filtered_sequences"),
        grids.GridColumn("Sequence length", key="sequence_length"),
        grids.GridColumn("%GC", key="perc_gc"),
        grids.GridColumn("", key='qc',link = (lambda item: dict(operation="show_pdf_status_report",id=item.id))),
        grids.GridColumn("ID", key="id", visible=False),
        #grids.MulticolFilterColumn("Search", [], key="results-search",
        #            visible=False, filterable="standard"),
    ]
    operations = [grids.GridOperation("Download",
                allow_multiple=False, allow_popup=False,
                global_operation=(lambda: dict(operation="download")))]

    def __init__(self, service_org):
        grids.Grid.__init__(self)
        for column in self.columns:
            column.sortable=False
        self._service_org = service_org

    def apply_query_filter(self, trans, query, **kwd):
        rows = []
        total_count = None
        user_roles = [r.name for r in trans.user.all_roles()]
        if 'sequencing' in user_roles:
            request_type_name = "Next gen sequencing details"
            try:
                request_type = self._get_request_type(trans, request_type_name)
            except ValueError:
                request_type = None
            if request_type:
                fcid = kwd.get('fcid', '')
                fcid_request = trans.sa_session.query(trans.app.model.Request).filter_by(type=request_type).filter_by(name = fcid).first()
                search_term = kwd.get("search", None)

                for s in fcid_request.samples_by_map():
                    sample_vals = self.form_value_dict(s.values)
                    if search_term and search_term not in s.name and search_term not in sample_vals['description']:
                        continue
                    request_name = '%s %s' %(fcid,s.name)
                    #request_name = '%s MOa694' %fcid
                    request_type_name = "Flowcell status report"
                    try:
                        request_type = self._get_request_type(trans, request_type_name)
                    except ValueError:
                        request_type = None
                    if request_type:
                        request_flowcell_status = trans.sa_session.query(trans.app.model.Request).filter_by(type=request_type).filter_by(name = request_name).first()

                    request_type_name = "FastQC Report"
                    try:
                        request_type = self._get_request_type(trans, request_type_name)
                    except ValueError:
                        request_type = None
                    if request_type:
                        request_fastqc = trans.sa_session.query(trans.app.model.Request).filter_by(type=request_type).filter_by(name = request_name).first()


                        if request_flowcell_status and request_fastqc:

                          request_flowcell_vals = self.form_value_dict(request_flowcell_status.values)
                          request_fastqc_vals = self.form_value_dict(request_fastqc.values)

                          self.title = '%s - FASTQC Report' %(request_flowcell_vals['run_dir'])
                          if 'pdf' in request_flowcell_vals['qc'].lower(): qc = 'PDF'
                          else: qc = request_flowcell_vals['qc']

                          rows.append(_GridRowFastqcReport(s.name,
                                                     sample_vals['description'],
                                                     request_fastqc_vals['total_sequences'],
                                                     request_fastqc_vals['filtered_sequences'],
                                                     request_fastqc_vals['sequence_length'],
                                                     request_fastqc_vals['gc'],
                                                     qc,
                                                     request_flowcell_status.id))


        return _QueryLikeList(rows, total=total_count)

class _GridRowFastqcReport:
    def __init__(self, sample_id, sample_name, total_sequences, filtered_sequences, sequence_length, perc_gc, qc, id):
        self.sample_id = sample_id
        self.sample_name = sample_name
        self.total_sequences = total_sequences
        self.filtered_sequences = filtered_sequences
        self.sequence_length = sequence_length
        self.perc_gc = perc_gc
        self.qc = qc
        self.id = id

class _FlowcellStatusReportGrid(grids.Grid, _RequestMixin, _FormMixin, _PagingMixin):
    title = ""
    model_class = model.Request
    num_rows_per_page = 25
    use_paging = False
    columns = [
        grids.GridColumn("Sample ID", key="sample_id"),
        grids.GridColumn("Sample Name", key="sample_name"),
        grids.GridColumn("Sequenced", key='sequenced'),
        grids.GridColumn("ngLIMS", key="nglims"),
        grids.GridColumn("Stage1", key="stage1"),
        grids.GridColumn("Qualitiy Check", key='qc',link = (lambda item: dict(operation="show_pdf_status_report",id=item.id))),
        grids.GridColumn("Status", key='status'),
        grids.GridColumn("ID", key="id", visible=False),

    ]

    def __init__(self, service_org):
        grids.Grid.__init__(self)
        for column in self.columns:
            column.sortable=False
        self._service_org = service_org

    def apply_query_filter(self, trans, query, **kwd):
        rows = []
        total_count = None
        user_roles = [r.name for r in trans.user.all_roles()]
        if 'sequencing' in user_roles:
            request_type_name = "Next gen sequencing details"
            try:
                request_type = self._get_request_type(trans, request_type_name)
            except ValueError:
                request_type = None
            if request_type:
                fcid = kwd.get('fcid', '')
                fcid_request = trans.sa_session.query(trans.app.model.Request).filter_by(type=request_type).filter_by(name = fcid).first()

                for s in fcid_request.samples_by_map():
                    sample_vals = self.form_value_dict(s.values)
                    request_name = '%s %s' %(fcid,s.name)
                    #request_name = '%s MOa694' %fcid
                    request_type_name = "Flowcell status report"
                    try:
                        request_type = self._get_request_type(trans, request_type_name)
                    except ValueError:
                        request_type = None
                    if request_type:
                        request = trans.sa_session.query(trans.app.model.Request).filter_by(type=request_type).filter_by(name = request_name).first()

                        if request:
                          request_vals = self.form_value_dict(request.values)
                          self.title = '%s - Flowcell Status Report' %(request_vals['run_dir'])
                          if 'pdf' in request_vals['qc'].lower(): qc = 'PDF'
                          else: qc = request_vals['qc']
                          rows.append(_GridRowFlowcellStatusReport(s.name,
                                                     sample_vals['description'],
                                                     request_vals['seq'],
                                                     request_vals['nglims'],
                                                     request_vals['stage1'],
                                                     qc,
                                                     request_vals['status'],
                                                     request.id))


        return _QueryLikeList(rows, total=total_count)

class _GridRowFlowcellStatusReport:
    def __init__(self, sample_id, sample_name, sequenced, nglims, stage1, qc, status, id):
        self.sample_id = sample_id
        self.sample_name = sample_name
        self.sequenced = sequenced
        self.nglims =  nglims
        #self.export_mode = export_mode
        #self.export_details = export_details
        self.stage1 = stage1
        self.qc = qc
        self.status = status
        self.id = id

class _StatusReportGrid(grids.Grid, _RequestMixin, _FormMixin, _PagingMixin):
    """Display a high level grid with status report of runs.
    """
    title = "Flowcell Status Report"
    #model_class = model.Request
    model_class = model.SqnRunResults
    num_rows_per_page = 25
    use_paging = True
    columns = [
        grids.GridColumn("Run Dir", key="run_dir",link = (lambda item: dict(operation="show_flowcell_status_report",
                    id=item.id))),
        grids.GridColumn("Flowcell ID", key="flowcell_id"),
        grids.GridColumn("Samples", key="samples",link = (lambda item: dict(operation="show_flowcell_status_report",
                    id=item.id))),
        grids.GridColumn("Sequenced", key='sequenced'),
        grids.GridColumn("ngLIMS", key="nglims"),
        grids.GridColumn("Stage1", key="stage1"),
        grids.GridColumn("Qualitiy Check", key='qc',link = (lambda item: dict(operation="show_fastqc_report",
                    id=item.id))),
        grids.GridColumn("Status", key='status'),
        grids.GridColumn("ID", key="id", visible=False),
        grids.MulticolFilterColumn("Search", [], key="results-search",
                    visible=False, filterable="standard"),
    ]

    def __init__(self, service_org):
        grids.Grid.__init__(self)
        for column in self.columns:
            column.sortable=False
        self._service_org = service_org

    def apply_query_filter(self, trans, query, **kwd):
        rows = []
        total_count = None
        user_roles = [r.name for r in trans.user.all_roles()]
        if 'sequencing' in user_roles:
            request_type_name = "Flowcell status report"
            try:
                request_type = self._get_request_type(trans, request_type_name)
            except ValueError:
                request_type = None
            if request_type:
                requests = trans.sa_session.query(trans.app.model.Request
                        ).filter_by(type=request_type
                        ).order_by("create_time DESC")
                #requests, total_count = self._paging(requests, kwd)
            else:
                requests = []
            flowcells = {}

            for request in requests:
                request_vals = self.form_value_dict(request.values)
                run_dir = request_vals['run_dir']
                search_term = kwd.get("search", None)
                if search_term:
                    # on searches, show all results
                    kwd['page'] = 'all'
                    if search_term not in run_dir: continue


                if not flowcells.has_key(run_dir):
                    flowcells[run_dir] = []
                flowcells[run_dir].append(request_vals)
            import collections
            flowcells = collections.OrderedDict(sorted(flowcells.items(),reverse=True))

            flowcells, total_count = self._paging_collection(flowcells, kwd)

            for run_dir, run_vals in flowcells.iteritems():
                fcid = run_dir[-9:]
                request_type_name = "Next gen sequencing details"
                try:
                    request_type = self._get_request_type(trans, request_type_name)
                except ValueError:
                    request_type = None
                request = trans.sa_session.query(trans.app.model.Request).filter_by(type=request_type).filter_by(name = fcid).first()
                if request is not None:
                    request_vals = self.form_value_dict(request.values)
                    unique_samples = []
                    for s in request.samples_by_map():
                        if s.values.content['field_0'] not in unique_samples:
                            unique_samples.append(s.values.content['field_0'])
                    samples = len(unique_samples)
                    status_report = {
                                     'sequenced' : 0,
                                     'nglims' : 0,
                                     'stage1' : 0,
                                     'qc' : 0,
                                     'status' : 0,
                                    }
                    for vals in run_vals:
                        status_report['sequenced'] = vals['seq']
                        status_report['nglims'] = vals['nglims']

                        if vals['sample_name'] in ['-']: break
                        if vals['stage1'].encode('utf-8') in [u'\u2714'.encode('utf-8')]:
                            status_report['stage1'] += 1
                        if 'pdf' in vals['qc'].lower():
                            status_report['qc'] += 1
                        if vals['status'].encode('utf-8') in [u'\u2714'.encode('utf-8')]:
                            status_report['status'] += 1

                    if  status_report['stage1'] == samples: status_report['stage1'] = u'\u2714'
                    else: status_report['stage1'] = '%s/%s' %(status_report['stage1'],samples)

                    if  status_report['qc'] == samples: status_report['qc'] = u'\u2714'
                    else: status_report['qc'] = '%s/%s' %(status_report['qc'],samples)

                    if  status_report['status'] == samples: status_report['status'] = u'\u2714'
                    else: status_report['status'] = '%s/%s' %(status_report['status'],samples)


                    rows.append(_GridRowStatusReport(run_dir,
                                                     fcid,
                                                     samples,
                                                     status_report['sequenced'],
                                                     status_report['nglims'],
                                                     status_report['stage1'],
                                                     status_report['qc'],
                                                     status_report['status'],
                                                     request.id))


        return _QueryLikeList(rows, total=total_count)

class _GridRowStatusReport:
    def __init__(self, run_dir, fcid, samples, sequenced, nglims, stage1, qc, status, id):
        self.run_dir = run_dir
        self.flowcell_id = fcid
        self.samples = samples
        self.sequenced = sequenced
        self.nglims =  nglims
        #self.export_mode = export_mode
        #self.export_details = export_details
        self.stage1 = stage1
        self.qc = qc
        self.status = status
        self.id = id


class _SqnRunGrid(grids.Grid, _RequestMixin, _FormMixin, _PagingMixin):
    """Display a high level grid with various sequencing runs performed.
    """
    title = "Sequencing runs"
    model_class = model.Request
    num_rows_per_page = 25
    use_paging = True
    columns = [
        grids.GridColumn("Run folder", key="run_folder"),
        grids.GridColumn("Samples", key='samples',
                link = (lambda item: dict(operation="show_sqnrun_samples",
                    id=item.id))),
        grids.GridColumn("Date", key="date"),
        grids.GridColumn("ID", key="id", visible=False),
        grids.GridColumn("", key="plots",
            link = (lambda item: dict(action="list_sqn_plots",
                search=item.run_folder))),
        grids.GridColumn("", key="results",
            #link = (lambda item: dict(action="list_sqn_results",search=item.run_folder))),
            link = (lambda item: dict(action="show_fastqc_report",fcid=item.run_folder))),
        grids.GridColumn("", key="edit",
                link = (lambda item: dict(operation="edit_sqnrun",
                    id=item.id))),
        grids.GridColumn("", key="save",
                #link = (lambda item: dict(operation="download_sqnrun",id=item.id))),
                link = (lambda item: dict(operation="download_samplesheet",id=item.id))),
        grids.GridColumn("", key="delete",
                link = (lambda item: dict(operation="delete_sqnrun",
                    id=item.id))),
    ]
    def __init__(self, service_org):
        grids.Grid.__init__(self)
        for column in self.columns:
            column.sortable=False
        self._service_org = service_org

    def apply_query_filter(self, trans, query, **kwd):
        rows = []
        total_count = None
        user_roles = [r.name for r in trans.user.all_roles()]
        if 'sequencing' in user_roles:
            request_type_name = "%s %s" % (self._service_org.next_gen_name(),
                    self._service_org.output_name())
            try:
                request_type = self._get_request_type(trans, request_type_name)
            except ValueError:
                request_type = None
            if request_type:
                requests = trans.sa_session.query(trans.app.model.Request
                        ).filter_by(type=request_type
                        ).order_by("create_time DESC")
                requests, total_count = self._paging(requests, kwd)
            else:
                requests = []
            for request in requests:
                request_vals = self.form_value_dict(request.values)
                unique_samples = []
                for s in request.samples_by_map():
                   if s.values.content['field_0'] not in unique_samples:
                       unique_samples.append(s.values.content['field_0'])

                #samples = sum(1 for s in request.samples_by_map())
                samples = len(unique_samples)
                rows.append(_GridRowSqnRun(request.id, samples,
                    request_vals['run_folder'],
                    request.update_time.strftime("%b %d, %Y")))
        return _QueryLikeList(rows, total=total_count)

class _GridRowSqnRun:
    def __init__(self, rid, samples, run_folder, date):
        self.id = rid
        self.samples = samples
        self.run_folder = run_folder
        self.date = date
        self.plots = "Plots"
        self.results = "Results"
        self.edit = "Edit"
        self.save = "Samplesheet"
        self.delete = "Delete"

class _ProjectGrid(grids.Grid, _PagingMixin, _FormMixin):
    """Display a grid of project information.
    """
    title = "Sequencing projects"
    model_class = model.Request
    num_rows_per_page = 25
    use_paging = True
    def __init__(self, project_org):
        self.columns = [
            grids.GridColumn("Project", key="project"),
            grids.GridColumn("Researcher", key="researcher"),
            grids.GridColumn("Samples", key='samples',
                    link = (lambda item: dict(operation="show_project_samples",
                        id=item.id))),
            grids.GridColumn("Sequencing", key="seq_type"),
            grids.GridColumn("Sample States", key="request_type"),
            grids.GridColumn("Payment", key="payment"),
            grids.GridColumn("Last update", key="update_time"),
            grids.GridColumn("ID", key="id", visible=False),
            grids.MulticolFilterColumn("Search samples", [], key="free-text-search",
                    visible=False, filterable="standard"),
        ]
        grids.Grid.__init__(self)
        self._project_org = project_org
        # No sorting implemented right now
        for column in self.columns:
            column.sortable=False

    def apply_query_filter(self, trans, query, **kwd):
        if kwd.has_key("_allow_edit"):
            self.columns.append(grids.GridColumn("", key="edit",
                    link = (lambda item: dict(display="edit_request",
                                              id=item.id, edit="true"))))
            self.columns[-1].sortable = False
        user_roles = [r.name for r in trans.user.all_roles()]
        restrict_to_user = 'sequencing' not in user_roles
        rows = []
        query = self._project_org.get_projects(trans,
                restrict_to_user=restrict_to_user)
        query = query.order_by("request.create_time DESC")
        query, total_count = self._paging(query, kwd)
        for project in query:
            project_info = self._project_org.form_value_dict(project.values)
            num_samples = 0
            updates = []
            sample_states = []
            seq_types = collections.defaultdict(int)
            for sample in project.samples_by_map():
                num_samples += 1
                seq_type = self._project_org.sample_summary(sample)
                seq_types[seq_type] += 1
                sample_states.append(sample.state.name)
                updates.append(sample.events[0].update_time)
            updates.sort(reverse=True)
            if len(updates) > 0:
                last_update = updates[0].strftime("%b %d, %Y")
            else:
                last_update = ""
            sample_states = sorted(list(set(sample_states)))
            seq_types = sorted(seq_types.keys())
            rows.append(_GridRowProject(project.id,
                    project_info.get("project_name", ""),
                    project.user.username, num_samples,
                    ", ".join(sample_states), ", ".join(seq_types),
                    self._get_payment(project_info),
                    last_update))
        rows.sort(reverse=True)
        return _QueryLikeList(rows, total=total_count)

class _QueryLikeList(list):
    def __init__(self, *args, **kwargs):
        if kwargs.has_key('total'):
            self._total = kwargs['total']
            del kwargs['total']
        else:
            self._total = None
        list.__init__(self, *args, **kwargs)

    def count(self):
        if self._total is not None:
            return self._total
        else:
            return len(self)

    def order_by(self, *args, **kwargs):
        """No ordering here; we handle it separately.
        """
        return self

    def limit(self, *args, **kwargs):
        """Need to handle in apply_query_filter.
        """
        return self

    def offset(self, *args, **kwargs):
        """Need to handle in apply_query_filter.
        """
        return self

class _GridRowProject:
    def __init__(self, id, project, researcher, samples, request_type,
            seq_type, payment, update_time):
        self.id = id
        self.project = project
        self.researcher = researcher
        self.samples = samples
        self.request_type = request_type
        self.seq_type = seq_type
        self.payment = payment
        self.update_time = update_time
        self.edit = "Edit"

    def __cmp__(self, other):
        if isinstance(other, self.__class__):
            return cmp(datetime.strptime(self.update_time, "%b %d, %Y"),
                       datetime.strptime(other.update_time, "%b %d, %Y"))
        else:
            return cmp(self.id, other)

class _ResultsGrid(grids.Grid, _PagingMixin):
    """Display a grid of sequencing results stats.
    """
    title = "Sequencing results"
    model_class = model.SqnRunResults
    model_class.web_display_name = "results"
    num_rows_per_page = 25
    use_paging = True
    def __init__(self, project_org, web_ready=True):
        self.columns = [
            grids.GridColumn("Run", key="run"),
            grids.GridColumn("Lane", key="lane"),
            grids.GridColumn("Researcher", key="researcher"),
            grids.GridColumn("Name", key="name"),
            grids.GridColumn("Description", key="description"),
            grids.GridColumn("Genome", key="genome_build"),
            grids.GridColumn("Length", key="read_length"),
            grids.GridColumn("Clusters", key="clusters"),
            grids.GridColumn("Passing clusters (reads)", key="reads"),
            grids.GridColumn("ID", key="id", visible=False),
            grids.MulticolFilterColumn("Search", [], key="results-search",
                    visible=False, filterable="standard"),
        ]
        self.operations = [grids.GridOperation("Download",
                allow_multiple=False, allow_popup=False,
                global_operation=(lambda: dict(operation="download")))]
        grids.Grid.__init__(self)
        self._project_org = project_org
        self.web_ready = web_ready
        # No sorting implemented right now
        for column in self.columns:
            column.sortable=False

    def apply_query_filter(self, trans, query, **kwd):
        model = trans.app.model
        user_roles = [r.name for r in trans.user.all_roles()]
        query = trans.sa_session.query(model.SqnRunResults)
        if 'sequencing' not in user_roles:
            query = query.filter_by(user=trans.user)
        query = query.order_by(
              ["sqn_run_results.request_id DESC", "lane ASC", "sample_id DESC"])
        search_term = kwd.get("search", None)
        if search_term:
            search_term = '%' + search_term.lower() + '%'
            query = query.outerjoin(model.SqnRunResults.user).join(
                    model.SqnRunResults.request).join(
                    model.SqnRunResults.sample)
            query = query.filter(or_(
                func.lower(model.Sample.name).like(search_term),
                func.lower(model.User.username).like(search_term),
                func.lower(model.Request.name).like(search_term),
                ))
            # on searches, show all results
            kwd['page'] = 'all'
        query, total_count = self._paging(query, kwd)
        rows = []
        for result in query:
            request_vals = self._project_org.form_value_dict(
                    result.request.values)
            sample_vals = self._project_org.form_value_dict(
                    result.sample.values)
            vals = self._project_org.form_value_dict(result.form_values)
            user = ""
            if result.user:
                user = result.user.username
            if vals.get("reads", ""):
                reads = vals["reads"]
            else:
                reads = vals["clusters_passed"]
            rows.append(_GridRowResults(result.request.id,
                request_vals["run_folder"], result.lane, user,
                sample_vals["name"], sample_vals["description"],
                sample_vals["genome_build"],
                vals["read_length"],
                self._num_finalize(vals["clusters"], None),
                self._num_finalize(reads, vals["clusters"])))
        return _QueryLikeList(rows, total=total_count)

    def _num_finalize(self, base, comparison):
        if not self.web_ready:
            return str(base)
        def _add_commas(s, sep=','):
            """Add commas to output counts.

            From: http://code.activestate.com/recipes/498181
            """
            if len(s) <= 3: return s
            return _add_commas(s[:-3], sep) + sep + s[-3:]
        comma_base = _add_commas(str(base))
        if comparison and int(comparison) >= int(base):
            percent = " (%.1f%%)" % (float(base) * 100.0 / float(comparison))
        else:
            percent = ""
        return comma_base + percent

class _GridRowResults:
    """Hold sequencing results for a sequencing run and lane.
    """
    def __init__(self, rid, run, lane, researcher, name, description,
            genome_build, read_length, clusters, reads):
        self.id = (rid, lane)
        self.run = run
        self.lane = lane
        self.researcher = researcher
        self.name = name
        self.description = description
        self.genome_build = genome_build
        self.read_length = read_length
        self.clusters = clusters
        self.reads = reads

class _ProjectOrganizer(_FormMixin, _RequestMixin):
    """Organize high level details associated with a sequencing project.

    This is a way to organize a set of samples into a single project with
    associated metadata. The naming of high level projects is also standardized
    in this class, and can be adjusted here.
    """
    def __init__(self, config, service_org, testing=False):
        self.name = None
        for request in config["requests"]:
            if request.get("top_level", False):
                self.name = request["name"]
                break
        assert self.name is not None, "Top level project config not specified"
        self._service_org = service_org
        self._testing = testing

    def get_sample_project(self, sample):
        """Retrieve a project containing the given sample.
        """
        for parent in sample.parent_requests_by_map():
            if parent.type.name == self.name:
                return parent
        return None

    def _current_request_type(self, trans):
        rt = self._get_request_type(trans, self.name)
        form = rt.request_form
        if form.id != form.current.latest_form.id:
            rt.request_form = form.current.latest_form
        return rt

    def get_project_form(self, trans):
        """Retrieve the form information associated with a project.
        """
        rt = self._current_request_type(trans)
        if rt:
            return rt.request_form
        else:
            raise ValueError("Need to define a project inputs form")

    def get_projects(self, trans, restrict_to_user=False,
            active_only=True):
        rtype = trans.sa_session.query(trans.app.model.RequestType).filter_by(
                name=self.name).first()
        requests = trans.sa_session.query(trans.app.model.Request).filter_by(
                type=rtype)
        if active_only:
            requests = self._filter_request_by_state(trans, requests,
                    trans.app.model.Request.states.SUBMITTED)
        if restrict_to_user:
            requests = requests.filter(
                    trans.app.model.Request.user == trans.user)
        return requests

    def save_project(self, trans, params):
        """Save a project request and associated samples.
        """
        request_type = self._current_request_type(trans)
        form_values = self._get_form_values(trans, request_type.request_form,
                params)
        cur_request = self._new_request(trans, request_type, form_values,
                params, trans.app.model.Request.states.SUBMITTED)
        # now find all of the samples and associate them with this request
        sample_ids= params.get('samples', [])
        if not isinstance(sample_ids, list):
            sample_ids = [sample_ids]
        assert len(sample_ids) > 0
        trans.sa_session.add(cur_request)
        if not self._testing:
            trans.sa_session.flush()
            for i, sid in enumerate(sample_ids):
                sample = trans.sa_session.query(trans.app.model.Sample).get(sid)
                assert sample is not None, (sid, sample_ids)
                self._service_org.move_to_starting_state(trans, sample)
                sample.request = cur_request
                request_map = trans.app.model.SampleRequestMap(sample,
                        cur_request, i, False)
            trans.sa_session.flush()

    def sample_summary(self, sample):
        """Return high level summary information for a sequencing sample.
        """
        seq_type = None
        for request in sample.requests_by_map():
            if request.type.name == "%s %s" % (
                    self._service_org.next_gen_name(),
                    self._service_org.input_name()):
                request_info = self.form_value_dict(request.values)
                seq_type = "%s %s" % (request_info['cycles'],
                        request_info['method'].split()[0])
                break
        if seq_type is None:
            raise ValueError("No sequencing info found for %s" %
                    self.form_value_dict(sample.values))
        return seq_type

class _ServiceOrganizer(_FormMixin):
    """Organize services involved in a LIMS, transparently handling in/outputs.

    Services have both inputs and outputs. For a set of chained services, we
    will need the inputs of the first and any inputs of subsequent services. If
    services are cherry-picked, we will need output information from previous
    services. This helps organize this and reveal only high level services.
    """
    def __init__(self, config, testing=False):
        self._service_order = None
        self._service_types = None
        self._testing=testing

        self._setup_states(config)
        self._setup_requests(config)
        self._setup_defaults(config)

        #self._state_order = ["On hold", "Awaiting sample", "Construction",
        #                     "Validation", "Pre-sequencing quantitation",
        #                     "Sequencing", "Sequencing confirmation",
        #                     "Complete", "Cancelled"]
        ## we begin at the "Awaiting sample"
        #self._start_state_index = 1
        #self._next_gen_name = "Next gen sequencing"
        #self._services = ["Library construction", "Library validation",
        #                       "Quantitation", self._next_gen_name,
        #                       "Sequencing confirmation"]
        #self._state_map = {
        #  "Construction" : "Library construction",
        #  "Validation" : "Library validation",
        #  "Pre-sequencing quantitation" : "Quantitation",
        #  "Sequencing" : self._next_gen_name,
        #  }
        # quantitation and controls are part of sequencing, but not exposed
        #self._not_exposed = ["Quantitation details", "Control",
        #                     "Sequencing confirmation details"]
        #self._ignore_names = [o.name for o in other_orgs] + self._not_exposed

    def _setup_states(self, config):
        """Read state information from the input configuration file.
        """
        self._state_order = []
        self._start_state = None
        self._state_operations = dict()
        for state in config["samples"]["states"]:
            if state.get("display", True):
                self._state_order.append(state["name"])
                if state.get("start", False):
                    self._start_state = state["name"]
                operations = state.get("operations", [])
                if not isinstance(operations, list):
                    operations = [operations]
                self._state_operations[state["name"]] = operations
        assert self._start_state is not None, \
                "No starting state specified in configuration file."

    def _setup_requests(self, config):
        """Read request information from input configuration file.
        """
        self._input_name = "inputs"
        self._output_name = "details"
        self._next_gen_name = None
        self._barcode_name = None
        self._exposed_services = []
        self._services = []
        self.default_services = []
        self._state_map = {}
        self._starting_states = []
        for request in config["requests"]:
            if not request.get("top_level", False):
                self._services.append(request["name"])
                if request.get("available", False):
                    self._exposed_services.append(request["name"])
                    self._starting_states.append((request["name"],
                        request["start_state"]))
                    if request.get("default", False):
                        self.default_services.append(request["name"])
                if request.get("state", "") == "Sequencing":
                    self._next_gen_name = request["name"]
                if (request.get("state", "") == "Multiplex" and
                    request["name"].lower().find("construction") == -1):
                    self._barcode_name = request["name"]
                if request.get("state", ""):
                    self._state_map[request["state"]] = request["name"]
        self._confirmation_name = self._services[-1]
        assert self._next_gen_name is not None, \
                "Need to have a sequencing associated request"

    def _setup_defaults(self, config):
        """Establish default information from the configuration file.
        """
        # identify the form value mapping to database keys
        self.dbkey_label = None
        for form_info in config["samples"]["form"]:
            if form_info.get("synonym", "") == "dbkey":
                self.dbkey_label = form_info["label"]
        assert self.dbkey_label is not None, \
                "Must setup a reference to a dbkey in sample form config"

    def pre_received_states(self):
        index = self._state_order.index(self._start_state)
        return self._state_order[:index + 1]

    def next_gen_name(self):
        return self._next_gen_name

    def input_name(self):
        return self._input_name

    def output_name(self):
        return self._output_name

    def get_states(self):
        return self._state_order

    def get_sample_sqn_runs(self, sample):
        """Retrieve sequencing runs associated with the given sample.
        """
        sqn_runs = []
        for order, request in sample.parent_requests_by_map(include_order=True):
            if request.type.name.startswith(self.next_gen_name()):
                request_vals = self.form_value_dict(request.values)
                request_vals['lane'] = order + 1
                request_vals['date'] = request.update_time.strftime("%b %d, %Y")
                request_vals['id'] = request.id
                sqn_runs.append(request_vals)
        return sqn_runs

    def get_sqn_run_results(self, sample, run_folder):
        """Retrieve the results information about a sequencing run.
        """
        for request in sample.requests_by_map():
            if request.type.name.startswith(self._confirmation_name):
                values = self.form_value_dict(request.values)
                if values["sequencing_result"].lower().find("pass") >= 0:
                    return values["results_notes"]
        return None

    def get_latest_sqn_run(self, sample):
        runs = self.get_sample_sqn_runs(sample)
        if len(runs) == 0:
            return None
        to_sort = [(r["id"], r) for r in runs]
        to_sort.sort(reverse=True)
        return to_sort[0][1]

    def get_barcode(self, sample):
        """Retrieve the barcode information associated with a sample.
        """
        for request in sample.requests_by_map():
            if request.type.name.startswith(self._barcode_name):
                vals = self.form_value_dict(request.values)
                vals["date"] = request.values.update_time
                return vals
        return dict()

    def get_sample_arrived_event(self, sample):
        """Retrieve the event when a sample physically arrived for processing.
        """
        prev_event = None
        found = False
        for event in sample.events:
            if self._start_state == event.state.name:
                found = True
                break
            else:
                prev_event = event
        if found and prev_event:
            return prev_event
        else:
            return None

    def sequencing_run_by_search(self, trans, name):
        """Retrieve a sequencing run based on the name ID associated with it.
        """
        form_def = trans.sa_session.query(trans.app.model.FormDefinition
                ).filter(trans.app.model.FormDefinition.name.like(
                "%s %s" % (self.next_gen_name(), self.output_name()))).first()
        term = "%" + name.lower() + "%"
        # SQLite has good handling of Blobs
        if trans.app.model.engine.name in ["sqlite"]:
            search_phrase = func.lower(model.FormValues.content).like(term)
        elif trans.app.model.engine.name in ["postgres"]:
            # Use escape since not all postgres have UTF8. Won't internationalize
            # but should work for our needs
            search_phrase = "lower(encode(form_values.content, 'escape')) LIKE '%s'" % (term)
        # others (MySQL tested) require conversion
        else:
            search_phrase = "LOWER(CONVERT(form_values.content USING utf8)) LIKE '%s'" % (term)
        form_val = trans.sa_session.query(trans.app.model.FormValues
            ).filter_by(form_definition=form_def).filter(search_phrase).order_by(
                    "id DESC")
        form_val = form_val.first()
        if form_val:
            request = trans.sa_session.query(trans.app.model.Request
                    ).filter_by(values=form_val).first()
            if request:
                return request
        return None

    def operations_by_state(self, state):
        """Return grid operations available depending on the queue being viewed.
        """
        operations = []
        for op in self._state_operations.get(state, []):
            operations.append(grids.GridOperation(op))
        return operations
        #if state in ["On hold", "Cancelled"]:
        #    operations.append(grids.GridOperation("Reactivate"))
        #if state in ["Awaiting sample"]:
        #    operations.append(grids.GridOperation("Samples arrived"))
        #if state in ["Construction"]:
        #    operations.append(grids.GridOperation("Add construction details"))
        #if state in ["Validation"]:
        #    operations.append(grids.GridOperation("Add validation"))
        #if state in ["Pre-sequencing quantitation"]:
        #    operations.append(grids.GridOperation("Add quantitation"))
        #if state in ["Sequencing"]:
        #    operations.append(grids.GridOperation("Prepare flow cell",
        #        allow_popup=False))
        #if state in ["Sequencing confirmation"]:
        #    operations.append(grids.GridOperation("Redo sequencing"))
        #    operations.append(grids.GridOperation("Completed"))

    def move_sample_to_state(self, trans, sample, state_name):
        new_state = trans.sa_session.query(trans.app.model.SampleState
                ).filter_by(name=state_name).first()
        event = trans.app.model.SampleEvent(sample, new_state)
        trans.sa_session.add(event)
        if not self._testing:
            trans.sa_session.flush()
        return new_state.name

    def order_services(self, service_names):
        """Organize a list of service names based on a configured order.
        """
        sort_names = [(self._services.index(n), n) for n in service_names]
        sort_names.sort()
        return [n for (_, n) in sort_names]

    def move_to_starting_state(self, trans, sample):
        # first state is a holding state, second is where everything begins
        return self.move_sample_to_state(trans, sample, self._start_state)

    def move_to_next_state(self, trans, sample):
        """Move samples to the next state in the linear flow of processes.

        This hardcodes the logic for moving from step to step in a single
        function.
        """
        cur_state = sample.state.name
        # Get the state a sample should start in, based on desired services.
        if cur_state == self._start_state:
            services = sample.desc.split(",")
            for service_name, state in self._starting_states:
                if service_name in services:
                    return self.move_sample_to_state(trans, sample, state)
            raise ValueError("Did not find initial state for services: %s" %
                    services)
        else:
            try:
                cur_pos = self._state_order.index(cur_state)
                next_state = self._state_order[cur_pos + 1]
            except (ValueError, IndexError):
                raise ValueError("Unexpected state for transition: %s" % cur_state)
            return self.move_sample_to_state(trans, sample, next_state)

    def get_detail_widgets(self, trans, state):
        request_name = "%s %s" % (self._state_map.get(state, state),
                self._output_name)
        request_type = trans.sa_session.query(trans.app.model.RequestType
                ).filter_by(name=request_name).first()
        if request_type is None:
            raise ValueError("Did not find widgets for state %s" % state)
        widgets = request_type.request_form.get_widgets(trans.user)
        return widgets, request_type

    def _get_service_details(self, trans):
        if self._service_order is None:
            display_types = dict()
            dt_names = []
            for rt in trans.sa_session.query(trans.app.model.RequestType
                    ).order_by("id").all():
                if not rt.deleted:
                    exposed = [n for n in self._exposed_services if
                            rt.name.startswith(n)]
                    if len(exposed) > 0:
                        name = " ".join(rt.name.split()[:-1])
                        try:
                            display_types[name].append(rt.id)
                        except KeyError:
                            dt_names.append(name)
                            display_types[name] = [rt.id]
            self._service_order = self.order_services(dt_names)
            self._service_types = display_types
        return self._service_order, self._service_types

    def _get_organized_requests(self, trans, request_ids):
        """Given a set of requests, return request names and associated IDs.
        """
        dt_names, display_types = self._get_service_details(trans)
        need_ids = collections.defaultdict(list)
        for dt_name in dt_names:
            dt_ids = display_types[dt_name]
            if len(dt_ids) == 2:
                input_id, output_id = dt_ids
            elif len(dt_ids) == 1:
                # Crappy way to decide if it's an input or output. Based on the
                # name of the request type.
                request_type = trans.sa_session.query(
                        trans.app.model.RequestType).get(dt_ids[0])
                name_parts = request_type.name.split()
                if name_parts[-1] == self.input_name():
                    input_id = dt_ids[0]
                    output_id = None
                else:
                    input_id = None
                    output_id = dt_ids[0]
            else:
                raise ValueError("Unexpected types associated with name: %s" %
                        (dt_name, dt_ids))
            # Always need inputs for the processes
            if input_id is not None:
                need_ids[dt_name].append(input_id)
            # If we did not do this one, we need to collect the outputs.
            if output_id is not None and output_id not in request_ids:
                need_ids[dt_name].append(output_id)
        return dt_names, need_ids

    def get_sample_form(self, trans):
        """Retrieve the form with details about sample information.

        This assumes that all requests have the same sample form definition,
        which is how we are implemented.
        """
        dt_names, display_types = self._get_service_details(trans)
        # we have not yet loaded any sample forms
        if len(display_types) == 0:
            return None
        one_id = display_types.values()[0][0]
        request = trans.sa_session.query(trans.app.model.RequestType
                ).get(one_id)
        return request.sample_form

    def get_request_types(self, trans, request_ids):
        """Retrieve an ordered set of request types for the given request IDs.
        """
        dt_names, need_ids = self._get_organized_requests(trans, request_ids)
        requests = []
        for dt_name in dt_names:
            for cur_id in need_ids[dt_name]:
                request_type = trans.sa_session.query(
                        trans.app.model.RequestType).get(cur_id)
                requests.append(request_type)
        return requests

    def selected_widgets(self, trans, request_ids):
        """Return widgets for a set of selected request IDs.
        """
        dt_names, need_ids = self._get_organized_requests(trans, request_ids)
        widgets = []
        # present the last (sequencing) items first
        for dt_name in reversed(dt_names):
            cur_ids = need_ids[dt_name]
            if len(cur_ids) > 0:
                widgets.append(dict(label="", helptext="", widget=
                    _SectionLabel(dt_name)))
                for cur_id in cur_ids:
                    request_type = trans.sa_session.query(
                            trans.app.model.RequestType).get(cur_id)
                    widgets.extend(request_type.request_form.get_widgets(
                        trans.user))
        return widgets

    def type_ids_to_display_name(self, trans, type_ids):
        """Given a set of type IDs, return the associated request display name.
        """
        dt_names, display_types = self._get_service_details(trans)
        names = []
        for dt_name in dt_names:
            for cur_id in display_types[dt_name]:
                if cur_id in type_ids:
                    names.append(dt_name)
                    break
        return names

    def display_name_to_type_ids(self, trans, display_names):
        """Given display names, reconsitute the typeIDs they point to.

        This is useful for reconstructing a form parameter from a saved sample
        in the database.
        """
        dt_names, display_types = self._get_service_details(trans)
        type_ids = []
        for dt_name in dt_names:
            if dt_name in display_names:
                type_ids.append(",".join([str(i) for i in
                    display_types[dt_name]]))
        return type_ids

    def multiselect_request_type(self, trans, pre_checked=None):
        """Retrieve a widget ready select box for picking multiple requests.
        """
        if pre_checked is None:
            pre_checked = []
        dt_names, display_types = self._get_service_details(trans)
        options = []
        checked_vals = []
        for dt_name in dt_names:
            dt_id_str = ",".join([str(i) for i in display_types[dt_name]])
            options.append(dict(name=dt_name, value=dt_id_str,
                options=[]))
            if dt_name in pre_checked:
                checked_vals.append(dt_id_str)
        service_w = DrillDownField("services_check", multiple=True,
                display="checkbox", options=options, value=checked_vals)
        tooltip = "Sequencing services needed for this sample. You'll be asked "\
                "to specify additional sample details based on the services "\
                "selected."
        return dict(label="Services needed", widget=service_w,
            helptext="(Required)", title=tooltip)

class _SectionLabel:
    """Provide a simple HTML label for widget-based form display.
    """
    def __init__(self, text):
        self.text = text

    def get_html(self):
        return "<h3 class='section_label'>%s</h3>" % self.text
