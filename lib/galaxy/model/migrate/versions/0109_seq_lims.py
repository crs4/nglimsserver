"""Add sequencing tables

Fixing constraints on table:

    1. alter table sample_request_map rename to sample_request_map_old;
    2. Add table with new constraints;
    3. insert into sample_request_map SELECT * from sample_request_map_old;
    4. drop table sample_request_map_old;
"""
from sqlalchemy import *
from migrate import *

import datetime
now = datetime.datetime.utcnow

import logging
log = logging.getLogger( __name__ )

metadata = MetaData( migrate_engine )

def display_migration_details():
    print ""
    print "Tables for a next generation sequencing lims tied to requests."
    print ""

SampleRequestMap_table = Table('sample_request_map', metadata,
    Column("sample_id", Integer, ForeignKey("sample.id"),
        primary_key = True),
    Column("request_id", Integer, ForeignKey("request.id"),
        primary_key = True),
    Column("order", Integer, nullable = False, autoincrement=False,
        primary_key = True),
    Column("is_forward", Boolean, default = True, index=True,
        primary_key = True),
    )

SqnRunResults_table = Table("sqn_run_results", metadata,
    Column("user_id", Integer, ForeignKey("galaxy_user.id"), index=True,
        nullable=True),
    Column("lane", String(255), primary_key=True),
    Column("request_id", Integer, ForeignKey("request.id"), primary_key=True),
    Column("sample_id", Integer, ForeignKey("sample.id"), primary_key=True),
    Column("form_values_id", Integer, ForeignKey("form_values.id")),
    )

tables = [SampleRequestMap_table, SqnRunResults_table]

def upgrade():
    display_migration_details()
    metadata.reflect()
    for table in tables:
        try:
            table.create()
        except Exception, e:
            print str(e)
            log.debug( "Creating LIMS table failed: %s" % str( e ) )

def downgrade():
    return # do not drop tables for now while in development
    metadata.reflect()
    tables.reverse()
    for table in tables:
        try:
            table.drop()
        except Exception, e:
            print str(e)
            log.debug( "Dropping LIMS table failed: %s" % str( e ) )
