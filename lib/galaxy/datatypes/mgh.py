"""Provide custom datatypes used at MGH.

Initially these are meant as workarounds to avoid having to upload very large
data files to Galaxy.
"""
import data

class AlignRef(data.Text):
    """Represent a reference to an alignment file located on the Galaxy server.

    - first line Y or N if there is frequency file,  
    - second line is the alignment file
    - third line reference labels in order
    - fourth line (opt) the frequency file
    """
    file_ext = "alignref"

class CoverageRef(data.Text):
    """Represent a reference to a coverage file located on the Galaxy server.

    Line info:
    - Location of coverage TSV file
    - Reference genome size file
    """
    file_ext = "covref"

class QltOut(data.Text):
    """Output from the Arachne QLT aligner.
    """
    file_ext = "qltout"
