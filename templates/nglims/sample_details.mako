<%inherit file="/base.mako"/>
<%namespace file="/nglims/common.mako" import="display_details" />

## Based on a section from /requests/show_request.mako
<h2>${name}</h2>
<table class="colored">
    % if base_edit:
       <tr class="odd_row"><th><a href="${base_edit}">Edit</a></th></tr>
    % endif
    % for key, val in sample_info:
      <tr><td><b>${key}</b></td><td>${val}</td></tr>
    % endfor
</table>
%if sqn_runs:
    <h3>Sequencing Runs</h3>
    <ul>
    % for run in sqn_runs:
        <li>${run['run_folder']} -- lane ${run['lane']} (${run['date']})
    %endfor
    </ul>
%endif
%for service in services:
    ${display_details(service, service_details[service], edit_urls.get(service, None))}
%endfor
