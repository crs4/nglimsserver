<%inherit file="/webapps/galaxy/base_panels.mako"/>

<%def name="init()">
<%
    self.has_left_panel=True
    self.has_right_panel=False
    self.message_box_visible=False
%>
</%def>

<%def name="stylesheets()">

    ${parent.stylesheets()}    

    ## TODO: Clean up these styles and move into panel_layout.css (they are
    ## used here and in the editor).
    ##
    ## Copied from admin/index.mako

    <style type="text/css">
    
    #left {
        background: #C1C9E5 url(${h.url_for('/static/style/menu_bg.png')}) top repeat-x;
    }
    
    div.toolMenu {
        margin: 5px;
        margin-left: 10px;
        margin-right: 10px;
    }
    div.toolSectionPad {
        margin: 0;
        padding: 0;
        height: 5px;
        font-size: 0px;
    }
    div.toolSectionDetailsInner { 
        margin-left: 5px;
        margin-right: 5px;
    }
    div.toolSectionTitle {
        padding-bottom: 0px;
        font-weight: bold;
    }
    div.toolMenuGroupHeader {
        font-weight: bold;
        padding-top: 0.5em;
        padding-bottom: 0.5em;
        color: #333;
        font-style: italic;
        border-bottom: dotted #333 1px;
        margin-bottom: 0.5em;
    }    
    div.toolTitle {
        padding-top: 5px;
        padding-bottom: 5px;
        margin-left: 16px;
        margin-right: 10px;
        display: list-item;
        list-style: square outside;
    }
    a:link, a:visited, a:active
    {
        color: #303030;
    }
    </style>
</%def>

<%def name="left_panel()">
  <div class="unified-panel-body" style="overflow: auto;">
    <div class="toolMenu">
    % for section_name, items in section_info:
	<div class="toolSectionList">
	  <div class="toolSectionTitle">
	    <span>${section_name}</span>
	  </div>
	  <div class="toolSectionBody"><div class="toolSectionBg">
	  % for item_name, item_url in items:
	    <div class="toolTitle"><a href="${item_url}" target="galaxy_main">${item_name}</a></div>
	  % endfor
          </div></div>
        </div>
    % endfor
    </div>    
  </div>
</%def>

<%def name="center_panel()">
    <iframe name="galaxy_main" id="galaxy_main" frameborder="0" style="position: absolute; width: 100%; height: 100%;" src="${start_url}"> </iframe>
</%def>
