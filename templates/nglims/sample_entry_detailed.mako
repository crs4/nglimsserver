## Display detailed form information for a sample
## This provides no external context and is meant to be
## called within an existing page.

<%namespace file="/nglims/common.mako" import="html_form_widgets"/>

<style type="text/css">
  h3.section_label {margin-bottom: 3px; margin-top: 3px}
  div.form-row {margin-left: 20px}
</style>
<script type="text/javascript">
  $(document).ready(function () {
    $("select.combobox").combobox();
    $("select.combobox-freetext").combobox({freetext: true});
    $('.tooltip').tipsy({gravity: 'w'});
  });
</script>

<div id="sample-details">
${html_form_widgets(widgets)}
</div>
<div class="help-message">
</div>
