<%inherit file="/base.mako"/>
<%namespace file="/nglims/common.mako" import="entry_form"/>
<%namespace file="/message.mako" import="render_msg" />

%if msg:
    ${render_msg( msg, messagetype )}
%endif

${entry_form(title, form_id, form_url, widgets, button_name=(button_name or 'Save'))}
