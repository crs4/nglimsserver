<%inherit file="/base.mako"/>
<%namespace file="/nglims/common.mako" import="entry_form, item_list, js_sample_details"/>
<%namespace file="/message.mako" import="render_msg" />

<%def name="stylesheets()">
${parent.stylesheets()}
${h.css( "base", "autocomplete_tagging" )}
<link href="${h.url_for('/static/jquery_humanity/jquery-ui-1.8.7.custom.css')}" rel="stylesheet" type="text/css" />
<style type="text/css">
  #initial_samples { list-style-type: none; margin: 0; padding: 1em; width: 60%; }
  #sample_list { list-style-type: none; margin: 0; padding: 1em; width: 60%; }
  #sample_list li { margin: 0 3px 3px 3px; padding: 0.4em; padding-left: 1.5em;
                 font-size: 1.4em; height: 18px; }
  #sample_list li span.ui-icon { position: absolute; margin-left: -1.3em; }

  #sample_pane {width: 40%; float:left; margin-right: 20px}
  #samples ul {list-style-type: none; margin: 0; padding: 0.5em;}
  #samples li { margin: 0 2px 2px 2px; padding: 0.2em; height: 14px; }
  #samples li span.ui-icon { position: absolute; margin-left: -1.3em; }
  #copy_bucket {list-style-type: none; margin: 0; padding: 0.5em;}
  #copy_bucket li { margin: 0 3px 3px 3px; padding: 0.2em; height: 14px; }
  #final_flowcell {width: 40%; float:left;}
  #final_flowcell ul { list-style-type: none; margin: 0; padding: 0.5em;}
  #final_flowcell li {margin: 0 2px 2px 2px; padding: 0.2em; height: 14px;}
  h4 {margin-bottom:.5em}
  h4 {margin-top:.5em}
  </style>
</%def>

<%def name="javascripts()">
  ${parent.javascripts()}
  ${h.js("libs/jquery/jquery", "libs/jquery/jquery.tipsy", "jquery-ui-1.8.7.custom.min", "libs/json2")}
  ${h.js("galaxy.base", "jquery.autocomplete")}
  <script type="text/javascript">
    /*global $ document console replace_big_select_inputs*/
    /*jslint indent:2 */
    $(document).ready(function () {
      // Prevent submit buttons from being double clicked
      $("input[type='submit']").one('click', function () {
	$(this).slideUp();
      });
      // Tooltips
      $('.tooltip').tipsy({gravity: 's'});
      replace_big_select_inputs();
      //
      // Provide a sortable list of items, organized in an input form field.
      //
      $("#samples").children('ul').sortable({
	placeholder: 'ui-state-highlight',
	revert: true,
	helper: 'clone',
	connectWith: '#copy_bucket,#final_flowcell ul'
      });
      $("#copy_bucket").sortable({
	placeholder: 'ui-state-highlight',
	revert: true,
	helper: 'clone',
	// When the copy bucket gets an item, it can now be cloned to the
	// flowcell
	receive: function (ev, ui) {
	  ui.item.draggable({
	    placeholder: "ui-state-highlight",
	    connectToSortable: '#final_flowcell ul',
	    helper: 'clone',
	    revert: 'invalid'
	  });
	}
      });
      $("#final_flowcell").children('ul').sortable({
	placeholder: 'ui-state-highlight',
	connectWith: "#samples ul,#final_flowcell ul",
	// Each time our flowcell is updated, we build a JSON object of where
	// samples are located.
	update: function (event, ui) {
	  var id_info = {};
	  $("#final_flowcell").children("ul").each(function () {
	    var lane_ids = [];
	    $(this).children("li").each(function () {
	      lane_ids.push(this.id);
	    });
	    id_info[this.id] = lane_ids;
	  });
	  $("#sorted_samples").attr('value', JSON.stringify(id_info));
	}
      });

      $("div.disabled").children('input').each(function () {
	console.info(this);
	$(this).attr("disabled", "disabled");
      });
      ${js_sample_details()}
    });
  </script>
</%def>

%if msg:
    ${render_msg( msg, messagetype )}
%endif

${entry_form(title, form_id, form_url, widgets, button_name=button_name or "Save", sort_samples=(sort_samples or None))}

%if current_samples:
   <h2>Current samples</h2>
  ${item_list(current_samples)}
%endif
