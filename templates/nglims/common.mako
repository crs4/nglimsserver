<%def name="js_sample_details()">
##
## Provide jQuery AJAX to retrieve sample details on the same page.
##
  var update_region = $(this).find("#sample-details"),
      sample_links = $(this).find("a.label");
  $(sample_links).each(function () {
    $(this).click(function () {
      update_region.load($(this).attr("href"));
      return false;
    });
  });
</%def>

## Display a single field of an HTML form
<%def name="widget_row(field)">
  <div
    %if field.get("label", ""):
        class="form-row"
    %endif
    >
    %if field.get("title", ""):
        <div class="tooltip" title="${field["title"]}">
    %else:
        <div>
    %endif
    <%
       if field['helptext'].lower().find('required') >= 0:
           field['widget'].required = True
       else:
           field['widget'].required = False
    %>
    <label>${field['label']}</label>
    ${field['widget'].get_html()}
    </div>
    <div class="toolParamHelp" style="clear: both;">
      ${field['helptext']}
    </div>
    <div style="clear: both"></div>
  </div>
</%def>

## Display all HTML widgets in a form environment
<%def name="html_form_widgets(widgets)">
    %for field in widgets:
        ${widget_row(field)}
    %endfor
</%def>

<%def name="entry_form(title, form_id, form_url, widgets, button_name='Save', select_form_type=None, add_finished=False, sort_samples=None)">
##
## A standard entry form, with items specified as widgets.
##
<div class="toolForm">
    %if title:
      <div class="toolFormTitle">${title}</div>
    %endif
    <div class="toolFormBody">
    <form name="${form_id}" id="${form_id}" action="${form_url}" method="post" >
       %if select_form_type:
          <div class="form-row">
            ${select_form_type.get_html()}
          </div>
       %endif
       %for i, field in enumerate(widgets):
          <div class="form-row">
                    <label>
                    %if field.get("title", ""):
                        <span class="tooltip" title="${field["title"]}">
                    %else:
                        <span>
                    %endif
                    ${field['label']}</span></label>
            ${field['widget'].get_html()}
            <div class="toolParamHelp" style="clear: both;">
            ${field['helptext']}
            </div>
            <div style="clear: both"></div>
          </div>
        %endfor
            %if sort_samples or (sort_sample_order and sort_sample_order not in ['{}', '', None]):
                ${sortable_samples(sort_samples, sort_sample_order)}
            %endif
        <div class="form-row">
        <div style="float: left; width: 250px; margin-right: 10px;">
            <input type="hidden" name="form_id" value="${form_id}" size="40"/>
        </div>
          <div style="clear: both"></div>
        </div>
        <div class="form-row">
        %if len(widgets) > 0:
          <input type="submit" name="create_request_button" value="${button_name}"/> 
        %endif
            %if add_finished:
          <input type="submit" name="create_request_button_done" value="Finished"/>
            %endif
        </div>
        </form>
       </div>
    </div>
</div>
</%def>

<%def name="item_list(current_items)">
##
## Display table of items related to a current entry.
##
<table class="grid">
      <thead>
          <tr>
              <th>Name</th>
              <th>Description</th>
          <th>Details</th>
              <th></th>
          <th></th>
          </tr>
      </thead>
      <tbody>
          %for item_id, name, descr, details, edit_url, multiplex_url, display_url in current_items:
              <tr class="libraryRow libraryOrFolderRow draggable" id="libraryRow">
                  <td><a class="label" href="${display_url}">${name}</td>
                  <td>${descr}</td>
                  <td>${details}</td>
                  <td>
                  % if multiplex_url:
                    <a href="${multiplex_url}">Barcodes</a>
                  % endif
                  </td>
                  <td>
                  % if edit_url:
                    <a href="${edit_url}">Edit</a>
                  % endif
                  </td>
              </tr>
          %endfor
      </tbody>
</table>
<p>
<span id="sample-details"><i>Click on a sample name for expanded information.</i></span>
</p>
</%def>

<%def name="sortable_samples(sort_samples, sort_sample_order)">
##
## Display a list of samples which can be sorted with the help of jQuery UI.
##
  <%
     sort_sample_order = util.json.from_json_string(sort_sample_order)
     display_lanes = []
     json_sample_order = {}
     for lane_name, sample_info in sort_sample_order.items():
         display_lanes.append((int(lane_name.replace("lane", "")), sample_info))
         json_sample_order[lane_name] = [id for (id, _) in sample_info]
     display_lanes.sort()
     json_sample_order = util.json.to_json_string(json_sample_order)
  %>

  <div class="form-row">
  <!-- <label>Sample order</label> -->
  <input type="hidden" id="sorted_samples" name="sorted_samples" value='${json_sample_order}' />
  <div id="sample_pane">
   <div id="samples">
   <h4 class="ui-widget-header">Samples</h4>
   <ul class="ui-widget-content">
  % if sort_samples:
     % for (sort_id, sort_str) in sort_samples:
         <li id="${sort_id}" class="ui-state-default">${sort_str}</li>
     % endfor
  %endif
  </ul>
  </div>
  <h4 class="ui-widget-header">Copy</h4>
  <ul id="copy_bucket" class="sortable ui-widget-content">
  </ul>
  </div>

  <div id="final_flowcell">
  % for lane_num, samples in display_lanes:
    <h4 class="ui-widget-header">Lane ${lane_num}</h4>
    <ul class="ui-widget-content" id="lane${lane_num}">
    % for sid, sname in samples:
         <li id="${sid}" class="ui-state-default">${sname}</li>
    % endfor
    </ul>
  % endfor
  </div>
  </div>
  <div style="clear: both"></div>
</%def>

<%def name="display_details(title, details, edit_urls=None)">
    <%
      if not (isinstance(details, list) or isinstance(details, tuple)):
          details = [details]
     %>
    <h3>${title}</h3>
    %for i, cur_detail in enumerate(details):
        <table class="colored">
        %if edit_urls:
          <tr class="odd_row"><th><a href="${edit_urls[i]}">Edit</a></th></tr>
        %endif
        %for label, value in cur_detail.items():
        % if value:
        <%
                  label = " ".join(label.split("_"))
                  label = label[0].upper() + label[1:]
                  if isinstance(value, list):
                      parts = []
                      for item in value:
                          key_val_items = ["%s: %s" % (key.replace("_", " "), val)
                                           for key, val in item.iteritems()]
                          parts.append(",&nbsp;&nbsp;".join(sorted(key_val_items)))
                      value = "<br/>".join(parts)
        %>
        <tr><td><b>${label}</b></td><td>${value}</td></tr>
        % endif
        %endfor
        </table>
    %endfor
</%def>

## Combobox from JqueryUI demos
## http://www.learningjquery.com/2010/06/a-jquery-ui-combobox-under-the-hood
## Modified to:
##   - Not pass on button presses
##   - Ignore return keypreses
##   - Revert to previous value when bad entry passed
##   - Also handle freetext entry
<%def name="jqueryui_combobox()">
  (function( $ ) {
    $.widget( "ui.combobox", {
      options: {
    freetext: false
      },
      _create: function() {
    var self = this,
      select = this.element.hide(),
      selected = select.children( ":selected" ),
      value = selected.val() ? selected.text() : "";
    var input = this.input = $( "<input>" )
      .insertAfter( select )
      .val( value )
      .autocomplete({
        delay: 0,
        minLength: 0,
        source: function( request, response ) {
          var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );
          response( select.children( "option" ).map(function() {
        var text = $( this ).text();
        if ( this.value && ( !request.term || matcher.test(text) ) )
          return {
            label: text.replace(
              new RegExp(
            "(?![^&;]+;)(?!<[^<>]*)(" +
            $.ui.autocomplete.escapeRegex(request.term) +
            ")(?![^<>]*>)(?![^&;]+;)", "gi"
              ), "<strong>$1</strong>" ),
            value: text,
            option: this
          };
          }) );
        },
        select: function( event, ui ) {
          ui.item.option.selected = true;
          self._trigger( "selected", event, {
        item: ui.item.option
          });
        },
        change: function( event, ui ) {
          if ( !ui.item ) {
        var matcher = new RegExp( "^" + $.ui.autocomplete.escapeRegex( $(this).val() ) + "$", "i" ),
          valid = false;
        select.children( "option" ).each(function() {
          if ( $( this ).text().match( matcher ) ) {
            this.selected = valid = true;
            return false;
          }
        });
        if ( !valid ) {
          // remove invalid value, as it didn't match anything
          // revert to the last selected value, or nothing
          if (!self.options.freetext) {
            var prev_val = select.children(":selected").val() ? selected.text() : "";
            $( this ).val( prev_val );
            select.val( prev_val );
            input.data( "autocomplete" ).term = prev_val;
          // If freetext allowed, add our new entry and set it as selected
          } else {
            var val = $(this).val(),
            new_item = $("<option></option>").attr("value", val).html(val);
            new_item.appendTo(select);
            select.children("option").each(function() {
              if ($(this).text() === val) {
            this.selected = true;
            return false;
              }
            });
          }
          return false;
        }
          }
        }
      })
      .addClass( "ui-widget ui-widget-content ui-corner-left" );
    
    input.data( "autocomplete" )._renderItem = function( ul, item ) {
      return $( "<li></li>" )
        .data( "item.autocomplete", item )
        .append( "<a>" + item.label + "</a>" )
        .appendTo( ul );
    };
    // Ignore return being pressed, which raises up the first combobox
    // if multiple are present
    this.input.bind('keypress', function(e) {
      if ((e.keyCode || e.which) == 13) return false;
    });
    this.button = $( "<button>&nbsp;</button>" )
      .attr( "tabIndex", -1 )
      .attr( "title", "Show All Items" )
      .insertAfter( input )
      .button({
        icons: {
          primary: "ui-icon-triangle-1-s"
        },
        text: false
      })
      .removeClass( "ui-corner-all" )
      .addClass( "ui-corner-right ui-button-icon" )
      // Return false on click to prevent event from being propagated if in a form
      .click(function() {
        // close if already visible
        if ( input.autocomplete( "widget" ).is( ":visible" ) ) {
          input.autocomplete( "close" );
          return false;
        }

        // pass empty string as value to search for, displaying all results
        input.autocomplete( "search", "" );
        input.focus();
        return false;
      });
      },

      destroy: function() {
    this.input.remove();
    this.button.remove();
    this.element.show();
    $.Widget.prototype.destroy.call( this );
      }
    });
  })( jQuery );
</%def>
