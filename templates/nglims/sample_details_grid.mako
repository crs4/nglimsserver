<%inherit file="/grid_base.mako"/>
<%namespace file="/grid_base.mako" import="render_grid_header, render_grid_table_body_contents, render_grid_table_footer_contents"/>
<%namespace file="/grid_common.mako" import="render_grid_filters" />
<%namespace file="/nglims/common.mako" import="display_details, js_sample_details" />


<%def name="grid_body( grid )">
    ${parent.grid_body( grid )}
    %if kwargs.get('request_details', None):
	${display_details("", kwargs['request_details'])}
    %endif
    <p>
    <span id="sample-details"><i>Click on a sample name for expanded information.</i></span>
    </p>
</%def>

<%def name="javascripts()">
  ${parent.javascripts()}
  <script type="text/javascript">        
     $(document).ready(function() {
      ${js_sample_details()}
     });
  </script>
</%def>

<%def name="render_grid_header( grid, render_title=True)">
    <div class="grid-header">
        %if render_title:
            <h2>${grid.title}</h2>
        %endif
        ## --- Brad, add the different filter choices to the top of the grid
        %for i, filter in enumerate(grid.standard_filters):
            %if i > 0:
                <span>|</span>
            %endif
                <span class="filter"><a href="${h.url_for(**filter.args)}">
            %if grid.show_filter == filter.label:
		<b>${filter.label}</b></a></span>
            %else:
                ${filter.label}
            %endif
	    </a></span>
        %endfor
        ## ---
        %if grid.global_actions:
            <ul class="manage-table-actions">
            %for action in grid.global_actions:
                <li>
                    <a class="action-button" href="${h.url_for( **action.url_args )}">${action.label}</a>
                </li>
            %endfor
            </ul>
        %endif
    
        ${render_grid_filters( grid )}
    </div>
</%def>

<%def name="render_grid_table( grid, show_item_checkboxes=False)">
    <%
        # Set flag to indicate whether grid has operations that operate on multiple items.
        multiple_item_ops_exist = False
        for operation in grid.operations:
            if operation.allow_multiple:
                multiple_item_ops_exist = True
                
        # Show checkboxes if flag is set or if multiple item ops exist.
        if show_item_checkboxes or multiple_item_ops_exist:
            show_item_checkboxes = True
    %>

    <form action="${url()}" method="post" onsubmit="return false;">
        <div class='loading-elt-overlay'></div>
        <table id='grid-table' class="grid">
            <thead id="grid-table-header">
                <tr>
                    %if show_item_checkboxes:
                        <th></th>
                    %endif
                    %for column in grid.columns:
                        %if column.visible:
                            ## Brad -- after sorting stay on the same filter page
                            ## Remove javascript that eats up these links
                            <%
                                href = ""
                                extra = ""
                                if column.sortable:
                                    if sort_key and sort_key.endswith(column.key):
                                        if not sort_key.startswith("-"):
                                            href = url( sort=( "-" + column.key ),
                                                       state=grid.show_filter)
                                            extra = "&darr;"
                                        else:
                                            href = url( sort=( column.key ),
                                                       state=grid.show_filter)
                                            extra = "&uarr;"
                                    else:
                                        href = url( sort=column.key,
                                                   state=grid.show_filter)
                            %>
                            <th\
                            id="${column.key}-header"
                            %if column.ncells > 1:
                                colspan="${column.ncells}"
                            %endif
                            >
                                %if href:
                                    <a href="${href}" class="not-sort-link" sort_key='${column.key}'>${column.label}</a>
                                %else:
                                    ${column.label}
                                %endif
                                <span class="sort-arrow">${extra}</span>
                            </th>
                            ## ---
                        %endif
                    %endfor
                    <th></th>
                </tr>
            </thead>
            <tbody id="grid-table-body">
                ${render_grid_table_body_contents( grid, show_item_checkboxes )}
            </tbody>
            <tfoot>
                ${render_grid_table_footer_contents( grid, show_item_checkboxes )}
            </tfoot>
        </table>
    </form>
</%def>
