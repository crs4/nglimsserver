## Display plots of sequencing statistics.
<%inherit file="/base.mako"/>

<%def name="stylesheets()">
${parent.stylesheets()}
${h.css( "base", "autocomplete_tagging" )}
<link href="${h.url_for('/static/jquery_humanity/jquery-ui-1.8.7.custom.css')}" rel="stylesheet" type="text/css" />
<link href="${h.url_for('/static/jquery_humanity/jquery.jqplot.min.css')}" rel="stylesheet" type="text/css" />
</%def>

<%def name="javascripts()">
  ${parent.javascripts()}
  <!--[if IE]><script language="javascript" type="text/javascript" src="${h.url_for('/static/scripts/jquery.jqplot.excanvas.min.js' )}"></script><![endif]-->
  ${h.js("libs/jquery/jquery", "libs/jquery/jquery.tipsy", "libs/json2")}
  ${h.js("jquery-ui-1.8.7.custom.min", "jquery.jqplot.min", "jquery.jqplot.cursor.min",
         "jquery.jqplot.dateAxisRenderer.min", "jquery.jqplot.highlighter.min",
         "jquery.jqplot.ohlcRenderer.min")}
  ${h.js("galaxy.base", "jquery.autocomplete", "autocomplete_tagging")}
  <script type="text/javascript">
    /*global $ document console window*/
    /*jslint indent:2 */
    $(document).ready(function () {
      var sqn_read_plot = function (reads) {
	var read_counts = [];
	$.each(reads, function(_, vals) {
	  $.each(vals, function(i, v) {
	    if (i > 0) {
	      read_counts.push(v);
	    };
	  });
	});
        var zoom_plot = $.jqplot('zoom-plot', [reads], {
          title:'Reads (passing clusters)',
	  series:[
	    {label: 'Reads (million)',
             renderer:$.jqplot.OHLCRenderer
	    } 
	  ],
	  legend:{show:true, location:'nw'},
	  axesDefaults:{useSeriesColor: true},
          cursor:{
            zoom: true,
            showTooltip:false
          },
	  axes:{
	    xaxis: {
	      renderer:$.jqplot.DateAxisRenderer
	    },
	    yaxis: {
	      tickOptions: {formatString:'%d'},
              min: Math.min.apply(Math, read_counts) - 1,
              max: Math.max.apply(Math, read_counts) + 1
	    }
	  },
	  highlighter: {
            showMarker:false,
            tooltipAxes: true,
            tooltipLocation: 'se',
            yvalues: 3,
	    formatString: '<table class="jqplot-highlighter">' +
              '<tr><td>date:</td><td>%s</td></tr>' +
              '<tr><td>max:</td><td>%.1f</td></tr>' +
              '<tr><td>min:</td><td>%.1f</td></tr>' +
              '<tr><td>mean:</td><td>%.1f</td></tr>' +
            '</table>'
	  }
        }),
        overview_plot = $.jqplot('overview-plot', [reads], {
	  series:[
	    {}, 
	    {yaxis:'y2axis'}
	  ],
          seriesDefaults:{neighborThreshold:0, showMarker: false},
          cursor:{
            zoom: true,
            showTooltip: false,
            constrainZoomTo:'x'
          },
	  axes:{
	    xaxis: {
	      renderer:$.jqplot.DateAxisRenderer
	    },
	    yaxis: {
	      tickOptions: {showLabel: false}
	    },
	    y2axis: {
	      tickOptions: {showLabel: false}
	    }
	  }
        });
	$.jqplot.Cursor.zoomProxy(zoom_plot, overview_plot);
      };
      var sqn_pass_plot = function(pass_data) {
	pass_plot = $.jqplot('pass-plot', [pass_data], {
	  series: [{showLine: false, markerOptions: {style: 'x'}}],
          cursor: {
            showTooltip:false
          },
	  axes: {
	    xaxis: {label: 'Reads (million)'},
	    yaxis: {
	      label: 'Pass',
	      tickOptions: {formatString: '%.1f%%'}
	    }
	  },
	  highlighter: {
            showMarker:false,
            tooltipAxes: true,
            tooltipLocation: 'ne',
            yvalues: 3,
	    formatString: '<table class="jqplot-highlighter">' +
              '<tr><td>reads:</td><td>%.1fm</td></tr>' +
              '<tr><td>pass:</td><td>%s</td></tr>' +
              '<tr><td>date:</td><td>%s</td></tr>' +
            '</table>'
	  }
	});
      };
      var prepare_data = function(plot_index) {
	$.ajax({
	  dataType: "json",
	  url: "${h.url_for('/nglims/sqn_plot_reads')}",
	  success: function (msg) {
	    switch(plot_index) {
	      case 0:
		sqn_read_plot(msg.reads);
		break;
	      case 1:
		sqn_pass_plot(msg.passing);
		break;
	    }
	  }
	});
      };

      $.jqplot.config.enablePlugins = true;
      $("#plot-tabs").tabs({selected: 0}).bind('tabsselect', function(event, ui) {
	prepare_data(ui.index);
      });
      prepare_data(0);
    });
  </script>
</%def>

<div id="plot-tabs">
  <ul>
    <li><a href="#date-plots">Reads over time</a></li>
    <li><a href="#passing-plots">Passing reads</a></li>
  </ul>
  <div id="date-plots">
    <div id="zoom-plot" style="height:550px;width:700px;"></div>
    <div id="overview-plot" style="height:150px;width:700px;"></div>
  </div>
  <div id="passing-plots">
    <div id="pass-plot" style="height:550px;width:700px;"></div>
  </div>
</div>
