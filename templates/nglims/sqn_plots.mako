## Display plots of sequencing statistics.
<%inherit file="/base.mako"/>

<%def name="stylesheets()">
${parent.stylesheets()}
${h.css( "base", "autocomplete_tagging" )}
<link href="${h.url_for('/static/jquery_humanity/jquery-ui-1.8.7.custom.css')}" rel="stylesheet" type="text/css" />
<link href="${h.url_for('/static/jquery_humanity/jquery.jqplot.min.css')}" rel="stylesheet" type="text/css" />
</%def>

<%def name="javascripts()">
  ${parent.javascripts()}
  <!--[if IE]><script language="javascript" type="text/javascript" src="${h.url_for('/static/scripts/jquery.jqplot.excanvas.min.js' )}"></script><![endif]-->
  ${h.js("libs/jquery/jquery", "libs/jquery/jquery.tipsy", "libs/json2")}
  ${h.js("jquery-ui-1.8.7.custom.min", "jquery.jqplot.min", "jquery.jqplot.cursor.min",
         "jquery.jqplot.dateAxisRenderer.min", "jquery.jqplot.highlighter.min",
         "jquery.jqplot.ohlcRenderer.min")}
  ${h.js("galaxy.base", "jquery.autocomplete", "autocomplete_tagging")}

   <script type="text/javascript">
    /*global $ document console window*/
    /*jslint indent:2 */
    $(document).ready(function () {
        $( "#plot-tabs" ).tabs({
                //event: "mouseover"
        });

        if ("${first_base_report}" == 0 ) {$("#plot-tabs").tabs('select',1);$("#plot-tabs").tabs({disabled: [0]});}
        if ("${status_report}" == 0) {$("#plot-tabs").tabs('select',0);$("#plot-tabs").tabs({disabled: [1]});}
    });


  </script>
</%def>

<div id="plot-tabs">
  <ul>
    <li id ='0'><a href="#first-plots">First report</a></li>
    <li id ='1'><a href="#status-plots">Status</a></li>
  </ul>
   <div id="first-plots" width="100%" height="750">
    <iframe src="${first_base_report}" width="100%" height="750"></iframe>
  </div>
   <div id="status-plots" width="100%" height="750">
    <iframe src="${status_report}" width="100%" height="750"></iframe>
  </div>

</div>


