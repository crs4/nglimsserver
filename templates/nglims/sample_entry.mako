## Provide wizard style input for samples.
<%inherit file="/base.mako"/>
<%namespace file="/nglims/common.mako" import="html_form_widgets, item_list, js_sample_details, jqueryui_combobox"/>
<%namespace file="/message.mako" import="render_msg" />

<%def name="stylesheets()">
${parent.stylesheets()}
${h.css( "base" )}
<link href="${h.url_for('/static/jquery_humanity/jquery-ui-1.8.7.custom.css')}" rel="stylesheet" type="text/css" />
<link href="${h.url_for('/static/jquery_humanity/jWizard.base.css')}" rel="stylesheet" type="text/css" />
<style type="text/css">
  .jWizard_form label.ui-state-error-text {display: none; margin-left: 10px;}
  #sum_barcode_list { list-style-type: none; margin: 0; padding: 0.2em;}
  .help-message {float: right; width=25%;}
  #sample-overview {float: left;}
  #sample-details {float: left;}
  .ui-button { margin-left: -1px; }
  .ui-button-icon-only .ui-button-text { padding: 0.35em; } 
  .ui-autocomplete-input { margin: 0; padding: 0.48em 0 0.47em 0.45em; }
  .tipsy {font-size: 14px;}
  .tipsy-inner {max-width: 300px;}
  .jw-steps-wrap {height: 42em}
  select, textarea, input[type="text"], input[type="file"], input[type="password"] {
    max-width: 500px;
  }
  input[readonly='true'] {
    background-color: #f4f0ec;
    cursor:default;
  }
  </style>
</%def>

<%def name="javascripts()">
  ${h.js("libs/jquery/jquery", "libs/jquery/jquery.tipsy", "jquery-ui-1.8.7.custom.min", "libs/json2")}
  ${h.js("jquery.jWizard.min", "jquery.validate")}
  <script type="text/javascript">
    $(document).ready(function () {
      ${js_sample_details()}
      ${jqueryui_combobox()}
      $("select.combobox").combobox();
      $("select.combobox-freetext").combobox({freetext: true});
      $(".ui-autocomplete-input").attr("size", "40");
      $('.tooltip').tipsy({gravity: 'w'});
      // Add names to autocomplete entries for validator identification
      $(".ui-autocomplete-input").each(function() {
        $(this).attr("name",
                     $(this).prev("select").attr("name") + "_validate");
      });
      $wiz = $(".jWizard_form");
      jQuery.validator.addMethod("selected_genome",
        function(value, element) {
          return this.optional(element) || value.match("^unspecified") === null;
        }, "Please select a genome build for the sample");
      jQuery.validator.addMethod("validate_services",
         function(value, element) {
           var nextgen_value = "Next gen sequencing",
               values = [];
           $wiz.find('input:checked[name="services_check"]').each(function() {
             values.push($(this)[0].nextSibling.wholeText.replace("\n", ""));
           });
           return values.length > 0 && $.inArray(nextgen_value, values) != -1;
        }, "Please select services needed; sequencing is required");
      $wiz.validate({
        errorClass: "ui-state-error-text",
        rules: {
          services_check: "validate_services",
          genome_build_validate: "selected_genome"
        },
        errorPlacement: function(error, element) {
          if (element.attr("name") === "services_check") {
            error.insertAfter(element.parent("li").parent("ul").parent("div")
                         .parent("div").find("label"));
          } else if (element.attr("aria-haspopup") === "true") {
            error.insertAfter(element.parent("div").find("button"));
          } else {
            error.insertAfter(element);
          }
        }
      });
      $wiz.find("input:text").attr("size", "45");
      $wiz.find(".ui-autocomplete-input").attr("size", "41");
      $wiz.jWizard({
        hideTitle: true,
        menuEnable: true,
        buttons: {
          cancelHide: true,
          finishType: "submit"
        }
      })
      .bind("jwizardchangestep", function (event, ui) {
    // Handle form validation checks
    if (ui.type !== "manual") {
      var cur_step = $(this).find(".jw-step:eq(" + ui.currentStepIndex + ")"),
          inputs = cur_step.find("input,textarea"),
          direction = ui.nextStepIndex - ui.currentStepIndex;
      if (inputs.length > 0 && direction > 0 && inputs.valid !== undefined && !inputs.valid()) {
        var error_pos = cur_step.find("input.ui-state-error-text,textarea.ui-state-error-text"),
        offset = error_pos.parent("div").offset().top;
        $("div.jw-steps-wrap").animate({scrollTop: offset - 10}, "slow");
        error_pos.parent("div").find("label.ui-state-error-text").effect("highlight", {}, "slow");
        return false;
      }
    }
    // Add detailed information based on user selections
    if (ui.type !== "manual") {
      switch(ui.currentStepIndex) {
        // Moving to the detail step; add based on selected services
        case 0:
          var cur_services = $wiz.find('input[name="services"]').attr("value"),
          service_ids = [];
          $wiz.find('input:checked[name="services_check"]').each(function() {
            service_ids.push($(this).attr("value"));
          });
          // If we've changed the services desired, update the form.
          if (service_ids.join(",") !== cur_services) {
            $.get('${h.url_for("/nglims/get_sample_form_detailed")}',
             {services: service_ids},
             function(data) {
               var cur_region = $("#sample_details");
               cur_region.html(data);
               cur_region.find("input:text").attr("size", "45");
               cur_region.find("textarea").attr("cols", "43").attr("rows", "5");
               cur_region.find(".ui-autocomplete-input").attr("size", "41");
             }
        );
          }
          break;
        // Moving from the barcode step; save the current information
        case 2:
          console.info(ui);
          if (ui.delta == 1) {
            $("#barcode_details").find("#submit-button").trigger('click');
          }
          break;
      }
    }
    switch(ui.nextStepIndex) {
        // Moving to the barcode step. Check if we need it based on multiplex selection.
        case 2:
          var no_multiplex = $('input:checked[name="is_multiplex"]').size() === 0;
          if (no_multiplex) {
        $("#barcode_details").html("<div class='infomessage'>" +
          "Current sample is not multiplexed. " +
          "Enable multiplexing in the sample information step.</div>");
        $("#multiplex_barcodes").attr("value", "{}");
          }
          if (no_multiplex && ui.currentStepIndex == 1) {
        event.nextStepIndex = 3;
        event.preventDefault();
        return false;
          } else if (no_multiplex && ui.currentStepIndex == 3) {
        event.nextStepIndex = 1;
        event.preventDefault();
        return false;
          // Otherwise load up barcoding if we haven't already
          } else if (!no_multiplex && $("#barcode_details").find("input#callback_url").size() === 0) {
            $("#barcode_details").load('${h.url_for("/nglims/add_barcode")}',
                                     {data_store_id: 'multiplex_barcodes'});
            $("#multiplex_barcodes").attr("value", "{}");
          }
          break;
      // Display a summary on the last page
      case 3:
        if (ui.currentStepIndex == 2 && $("#bcs_okay").val() !== "yes") {
          return false;
        }
        var bcs = JSON.parse($("#multiplex_barcodes").attr("value"));
        $("#wiz_summary").find("#sum_name").html(
          $("#sample_info").find("input[name='name']").attr("value"));
        $("#wiz_summary").find("#sum_desc").html(
          $("#sample_info").find("input[name='description']").attr("value"));
        $("#wiz_summary").find("#sum_services").html(
          $("#sample_details").find("input[name='service_names']").attr("value"));
        $("#wiz_summary").find("#sum_sqn").html(
          $("#sample_details").find("select[name='cycles']").find(":selected").attr("value") +
          " " +
          $("#sample_details").find("select[name='method']").find(":selected").attr("value"));
        if (bcs.barcodes) {
          var bc_list = $("#wiz_summary").find("#sum_barcode_list");
          bc_list.empty();
          $("#wiz_summary").find("#sum_barcode_type").html(bcs.barcode_type);
          $.each(bcs.barcodes, function(_, val) {
        $('<li>' + val[0].replace("NEW^^", "") + " -- " + val[1] + '</li>').appendTo(bc_list);
          });
        }
        break;
    }
      });
    });
  </script>
</%def>

<%def name="wizard_sample_form(form_id, form_url, widgets)">
  <form class="jWizard_form" name="${form_id}" id="${form_id}" action="${form_url}" method="post">
    <div id="sample_info" title="Sample information">
      <div id="sample-overview">
    ${html_form_widgets(widgets)}
    <input type="hidden" id="multiplex_barcodes" name="multiplex_barcodes" value="{}"/>
      </div>
    </div>
    <div id="sample_details" title="Preparation details">
    </div>
    <div id="barcode_details" title="Barcodes">
    </div>
    <div id="wiz_summary" title="Summary">
      <table class="grid">
        <thead><tr>
          <th></th>
          <th>${"&nbsp" * 150}</th>
        </tr></thead>
        <tbody>
      <%
            summary_values = [("Name", "sum_name"), ("Description", "sum_desc"),
                              ("Services", "sum_services"),
                              ("Sequencing", "sum_sqn"),
                              ("Barcodes", "sum_multi")]
          %>
      %for name, id_name in summary_values:
        <tr class="libraryRow libraryOrFolderRow draggable" id="libraryRow">
          <td><b>${name}</b></td>
          <td><span id="${id_name}">
        % if id_name == "sum_multi":
            <i id="sum_barcode_type"></i>
            <ul id="sum_barcode_list">
            </ul>
        % endif
          </span></td>
        </tr>
      %endfor
        </tbody>
      </table>
    </div>
  </form>
</%def>

${wizard_sample_form(form_id, form_url, widgets)}

%if current_samples:
   <h2>Current samples</h2>
  ${item_list(current_samples)}
%endif
