---
# Configuration file defining requests and sample details for
# next generation sequencing sample management (454 specific).

samples:
  states:
    - name: Unsubmitted
      display: false
    - name: On hold
      operations: Reactivate
    - name: Awaiting sample
      start: true
      operations: Sample arrived
    - name: Construction
      operations: [Add construction details, Add barcode]
    - name: Validation
      operations: Add validation
    - name: Pre-sequencing quantitation
      operations: Add quantitation
    - name: Sequencing
      operations: [Add to sequencing run]
    - name: Sequencing confirmation
      operations: [Redo sequencing, Completed]
    - name: Complete
    - name: Cancelled
      operations: Reactivate
  form:
    - label: Name
    - label: Description
      tooltip: Your descriptive name for the sample
    - label: Genome Build
      synonym: dbkey
      tooltip: Organism and genome build associated with the sample
    - label: Analysis
      type: [None]
      tooltip: >
        Type of automated post-processing analysis to perform on the samples.
        454 analyses yet to be decided. 

requests:
  - name: Project
    top_level: true
    form:
      - label: Project name
      - label: Description
        type: TextArea
        required: false
      - label: Payment (Fund number/Cost code)
  - name: Library construction
    state: Construction
    start_state: Construction
    available: true
    form:
      inputs:
        - label: Sample preparation method (plus volume and concentration of the submitted sample).
          type: TextArea
          tooltip: Description of the method used to prepare your DNA or RNA samples.
            If library construction is needed, please also include the volume and 
            concentration of the submitted sample.
        - label: Adapters
          type: [454 Lib-A (Amplicons), 454 Lib-L (Genomic)]
          freetext: true
          tooltip: >
            Relevant adaptor sequences used in library preparation. Feel free to enter a 
            free text description if none of the choices match.
        - label: Library type
          type: [Genomic DNA, dscDNA, sscDNA, sRNA, mRNA, DGE, ChIP-Seq, PCR Product]
          freetext: true
          tooltip: >
            Describe the type of prepared library. Feel free to enter a free text
            description if none of the choices match.
        - label: Library construction method
          type: TextArea
          tooltip: >
            Description of the method used to generate the sample library. 
            Please name any protocols used (e.g. cDNA amplification). If you are submitting DNA or RNA to be 
            constructed here, please specify the specific protocols to use. 
      details:
        - label: Quantitation method
          tooltip: Method used to quantitate your library sample preparation.
        - label: Concentration
          tooltip: >
           Indicate units. Should be at least 0.5ug (at least 1ug is recommended).
        - label: Volume
          tooltip: We request a minimum volume of 40ul.
        - label: Amplicon size (including primers)
          tooltip: >
           Average amplicon size (as confirmed by gel/bioanalyzer) if you are submitting amplicons. 
           If you're not submitting amplicons, leave blank.
        
  - name: Barcode
    state: Multiplex 
    form:
      inputs:
      details:
        - label: Barcode type
          required: false
        - label: Barcode id
          required: false
        - label: Sequence
          required: false
        - label: Barcodes
          required: false
  - name: Library validation
    state: Validation
    start_state: Validation
    available: true
    form:
      inputs:
      details:
        - label: Validation results
          type: TextArea
          tooltip: >
            Description of method and results in validating the library. For
            spot sequencing validation, please provide a summary of counts
            for good and problematic clones.
  - name: Quantitation
    state: Pre-sequencing quantitation 
    form:
      inputs:
      details:
        - label: Quantitation results
          type: TextArea
  - name: Next gen sequencing
    state: Sequencing
    start_state: Pre-sequencing quantitation
    available: true
    default: true
    form:
      inputs:
        - label: Cycles
          type: [200, 150, 100]
          tooltip: >
            **200 cycles is recommended**
            100 cycles = ~250bp reads. 150 cycles = ~375bp reads. 200 cycles = ~500bp reads. 
        - label: Method
          type: [single, paired end]
      details:
        - label: Run folder
        - label: Run notes
          type: TextArea
  - name: Sequencing confirmation
    form:
      inputs:
      details:
        - label: Run folder
        - label: Sequencing result
        - label: Results notes
          type: TextArea
          required: false

user:
  form:
    - label: Lab association

controls:
  - name: PhiX
    description: control
    genome_build: phix
    analysis: Standard

sequencing:
  lanes: 16

sqn_run_results:
  name: Sequencing run results
  form:
    - label: Read length
    - label: Clusters
    - label: Clusters passed
    - label: Reads
    - label: Aligned
    - label: Pair duplicates

messages:
  dropoff : >
    After submitting your project, please put your sample in the box "samples for pyrosequencing" in the in the top 2nd drawer of freezer PLH298 in lab 04F10/11. 

barcodes:
  - name: 454_Rapid
    data:
    - RL1: ACACGACGACT
    - RL2: ACACGTAGTAT
    - RL3: ACACTACTCGT
    - RL4: ACGACACGTAT
    - RL5: ACGAGTAGACT
    - RL6: ACGCGTCTAGT
    - RL7: ACGTACACACT
    - RL8: ACGTACTGTGT
    - RL9: ACGTAGATCGT
    - RL10: ACTACGTCTCT
    - RL11: ACTATACGAGT
    - RL12: ACTCGCGTCGT
  - name: 454_Standard
    data:
    - SL1: ACGAGTGCGT
    - SL2: ACGCTCGACA
    - SL3: AGACGCACTC
    - SL4: AGCACTGTAG
    - SL5: ATCAGACACG
    - SL6: ATATCGCGAG
    - SL7: CGTGTCTCTA
    - SL8: CTCGCGTGTC
    - SL9: TAGTATCAGC
    - SL10: TCTCTATGCG
    - SL11: TGATACGTCT
    - SL12: TACTGAGCTA
    - SL13: CATAGTAGTG
    - SL14: CGAGAGATAC
    - SL15: ATACGACGTA
    - SL16: TCACGTACTA
    - SL17: CGTCTAGTAC
    - SL18: TCTACGTAGC
    - SL19: TGTACTACTC
    - SL20: ACGACTACAG
    - SL21: CGTAGACTAG
    - SL22: TACGAGTATG
    - SL23: TACTCTCGTG
    - SL24: TAGAGACGAG
    - SL25: TCGTCGCTCG
    - SL26: ACATACGCGT
