This is a custom fork of the main [Galaxy][1] server that incorporates
a lightweight next generation sequencing information management system
on top of existing Galaxy functionality. The Galaxy portion of the
code is hosted at:

https://bitbucket.org/chapmanb/galaxy-central

and it integrates with an automated analysis pipeline:

https://github.com/chapmanb/bcbb/tree/master/nextgen

A high level overview, with videos, is available at:

http://bcbio.wordpress.com/2011/01/11/next-generation-sequencing-information-management-and-analysis-system-for-galaxy/

[1]: http://usegalaxy.org

## Installation

Instructions are available from:

https://bitbucket.org/galaxy/galaxy-central/wiki/LIMS/nglims

## Customization

The main entry point for customization is the configuration file in
`tool-data/nglims.yaml.` This provides a way to automatically populate
the forms used for requesting sample information and organizing sample
states. This information is stored in the Galaxy database, using their
standard form parameters, so should be edited and settled before
entering your sample data. Adjustments are possible at any time but can
require custom scripts to migrate existing data, so it's nice to avoid
this if you can.

### Requests

The `request` section contains definitions for the forms. These are
stored in the database so after each edit you need to run the python
script to update the correct tables:

         python scripts/nglims/add_ng_defaults.py universe_wsgi.ini

Requests are different processes that might be done in the lab, like
Library Constrution or Sequencing. Each request contains two main
types of form definitions:

* `inputs` -- These are values that are required for a process. These
  will need to be entered by the user whether the process was done by
  the researcher, or is requested to be done at your facility. This
  helps collect information that is needed to perform the process.

* `details` -- These are values that come out of a process and will be
  known once it's finished. These may be required by subsequent steps
  and need to be entered by a researcher if the process is not done at
  the sequencing facility. For instance, the insert size from library
  construction is helpful for validating libraries before sequencing.

### Barcodes

The custom YAML file also provides a location for standardized
barcodes. Any number of barcode choices can be included, and require a
top level name along with barcode name/sequence key/value pairs for
each barcode. Edits here do not require any update of the database;
you only need to restart Galaxy.

### Samples

The samples section defines the states and operations associated with
these states. While names are configurable, adding new operations
requires writing corresponding request code in the controller.

## Use

This details some of the more non-obvious features which are available
in the interface.

### Manually editing values and changing states

Administrators and sequencing technicians can edit samples and move
samples between states. This is useful for correcting mistakes in
entry, manually adjusting the state of a sample, or moving samples to
the cancelled or on-hold queue.

When logging in as a sequencing user, this is done in the
'View projects' section. Clicking on the samples for the project give
you an 'Edit' choice in the right panel to edit samples. The 'Modify
state' button allows manually swapping the state of a sample.
